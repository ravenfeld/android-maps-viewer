package com.ravenfeld.maps

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.core.app.NotificationManagerCompat
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class MapsApplication : Application(), Configuration.Provider {

    @Suppress("LateinitUsage")
    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    override val workManagerConfiguration: Configuration
        get() = Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()

    override fun onCreate() {
        super.onCreate()
        CoroutineScope(Dispatchers.IO).launch {
            initNotificationChannel()
        }
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = NotificationManagerCompat.from(this)
            val idDownload = getString(R.string.notification_channel_id_download)
            val name = getString(R.string.notification_channel_name)
            val importanceDownload = NotificationManager.IMPORTANCE_LOW
            val channelDownload = NotificationChannel(idDownload, name, importanceDownload)
            channelDownload.enableLights(false)
            channelDownload.enableVibration(false)
            channelDownload.setShowBadge(false)
            channelDownload.setSound(null, null)
            notificationManager.createNotificationChannel(channelDownload)
        }
    }
}
