package com.ravenfeld.maps.data.di

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.haroldadmin.cnradapter.NetworkResponseAdapterFactory
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.moczul.ok2curl.CurlInterceptor
import com.moczul.ok2curl.logger.Logger
import com.ravenfeld.maps.BuildConfig
import com.ravenfeld.maps.R
import com.ravenfeld.maps.data.remote.DownloadFileApi
import com.ravenfeld.maps.data.remote.GpxStudioBrouterApi
import com.ravenfeld.maps.data.remote.IgnApi
import com.ravenfeld.maps.data.remote.OpenElevationApi
import com.ravenfeld.maps.data.remote.OverpassApi
import com.ravenfeld.maps.data.remote.SearchApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private val JSON = "application/json".toMediaType()

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun provideRetrofitLocal(@ApplicationContext context: Context): DownloadFileApi {
        return Retrofit.Builder()
            .baseUrl("http://localhost/")
            .client(createClient(context))
            .build()
            .create(DownloadFileApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofitGpxStudioBrouter(@ApplicationContext context: Context): GpxStudioBrouterApi {
        val json = Json {
            ignoreUnknownKeys = true
        }
        return Retrofit.Builder()
            .baseUrl("https://routing.gpx.studio/")
            .client(createClient(context))
            .addConverterFactory(json.asConverterFactory(JSON))
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
            .create(GpxStudioBrouterApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofitIgn(@ApplicationContext context: Context): IgnApi {
        val json = Json { ignoreUnknownKeys = true }
        return Retrofit.Builder()
            .baseUrl("https://data.geopf.fr/")
            .client(createClient(context))
            .addConverterFactory(json.asConverterFactory(JSON))
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
            .create(IgnApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofitOpenElevation(@ApplicationContext context: Context): OpenElevationApi {
        val json = Json { ignoreUnknownKeys = true }
        return Retrofit.Builder()
            .baseUrl("https://api.open-elevation.com/")
            .client(createClient(context))
            .addConverterFactory(json.asConverterFactory(JSON))
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
            .create(OpenElevationApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofitSearch(@ApplicationContext context: Context): SearchApi {
        val json = Json { ignoreUnknownKeys = true }
        return Retrofit.Builder()
            .baseUrl("https://photon.komoot.io")
            .client(createClient(context))
            .addConverterFactory(json.asConverterFactory(JSON))
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
            .create(SearchApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofitOverpass(@ApplicationContext context: Context): OverpassApi {
        val json = Json { ignoreUnknownKeys = true }
        return Retrofit.Builder()
            .baseUrl("https://overpass.private.coffee/api/")
            .client(createClient(context))
            .addConverterFactory(json.asConverterFactory(JSON))
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
            .create(OverpassApi::class.java)
    }

    private fun createClient(context: Context): OkHttpClient {
        val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        okHttpClientBuilder.readTimeout(2, TimeUnit.MINUTES)
        if (BuildConfig.DEBUG) {
            if (BuildConfig.BUILD_TYPE == "chucker") {
                okHttpClientBuilder.addInterceptor(ChuckerInterceptor.Builder(context).build())
            }
            okHttpClientBuilder.addInterceptor(
                CurlInterceptor(
                    logger = object : Logger {
                        override fun log(message: String) {
                            Timber.tag("Ok2Curl").v(message)
                        }
                    }
                )
            )
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
        }
        okHttpClientBuilder.addInterceptor(
            Interceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader("User-Agent", context.getString(R.string.app_name))
                    .build()
                chain.proceed(request)
            }
        )
        return okHttpClientBuilder.build()
    }
}
