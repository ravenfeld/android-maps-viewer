package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.data.remote.dto.OverpassResponseDto
import com.ravenfeld.maps.domain.place.model.Type
import org.maplibre.android.geometry.LatLngBounds
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

private const val OVERPASS_PEAK =
    """[bbox:BBOX][out:json];
        (node[natural=peak];);
        out%20center;"""

private const val OVERPASS_SADDLE =
    """[bbox:BBOX][out:json];
        (
        node[mountain_pass=yes];
        node[natural=saddle];
        );
        out%20center;"""

private const val OVERPASS_ALPINE_HUT =
    """[bbox:BBOX][out:json];
        (nw[tourism=alpine_hut];);
        out%20geom;"""

private const val OVERPASS_WILDERNESS_HUT =
    """[bbox:BBOX][out:json];
        (nw[tourism=wilderness_hut];);
        out%20geom;"""

private const val OVERPASS_WATER =
    """[bbox:BBOX][out:json];
        (
        node[amenity~"^(drinking_water|water_point)$"];
        node[drinking_water~"^(yes|conditional)$"];
        );
        out%20center;"""

private const val OVERPASS_PARKING =
    """[bbox:BBOX][out:json];
        (nw[amenity=parking][access!=private];);
        out%20geom;"""

private const val OVERPASS_PARKING_BICYCLE =
    """[bbox:BBOX][out:json];
        (nw[amenity=bicycle_parking][access!~"^(private|customers|members )$"];);
        out%20geom;"""

private const val OVERPASS_TOILET =
    """[bbox:BBOX][out:json];
        (nw[amenity=toilets][access!~"^(permissive|customers)$"];);
        out%20geom;"""

private const val OVERPASS_SHOP =
    """[bbox:BBOX][out:json];
        (
        nw[shop~"^(bakery|supermarket|convenience)$"];
        );
        out%20geom;"""

private const val OVERPASS_CAMP_SITE =
    """[bbox:BBOX][out:json];
        (nw[tourism=camp_site];);
        out%20geom;"""

@Singleton
class OverpassWebService
@Inject constructor(
    private val overpassApi: OverpassApi
) {
    suspend fun getPlace(type: Type, area: LatLngBounds): Result<OverpassResponseDto> {
        val data = when (type) {
            Type.PEAK -> OVERPASS_PEAK
            Type.SADDLE -> OVERPASS_SADDLE
            Type.ALPINE_HUT -> OVERPASS_ALPINE_HUT
            Type.WILDERNESS_HUT -> OVERPASS_WILDERNESS_HUT
            Type.WATER -> OVERPASS_WATER
            Type.PARKING -> OVERPASS_PARKING
            Type.PARKING_BICYCLE -> OVERPASS_PARKING_BICYCLE
            Type.TOILET -> OVERPASS_TOILET
            Type.SHOP -> OVERPASS_SHOP
            Type.CAMP_SITE -> OVERPASS_CAMP_SITE
        }.replace(
            "BBOX",
            "${area.latitudeSouth},${area.longitudeWest},${area.latitudeNorth},${area.longitudeEast}"
        )

        return when (
            val response = overpassApi.getWay(data)
        ) {
            is NetworkResponse.Success -> {
                Result.success(response.body)
            }

            is NetworkResponse.Error -> {
                Timber.e(response.error, "Unwrapping network error")
                Result.failure(response.error ?: IOException())
            }
        }
    }
}
