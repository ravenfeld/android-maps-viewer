package com.ravenfeld.maps.data.mapper

import com.ravenfeld.maps.data.database.PresetEntity
import com.ravenfeld.maps.domain.preset.model.Preset

fun List<PresetEntity>.toDomain(presetIdSelected: Int) = this.map {
    it.toDomain(presetIdSelected == it.id)
}
fun PresetEntity.toDomain(selected: Boolean) = Preset(id = id, name = name, selected = selected)
