package com.ravenfeld.maps.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.ravenfeld.maps.data.database.TrackColorUpdateEntity
import com.ravenfeld.maps.data.database.TrackEntity
import com.ravenfeld.maps.data.database.TrackNameUpdateEntity
import com.ravenfeld.maps.data.database.TrackUpdateEntity
import com.ravenfeld.maps.data.database.TrackVisibleUpdateEntity
import com.ravenfeld.maps.data.database.TrackWithUserActions
import kotlinx.coroutines.flow.Flow

@Suppress("ComplexInterface")
@Dao
interface TrackDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(track: TrackEntity): Long

    @Query("DELETE FROM track WHERE id = :id")
    suspend fun delete(id: Int)

    @Update(entity = TrackEntity::class)
    suspend fun update(track: TrackUpdateEntity)

    @Update(entity = TrackEntity::class)
    suspend fun update(track: TrackVisibleUpdateEntity)

    @Update(entity = TrackEntity::class)
    suspend fun update(track: TrackColorUpdateEntity)

    @Update(entity = TrackEntity::class)
    suspend fun update(track: TrackNameUpdateEntity)

    @Query("SELECT * FROM track ORDER BY id ASC")
    fun getTracks(): Flow<List<TrackEntity>>

    @Query("SELECT name FROM track")
    fun getTracksName(): List<String>

    @Transaction
    @Query("SELECT * FROM track WHERE id = :id")
    suspend fun getTrack(id: Int): TrackWithUserActions

    @Query("SELECT max(id) FROM track")
    suspend fun getTrackMaxId(): Int?
}
