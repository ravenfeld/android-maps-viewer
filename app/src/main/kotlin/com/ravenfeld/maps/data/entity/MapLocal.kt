package com.ravenfeld.maps.data.entity

data class MapLocal(
    val name: String,
    val path: String,
    val minZoom: Float,
    val maxZoom: Float
)
