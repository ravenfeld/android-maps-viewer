package com.ravenfeld.maps.data.extension

import com.google.android.material.math.MathUtils.lerp
import com.ravenfeld.maps.data.mapper.toPoint
import com.ravenfeld.maps.data.mapper.toTrackPoint
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.TrackPoint
import com.ravenfeld.maps.domain.geo.toPoint
import org.maplibre.geojson.LineString
import org.maplibre.geojson.Point
import org.maplibre.turf.TurfConstants
import org.maplibre.turf.TurfMeasurement
import org.maplibre.turf.TurfMisc
private const val MIN_DISTANCE_POI_TRACK = 50.0

internal fun List<List<TrackPoint>>.findSliceSegment(cutPoint: GeoPoint): SliceSegment? {
    var distance = MIN_DISTANCE_POI_TRACK
    var indexSegment = 0
    var pointResult: Point? = null

    this.forEachIndexed { index, segment ->
        val pointOnLine = if (segment.size >= 2) {
            TurfMisc.nearestPointOnLine(cutPoint.toPoint(), segment.map { it.toPoint() })
                .geometry() as Point
        } else {
            segment.first().toPoint()
        }
        val distanceWaypointAndPoint = TurfMeasurement.distance(
            pointOnLine,
            cutPoint.toPoint(),
            TurfConstants.UNIT_METERS
        )
        if (distanceWaypointAndPoint < distance) {
            indexSegment = index
            distance = distanceWaypointAndPoint
            pointResult = pointOnLine
        }
    }
    return pointResult?.toTrackPoint()?.let { geoPointResult ->
        val segment = this[indexSegment]
        val startSegment = sliceSegment(segment.first(), geoPointResult, segment)
            .dropLastIf(1) {
                it.size > 1
            }
        val endSegment = segment.subList(startSegment.size, segment.size)
        val altitude = if (endSegment.isEmpty()) {
            startSegment.last().geoPoint.altitude
        } else {
            findElevation(startSegment.last().geoPoint, endSegment.first().geoPoint, geoPointResult.geoPoint)
        }
        val point = TrackPoint(
            geoPoint = GeoPoint(
                longitude = geoPointResult.geoPoint.longitude,
                latitude = geoPointResult.geoPoint.latitude,
                altitude = altitude
            ),
            way = startSegment.last().way
        )
        SliceSegment(
            geoPoint = point.geoPoint,
            index = indexSegment,
            startSegment = startSegment + point,
            endSegment = endSegment.toMutableList().apply { add(0, point) }
        )
    }
}

data class SliceSegment(
    val geoPoint: GeoPoint,
    val index: Int,
    val startSegment: List<TrackPoint>,
    val endSegment: List<TrackPoint>
)

private fun sliceSegment(
    startPoint: TrackPoint,
    endPoint: TrackPoint,
    segment: List<TrackPoint>
): List<TrackPoint> =
    if (!startPoint.geoPoint.isSamePosition(endPoint.geoPoint)) {
        TurfMisc.lineSlice(
            startPoint.toPoint(),
            endPoint.toPoint(),
            LineString.fromLngLats(
                segment.map {
                    it.toPoint()
                }
            )
        ).coordinates().map { it.toTrackPoint(way = startPoint.way) }
    } else {
        listOf(startPoint)
    }

private fun findElevation(startPoint: GeoPoint, endPoint: GeoPoint, point: GeoPoint): Double {
    if (startPoint == endPoint) {
        return startPoint.altitude ?: 0.0
    } else {
        val distanceTotal = TurfMeasurement.distance(startPoint.toPoint(), endPoint.toPoint())
        val distanceAtPoint = TurfMeasurement.distance(startPoint.toPoint(), point.toPoint())
        val percent = distanceAtPoint / distanceTotal
        val latitude = lerp(
            (startPoint.altitude ?: 0.0).toFloat(),
            (endPoint.altitude ?: 0.0).toFloat(),
            percent.toFloat()
        ).toDouble()
        return latitude
    }
}

private fun <T> List<T>.dropLastIf(n: Int, predicate: (List<T>) -> Boolean): List<T> =
    if (predicate(this)) {
        dropLast(n)
    } else {
        this
    }
