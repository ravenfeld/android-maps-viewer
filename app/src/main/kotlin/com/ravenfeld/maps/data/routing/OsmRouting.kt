package com.ravenfeld.maps.data.routing

import android.content.Context
import com.ravenfeld.maps.BuildConfig
import com.ravenfeld.maps.data.utils.UnzipUtils.unzip
import com.ravenfeld.maps.data.utils.safeLet
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.ProjectionResult
import com.ravenfeld.maps.domain.geo.distanceBetween
import com.ravenfeld.maps.domain.geo.distanceBetweenPointLine
import com.ravenfeld.maps.domain.geo.getProjectionGeoPoint
import com.ravenfeld.maps.domain.geo.symplify
import com.ravenfeld.maps.domain.routing.RoutingState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import kotlin.experimental.and
import kotlin.math.max
import kotlin.math.min

// Routing constants
private const val ROUTE_HEAP_CAPACITY = 1_000_000

// Max RAM heap space usage to limit grid loading
private const val GRID_RAM_LIMIT = 0.7
private const val RADIUS_START = 500.0

// 1 048 576 octets = one megabyte (1 MiB)
private const val NB_OCTETS = 1_048_576
private const val FOLDER_NAME = "osm_routing"

/**
 * Solves routing problems using A* algorighm
 * Class inspired by AStarRouteSolver.java by Jonas Grunert
 */
@Suppress("TooManyFunctions", "LargeClass")
class OsmRouting(val context: Context) {

    private var maxGridCount = -1

    // Grid information
    private var gridInfo: GridInfo? = null

    // A simple queue with order of grids loaded to unload it in same order
    private var loadedGrids: MutableList<MapGrid> = ArrayList()

    private var gridLoadsTotal = 0
    private var gridLoadsRoute = 0
    private var gridVisitTimestamp: Long = 0

    // Heap for rout finding
    private var routeDistHeap: NodeDistHeap? = null

    private val mapGridsDir = File(context.getExternalFilesDir(null), FOLDER_NAME)

    // Edge bitfilter and speed
    private var found = false
    private var edgeFilterBitMask: Byte = 0
    private var edgeFilterBitValue: Byte = 0
    private var visNodeGridIndex: Long = 0
    private var visGridIndex = 0
    private var visNodeIndex = 0
    private var visitedCount = 0
    private var hCalc = 0
    private var hReuse = 0
    private var gridChanges = 0
    private var gridStays = 0
    private var firstVisits = 0
    private var againVisits = 0
    private var startTime: Long = 0

    // Stores all buffers for all grids involved in routing
    private val routingGridBuffers: MutableMap<Int, MapGridRoutingBuffer> = HashMap()

    // Stores all open nodes and their heuristic
    private var openList: MutableSet<Long> = HashSet()

    private var oldVisGridIndex = 0
    private var visGrid: MapGrid? = null
    private var visGridRB: MapGridRoutingBuffer? = null

    private val state: MutableStateFlow<RoutingState> = MutableStateFlow(RoutingState.NotReady)

    /**
     * Constructor, loads grid data
     */
    init {
        CoroutineScope(Dispatchers.IO).launch {
            init()
        }
    }

    @Suppress("TooGenericExceptionCaught")
    private suspend fun init() = withContext(Dispatchers.IO) {
        state.update { RoutingState.NotReady }
        if (BuildConfig.DEBUG) {
            Timber.i("Start init grid")
        }
        try {
            context.getExternalFilesDirs(null).forEach {
                val unzip = File(it, "$FOLDER_NAME.zip")
                if (unzip.exists()) {
                    unzip(unzip, it.absolutePath + File.separator + FOLDER_NAME)
                    unzip.delete()
                }
            }
            intializeGrids()
            routeDistHeap = NodeDistHeap(ROUTE_HEAP_CAPACITY)
            state.update { RoutingState.Standby }
        } catch (e: Exception) {
            Timber.e("Error at loadOsmData")
            e.printStackTrace()
            state.update { RoutingState.NotReady }
        }
        if (BuildConfig.DEBUG) {
            Timber.i("Finished init grid")
        }
    }

    /**
     * Read and initialize grid information
     * @throws Exception
     */
    @Throws(Exception::class)
    private fun intializeGrids() {
        val grid = GridInfo(File(mapGridsDir, "grids.index"))
        if (grid.isLoaded) {
            gridInfo = grid
        }
    }

    fun getState(): Flow<RoutingState> = state

    /**
     * Calculates a route using an improved A Star algorithm
     */
    @Suppress("ReturnCount")
    @Synchronized
    fun startCalculateRoute(startGeoPoint: GeoPoint, targetGeoPoint: GeoPoint): List<GeoPoint> {
        if (state.value != RoutingState.Standby) {
            Timber.e("Routing not available")
            if (state.value == RoutingState.NotReady) {
                return emptyList()
            }
            requestCancelRouting()
            while (state.value != RoutingState.Standby) {
                Timber.e("Routing not available")
            }
        }
        if (BuildConfig.DEBUG) {
            Timber.i("Max memory: " + Runtime.getRuntime().maxMemory() / NB_OCTETS + "Mb")
        }
        val startNearNode = findNearNode(startGeoPoint)
        val targetNearNode = findNearNode(targetGeoPoint)

        if (startNearNode == null || targetNearNode == null) {
            Timber.e("Cannot calculate route: Must select any start and target")
            return emptyList()
        }

        state.update { RoutingState.Routing }
        startTime = System.currentTimeMillis()
        gridLoadsRoute = 0

        // Reset buffers and
        routeDistHeap?.resetEmpty()
        routingGridBuffers.clear() // Stores all buffers for all grids involved in routing
        openList.clear()

        found = false
        visitedCount = 0
        hCalc = 0
        hReuse = 0
        gridChanges = 0
        gridStays = 0
        firstVisits = 0
        againVisits = 0

        routeDistHeap?.add(startNearNode.getIndex(), 0.0f)
        val startGrid = getGrid(startNearNode.gridIndex)
        startGrid?.let {
            val startGridRB = MapGridRoutingBuffer(startGrid.nodeCount)
            routingGridBuffers[startNearNode.gridIndex] = startGridRB
            visGridRB = startGridRB
        }
        openList.add(startNearNode.getIndex())
        visGrid = startGrid
        oldVisGridIndex = startNearNode.gridIndex

        System.out.flush()
        if (BuildConfig.DEBUG) {
            Timber.i("Start routing from ${startNearNode.nodeGeoPoint} to ${targetNearNode.nodeGeoPoint}")
        }
        return doRouting(startNearNode, targetNearNode)
    }

    /**
     * Loads grid, caches and returns it
     */
    @Suppress("MagicNumber", "TooGenericExceptionCaught", "NestedBlockDepth")
    private fun loadGrid(gridIndex: Int): MapGrid? {
        return try {
            // Unload grid if to many grids in buffer
            if (maxGridCount != -1) {
                while (loadedGrids.size >= maxGridCount) {
                    // Find grid longest time not used
                    loadedGrids.sortBy { it.visitTimestamp }
                    for (index in 0..100) {
                        val toUnload = loadedGrids.removeAt(index)
                        // Unload
                        if (BuildConfig.DEBUG) {
                            Timber.i("Unloaded grid  ${toUnload.gridIndex} . Grids loaded: ${loadedGrids.size}")
                        }
                    }
                }
            }

            // Load grid
            val loaded = MapGrid(
                gridIndex,
                gridVisitTimestamp,
                File(mapGridsDir, "$gridIndex.grid")
            )
            if (loaded.isLoaded) {
                loadedGrids.add(loaded)
                gridLoadsTotal++
                gridLoadsRoute++
                if (BuildConfig.DEBUG) {
                    Timber.i(
                        "Loaded grid $gridIndex. Grids loaded: ${loadedGrids.size}" +
                            ". Load operations: $gridLoadsTotal" +
                            ". Heap-Size: ${Runtime.getRuntime().totalMemory() / NB_OCTETS}Mb"
                    )
                }

                // Check if grid count is not limited yet but needs to be limited
                if (maxGridCount == -1 &&
                    Runtime.getRuntime().totalMemory() / Runtime.getRuntime().maxMemory()
                        .toDouble() > GRID_RAM_LIMIT
                ) {
                    maxGridCount = loadedGrids.size
                    if (BuildConfig.DEBUG) {
                        Timber.i("RAM limit reached - limited grid buffer to $maxGridCount")
                    }
                }
                loaded
            } else {
                null
            }
        } catch (e: Exception) {
            Timber.e("Failed while grid loading: ${e.message}")
            null
        }
    }

    private fun loadGrid2(gridIndex: Int): MapGrid2? {
        val mapGrid2 = MapGrid2(gridIndex, File(mapGridsDir, "$gridIndex.grid2"))
        return if (mapGrid2.isLoaded) {
            mapGrid2
        } else {
            null
        }
    }

    /**
     * Returns grid a point is located in or next grid if not in any grid
     */
    private fun getGridOfPoint(searchGeoPoint: GeoPoint, radius: Double): List<Int> {
        val listGridIndex = ArrayList<Int>()
        gridInfo?.let {
            var latI = ((searchGeoPoint.latitude - it.gridMinLat) / it.gridRaster).toInt()
            var lonI = ((searchGeoPoint.longitude - it.gridMinLon) / it.gridRaster).toInt()
            latI = max(0, min(it.gridLatCount - 1, latI))
            lonI = max(0, min(it.gridLonCount - 1, lonI))

            val lat0Grid = (latI * it.gridRaster + it.gridMinLat).toDouble()
            val lon0Grid = (lonI * it.gridRaster + it.gridMinLon).toDouble()

            val lat1Grid = ((latI + 1) * it.gridRaster + it.gridMinLat).toDouble()
            val lon1Grid = ((lonI + 1) * it.gridRaster + it.gridMinLon).toDouble()

            listGridIndex.add(it.gridIndices[latI][lonI])
            // Line left
            if (distanceBetweenPointLine(
                    searchGeoPoint,
                    GeoPoint(lat0Grid, lon0Grid),
                    GeoPoint(lat1Grid, lon0Grid),
                ) <= radius
            ) {
                listGridIndex.add(it.gridIndices[latI][lonI - 1])
            }

            // Line Top
            if (distanceBetweenPointLine(
                    searchGeoPoint,
                    GeoPoint(lat0Grid, lon0Grid),
                    GeoPoint(lat0Grid, lon1Grid),
                ) <= radius
            ) {
                listGridIndex.add(it.gridIndices[latI - 1][lonI])
            }

            // Line Right
            if (distanceBetweenPointLine(
                    searchGeoPoint,
                    GeoPoint(lat0Grid, lon1Grid),
                    GeoPoint(lat1Grid, lon1Grid),
                ) <= radius
            ) {
                listGridIndex.add(it.gridIndices[latI][lonI + 1])
            }

            // Line Bottom
            if (distanceBetweenPointLine(
                    searchGeoPoint,
                    GeoPoint(lat1Grid, lon0Grid),
                    GeoPoint(lat1Grid, lon1Grid),
                ) <= radius
            ) {
                listGridIndex.add(it.gridIndices[latI + 1][lonI])
            }
        }
        return listGridIndex
    }

    /**
     * Tries to return a grid, tries to load grid if necessary
     */
    private fun getGrid(gridIndex: Int): MapGrid? {
        val mapGrid = loadedGrids.find { it.gridIndex == gridIndex }
        return if (mapGrid?.isLoaded == true) {
            mapGrid
        } else {
            loadGrid(gridIndex)
        }
    }

    /**
     * Tries to determine coordinates of a node, tries to load grid if necessary
     * @return Coordinates of node
     */
    private fun getNodeGeoPoint(grid: MapGrid, nodeIndex: Int): GeoPoint =
        GeoPoint(grid.nodesLat[nodeIndex].toDouble(), grid.nodesLon[nodeIndex].toDouble())

    /**
     * Tries to find out index of next point to given coordinate
     * @return Index of next point
     */
    private fun findNearNode(searchGeoPoint: GeoPoint): NearNode? {
        val results = ArrayList<NearNode>()
        var radiusChange = RADIUS_START
        do {
            getGridOfPoint(searchGeoPoint, radiusChange).forEach {
                results.addAll(findNearNode(it, searchGeoPoint, radiusChange))
            }
            radiusChange += RADIUS_START
        } while (results.isEmpty())
        results.sortBy { it.nodeDistance }

        return findNearEdge(searchGeoPoint, results) ?: results.firstOrNull()
    }

    @Suppress("NestedBlockDepth")
    private fun findNearEdge(
        searchGeoPoint: GeoPoint,
        listNearNode: ArrayList<NearNode>
    ): NearNode? {
        var mapGrid2: MapGrid2? = null

        var nearNodeWithEdge: NearNode? = null
        var smallestDist = listNearNode.first().nodeDistance
        listNearNode.forEach {
            val gridIndex = it.gridIndex
            val nodeIndex = it.nodeIndex
            getGrid(gridIndex)?.let { mapGrid ->

                if (mapGrid2?.gridIndex != gridIndex) {
                    mapGrid2 = loadGrid2(gridIndex)
                }

                mapGrid2?.let { mapGrid2 ->
                    var edgeIndex = mapGrid.nodesEdgeOffset[nodeIndex]
                    var edgeIndexMapGrid2 = mapGrid2.edgeCoordsOffsets[edgeIndex]
                    @Suppress("ComplexCondition")
                    while (nodeIndex + 1 < mapGrid.nodesEdgeOffset.size &&
                        edgeIndex < mapGrid.nodesEdgeOffset[nodeIndex + 1] ||
                        nodeIndex + 1 == mapGrid.nodesEdgeOffset.size &&
                        edgeIndex < mapGrid.edgeCount
                    ) {
                        @Suppress("ComplexCondition")
                        while (edgeIndex + 1 < mapGrid2.edgeCoordsOffsets.size &&
                            edgeIndexMapGrid2 < mapGrid2.edgeCoordsOffsets[edgeIndex + 1] ||
                            edgeIndex + 1 == mapGrid2.edgeCoordsOffsets.size &&
                            edgeIndexMapGrid2 < mapGrid2.edgeCoordsLat.size
                        ) {
                            val edgeGeoPoint =
                                GeoPoint(
                                    mapGrid2.edgeCoordsLat[edgeIndexMapGrid2].toDouble(),
                                    mapGrid2.edgeCoordsLon[edgeIndexMapGrid2].toDouble()
                                )

                            val dist = distanceBetween(searchGeoPoint, edgeGeoPoint)
                            if (dist < smallestDist) {
                                smallestDist = dist
                                nearNodeWithEdge = it.copy(edgeIndex, edgeGeoPoint, dist)
                            }
                            edgeIndexMapGrid2++
                        }
                        edgeIndex++
                    }
                }
            }
        }
        return nearNodeWithEdge
    }

    /**
     * Tries to find out index of next point to given coordinate
     * @return Index of next point
     */
    private fun findNearNode(
        gridIndex: Int,
        searchGeoPoint: GeoPoint,
        radius: Double
    ): List<NearNode> {
        // Get grid
        val grid = getGrid(gridIndex)
        val listNearNode = ArrayList<NearNode>()
        return grid?.let {
            for (nodeIndex in 0 until grid.nodeCount) {
                val node = getNodeGeoPoint(grid, nodeIndex)
                val dist = distanceBetween(searchGeoPoint, node)
                if (dist <= radius) {
                    listNearNode.add(NearNode(searchGeoPoint, gridIndex, nodeIndex, node, dist))
                }
            }
            listNearNode
        } ?: run {
            listNearNode
        }
    }

    private fun doRouting(startNearNode: NearNode, targetNearNode: NearNode): List<GeoPoint> {
        // Find route with A*
        routeDistHeap?.let {
            while (!it.isEmpty()) {
                if (state.value == RoutingState.Canceling) {
                    canceledRouting()
                    return emptyList()
                }

                // Remove and get index
                visNodeGridIndex = it.removeFirst()

                // Visit node/neighbors
                if (visitNode(targetNearNode)) {
                    found = true
                    break
                }
                safeLet(visGrid, visGridRB) { visGrid, visGridRB ->
                    visitNodeEdges(targetNearNode, visGrid, visGridRB)
                }
            }
        }
        if (BuildConfig.DEBUG) {
            Timber.i("H calc: $hCalc")
            Timber.i("H reuse: $hReuse")
            Timber.i("gridChanges: $gridChanges")
            Timber.i("gridStays: $gridStays")
            Timber.i("firstVisits: $firstVisits")
            Timber.i("againVisits: $againVisits")
            Timber.i("MaxHeapSize: " + routeDistHeap?.sizeUsageMax)
            Timber.i("gridLoads: $gridLoadsRoute")
        }
        val calculatedRoute = ArrayList<GeoPoint>()
        // Routing finished
        if (found) {
            // Reconstruct route
            if (startNearNode.nodeIndex == targetNearNode.nodeIndex) {
                calculatedRoute.addAll(doRoutingSameNode(startNearNode, targetNearNode))
            } else {
                calculatedRoute.addAll(reconstructRoute(startNearNode, targetNearNode))
            }
        } else {
            // No route found
            calculatedRoute.clear()
            Timber.e("No way found")
        }

        // Cleanup
        openList.clear()
        loadedGrids.clear()
        routingGridBuffers.clear()
        routeDistHeap?.resetEmpty()

        state.update { RoutingState.Standby }
        if (BuildConfig.DEBUG) {
            Timber.i("Finished routing after ${System.currentTimeMillis() - startTime} ms")
        }
        return calculatedRoute
    }

    @Suppress("ComplexMethod")
    private fun doRoutingSameNode(
        startNearNode: NearNode,
        targetNearNode: NearNode
    ): List<GeoPoint> {
        val start = startNearNode.edgeGeoPoint ?: startNearNode.nodeGeoPoint
        val target = targetNearNode.edgeGeoPoint ?: targetNearNode.nodeGeoPoint

        val projection = getProjectionGeoPoint(startNearNode.nodeGeoPoint, start, target)
        return when {
            projection == null -> route(
                startNearNode.searchGeoPoint,
                targetNearNode.searchGeoPoint,
                listOf(listOf(startNearNode.nodeGeoPoint))
            )

            projection.first == ProjectionResult.CENTER -> {
                val startRoute = ArrayList<GeoPoint>()
                val endRoute = ArrayList<GeoPoint>()
                targetNearNode.edgeIndex?.let {
                    startRoute.addAll(edgeRight(targetNearNode.gridIndex, it))
                }

                endRoute.add(targetNearNode.nodeGeoPoint)

                startNearNode.edgeIndex?.let {
                    endRoute.addAll(edgeLeft(startNearNode.gridIndex, it))
                }
                if (endRoute.isEmpty()) {
                    endRoute.add(start)
                }
                val routeAssembled = ArrayList<GeoPoint>()
                if (startRoute.isNotEmpty()) {
                    routeAssembled.addAll(startRoute)
                }
                if (endRoute.isNotEmpty()) {
                    routeAssembled.addAll(endRoute)
                }
                return route(
                    startNearNode.searchGeoPoint,
                    targetNearNode.searchGeoPoint,
                    listOf(routeAssembled)
                )
            }

            else -> {
                val route = if (startNearNode.distanceNodeEdge() < targetNearNode.distanceNodeEdge()) {
                    startNearNode.edgeIndex?.let {
                        edgeRight(startNearNode.gridIndex, it)
                    }
                } else {
                    targetNearNode.edgeIndex?.let {
                        edgeLeft(targetNearNode.gridIndex, it)
                    }
                }
                route?.let {
                    route(startNearNode.searchGeoPoint, targetNearNode.searchGeoPoint, listOf(it))
                } ?: run {
                    emptyList()
                }
            }
        }
    }

    @Suppress("NestedBlockDepth", "ComplexMethod", "LongMethod")
    private fun reconstructRoute(
        startNearNode: NearNode,
        targetNearNode: NearNode
    ): List<GeoPoint> {
        state.update { RoutingState.Reconstructing }

        // Buffers for removed edge points in a grid to reconstruct points
        var mapGrid2: MapGrid2? = null
        var indexGridNode = targetNearNode.getIndex()
        val listHighway = ArrayList<ArrayList<GeoPoint>>()

        while (indexGridNode != startNearNode.getIndex()) {
            if (state.value == RoutingState.Canceling) {
                canceledRouting()
                return emptyList()
            }
            val highway = ArrayList<GeoPoint>()
            val extract = RoutingUtils.extractGridIndexNodeIndex(indexGridNode)
            val gridIndex = extract.gridIndex
            val nodeIndex = extract.nodeIndex
            getGrid(gridIndex)?.let { mapGrid ->
                if (nodeIndex == targetNearNode.nodeIndex) {
                    targetNearNode.edgeIndex?.let {
                        val e = edgeRight(gridIndex, it)
                        if (e.contains(targetNearNode.edgeGeoPoint)) {
                            highway.addAll(edgeRight(gridIndex, it))
                        }
                    }
                }

                // Route point coordinates (for view)
                val nodeGeoPoint = getNodeGeoPoint(mapGrid, nodeIndex)
                if (listHighway.isEmpty()) {
                    listHighway.add(highway)
                    highway.add(nodeGeoPoint)
                } else {
                    listHighway.last().add(nodeGeoPoint)
                    listHighway.add(highway)
                    highway.add(nodeGeoPoint)
                }
            }
            routingGridBuffers[gridIndex]?.let { mapGridRouting ->

                val pre = mapGridRouting.nodesPreBuffer[nodeIndex]
                val edge = mapGridRouting.nodesRouteEdges[nodeIndex]
                val extractPre = RoutingUtils.extractGridIndexNodeIndex(pre)
                val preGridIndex = extractPre.gridIndex

                // Reconstruct deleted points on edge

                if (mapGrid2?.gridIndex != preGridIndex) {
                    // Load deleted edge points
                    mapGrid2 = loadGrid2(preGridIndex)
                }
                mapGrid2?.let { mapGrid2 ->
                    // Reconstruct deleted points
                    val listEdgeGeoPoint: MutableList<GeoPoint> = ArrayList()
                    var indexEdgeRemove = mapGrid2.edgeCoordsOffsets[edge]
                    @Suppress("ComplexCondition")
                    while (edge + 1 < mapGrid2.edgeCoordsOffsets.size &&
                        indexEdgeRemove < mapGrid2.edgeCoordsOffsets[edge + 1] ||
                        edge + 1 == mapGrid2.edgeCoordsOffsets.size &&
                        indexEdgeRemove < mapGrid2.edgeCoordsLat.size
                    ) {
                        val edgeRemoveGeoPoint =
                            GeoPoint(
                                mapGrid2.edgeCoordsLat[indexEdgeRemove].toDouble(),
                                mapGrid2.edgeCoordsLon[indexEdgeRemove].toDouble()
                            )
                        listEdgeGeoPoint.add(edgeRemoveGeoPoint)

                        indexEdgeRemove++
                    }
                    listEdgeGeoPoint.reverse()
                    highway.addAll(listEdgeGeoPoint)
                }
                // Go one step back
                indexGridNode = pre
            } ?: run {
                requestCancelRouting()
            }
        }
        val extractPre = RoutingUtils.extractGridIndexNodeIndex(indexGridNode)
        val gridIndex = extractPre.gridIndex
        val nodeIndex = extractPre.nodeIndex
        getGrid(gridIndex)?.let { mapGrid ->
            val nodeGeoPoint = getNodeGeoPoint(mapGrid, nodeIndex)
            listHighway.last().add(nodeGeoPoint)
        }

        if (nodeIndex == startNearNode.nodeIndex && !listHighway.last()
                .contains(startNearNode.edgeGeoPoint) && startNearNode.edgeIndex != null
        ) {
            val highway = ArrayList<GeoPoint>()
            val edges = edgeLeft(gridIndex, startNearNode.edgeIndex)
            highway.addAll(edges)
            listHighway.add(highway)
        }

        // Repositioning of nodes and edges
        return route(startNearNode.searchGeoPoint, targetNearNode.searchGeoPoint, listHighway)
    }

    private fun route(
        startGeoPoint: GeoPoint,
        targetGeoPoint: GeoPoint,
        route: List<List<GeoPoint>>
    ): List<GeoPoint> {
        val highway = ArrayList<GeoPoint>()
        route.reversed().forEachIndexed { index, list ->
            val reverse = list.reversed()
            if (index == 0) {
                if (route.size == 1) {
                    highway.addAll(reverse.symplify(startGeoPoint, targetGeoPoint))
                } else {
                    highway.addAll(reverse.symplify(startGeoPoint, reverse.last()))
                }
            } else if (index == route.size - 1) {
                highway.remove(reverse.first())
                highway.addAll(reverse.symplify(reverse.first(), targetGeoPoint))
            } else {
                highway.remove(reverse.first())
                highway.addAll(reverse)
            }
        }
        return highway
    }

    private fun edgeRight(gridIndex: Int, edgeIndex: Int): List<GeoPoint> {
        val mapGrid2 = loadGrid2(gridIndex)
        val coordsRemt: MutableList<GeoPoint> = ArrayList()
        mapGrid2?.let {
            var iEdgeRemt = it.edgeCoordsOffsets[edgeIndex]
            @Suppress("ComplexCondition")
            while (edgeIndex + 1 < it.edgeCoordsOffsets.size && iEdgeRemt < it.edgeCoordsOffsets[edgeIndex + 1] ||
                edgeIndex + 1 == it.edgeCoordsOffsets.size && iEdgeRemt < it.edgeCoordsLat.size
            ) {
                val coordRem = GeoPoint(
                    it.edgeCoordsLat[iEdgeRemt].toDouble(),
                    it.edgeCoordsLon[iEdgeRemt].toDouble()
                )
                coordsRemt.add(coordRem)

                iEdgeRemt++
            }
            coordsRemt.reverse()
        }
        return coordsRemt
    }

    private fun edgeLeft(gridIndex: Int, edgeIndex: Int): List<GeoPoint> {
        val mapGrid2 = loadGrid2(gridIndex)
        val coordsRemt: MutableList<GeoPoint> = ArrayList()
        mapGrid2?.let {
            var iEdgeRemt = it.edgeCoordsOffsets[edgeIndex + 1] - 1
            @Suppress("ComplexCondition")
            while (edgeIndex < it.edgeCoordsOffsets.size && iEdgeRemt >= it.edgeCoordsOffsets[edgeIndex] ||
                edgeIndex == it.edgeCoordsOffsets.size && iEdgeRemt >= 0
            ) {
                val coordRem = GeoPoint(
                    it.edgeCoordsLat[iEdgeRemt].toDouble(),
                    it.edgeCoordsLon[iEdgeRemt].toDouble()
                )
                coordsRemt.add(coordRem)

                iEdgeRemt--
            }
            coordsRemt.reverse()
        }
        return coordsRemt
    }

    @Suppress("NestedBlockDepth")
    private fun visitNode(targetNearNode: NearNode): Boolean {
        val extract = RoutingUtils.extractGridIndexNodeIndex(visNodeGridIndex)
        visGridIndex = extract.gridIndex
        visNodeIndex = extract.nodeIndex
        if (visGridIndex != oldVisGridIndex) {
            oldVisGridIndex = visGridIndex
            visGrid = getGrid(visGridIndex)
            visGridRB = routingGridBuffers[visGridIndex]
            if (visGridRB == null) {
                visGrid?.let {
                    visGridRB = MapGridRoutingBuffer(it.nodeCount).apply {
                        routingGridBuffers[visGridIndex] = this
                    }
                }
            }
            gridChanges++
        } else {
            gridStays++
        }
        // Update visitTimestamp to mark that grid is still in use
        visGrid?.let {
            it.visitTimestamp = ++gridVisitTimestamp
        }

        // Mark as closed/visited
        visGridRB?.let {
            it.nodesRouteClosedList[visNodeIndex] = true
        }
        openList.remove(visNodeGridIndex)
        visitedCount++

        // Check if found
        if (visNodeGridIndex == targetNearNode.getIndex()) {
            // Found! Return
            if (BuildConfig.DEBUG) {
                Timber.i("Found after $visitedCount nodes visited. ${routeDistHeap?.size} still in heap")
            }
            return true
        }

        return false
    }

    @Suppress("LoopWithTooManyJumpStatements", "NestedBlockDepth", "ComplexMethod", "LongMethod")
    private fun visitNodeEdges(
        targetNearNode: NearNode,
        visGrid: MapGrid,
        visGridRB: MapGridRoutingBuffer
    ) {
        // Get distance of node from start, remove and get index
        val nodeCost = visGridRB.nodesRouteCosts[visNodeIndex]

        // Iterate over edges to neighbors
        var iEdge = visGrid.nodesEdgeOffset[visNodeIndex]
        @Suppress("ComplexCondition")
        while (visNodeIndex + 1 < visGrid.nodesEdgeOffset.size && iEdge < visGrid.nodesEdgeOffset[visNodeIndex + 1] ||
            visNodeIndex + 1 == visGrid.nodesEdgeOffset.size && iEdge < visGrid.edgeCount // Last node in offset array
        ) {
            // Skip if edge not accessible
            if (visGrid.edgesInfobits[iEdge] and edgeFilterBitMask != edgeFilterBitValue) {
                iEdge++
                continue
            }

            // Get neighbor
            val neighborNodeGridIndex = visGrid.edgesTargetNodeGridIndex[iEdge]
            val extract = RoutingUtils.extractGridIndexNodeIndex(neighborNodeGridIndex)
            val neighborGridIndex = extract.gridIndex
            val neighborNodeIndex = extract.nodeIndex

            // Skip loop edges
            if (neighborNodeGridIndex == visNodeGridIndex) {
                Timber.e("Warning: Loop edge - skipping")
                iEdge++
                continue
            }

            // Get neighbor grid routing buffer
            val neighborMapGrid: MapGrid? = if (neighborGridIndex == visGridIndex) {
                visGrid
            } else {
                getGrid(neighborGridIndex)
            }
            neighborMapGrid?.let {
                // Get neighbor grid routing buffer
                var nbGridRB: MapGridRoutingBuffer?
                if (neighborGridIndex == visGridIndex) {
                    nbGridRB = visGridRB
                } else {
                    nbGridRB = routingGridBuffers[neighborGridIndex]
                    if (nbGridRB == null) {
                        nbGridRB = MapGridRoutingBuffer(it.nodeCount).apply {
                            routingGridBuffers[neighborGridIndex] = this
                        }
                    }
                }

                // Continue if target node node already in closed list
                if (!nbGridRB.nodesRouteClosedList[neighborNodeIndex]) {
                    // Distance/Time calculation, depending on routing mode
                    val nbCost = nodeCost + calcNodeDist(visGrid, iEdge)

                    // Caching h or holding visited in a nodes does not make sense
                    // Re-visiting rate seems to be below 1:10 and maps get very slow and memory consuming
                    val h = distanceBetween(
                        getNodeGeoPoint(neighborMapGrid, neighborNodeIndex),
                        targetNearNode.nodeGeoPoint,
                    )

                    if (openList.contains(neighborNodeGridIndex)) {
                        hReuse++
                        // Point open and not closed - update if necessary
                        if (routeDistHeap?.decreaseKeyIfSmaller(
                                neighborNodeGridIndex,
                                nbCost + h.toFloat()
                            ) == true
                        ) {
                            nbGridRB.nodesPreBuffer[neighborNodeIndex] = visNodeGridIndex
                            nbGridRB.nodesRouteEdges[neighborNodeIndex] = iEdge
                            nbGridRB.nodesRouteCosts[neighborNodeIndex] = nbCost
                        }
                        againVisits++
                    } else {
                        // Point not found yet - add to heap and open list
                        hCalc++
                        // Add
                        routeDistHeap?.add(neighborNodeGridIndex, nbCost + h.toFloat())
                        nbGridRB.nodesPreBuffer[neighborNodeIndex] = visNodeGridIndex
                        nbGridRB.nodesRouteEdges[neighborNodeIndex] = iEdge
                        nbGridRB.nodesRouteCosts[neighborNodeIndex] = nbCost
                        openList.add(neighborNodeGridIndex)
                        firstVisits++
                    }
                }
            }
            iEdge++
        }
    }

    private fun calcNodeDist(visGrid: MapGrid, iEdge: Int): Float = visGrid.edgesLengths[iEdge]

    fun requestCancelRouting() {
        state.update { RoutingState.Canceling }
    }

    private fun canceledRouting() {
        state.update { RoutingState.Standby }
    }
}
