package com.ravenfeld.maps.data.service

import android.content.Context
import fi.iki.elonen.NanoHTTPD
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream

private const val PORT = 22_222

class LocalWebServer(context: Context) : NanoHTTPD(PORT) {
    private val mRootDir: File? = context.getExternalFilesDir(null)

    @Suppress("ReturnCount")
    override fun serve(session: IHTTPSession): Response {
        var mimeType = MIME_HTML
        val method = session.method
        val uri = session.uri
        if (method.toString().equals("GET", ignoreCase = true)) {
            val path: String = uri
            var src: File? = null

            if (path.endsWith(".fit") || path.endsWith(".FIT")) {
                src = File(mRootDir, path)
                mimeType = MIME_FIT
            }

            if (src == null || !src.isFile) {
                return newFixedLengthResponse(Response.Status.NOT_FOUND, "text/html", "Not found")
            }
            return try {
                // Open file from SD Card
                val descriptor: InputStream = FileInputStream(src)
                newFixedLengthResponse(Response.Status.OK, mimeType, descriptor, src.length())
            } catch (ioe: IOException) {
                errorResponse(ioe)
            }
        }
        return newFixedLengthResponse(Response.Status.NOT_FOUND, "text/html", "Not found")
    }

    private fun errorResponse(e: Exception): Response = newFixedLengthResponse(
        Response.Status.NOT_FOUND,
        MIME_JSON,
        "{ \"error\" : \"$e\" } "
    )

    companion object {
        private const val MIME_JSON = "application/json"
        private const val MIME_FIT = "application/fit"
    }
}
