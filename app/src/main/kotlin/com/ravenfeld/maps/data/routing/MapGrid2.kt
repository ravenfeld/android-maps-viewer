package com.ravenfeld.maps.data.routing

import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.ObjectInputStream

@Suppress("DataClassShouldBeImmutable")
data class MapGrid2(val gridIndex: Int, val gridFile: File?) {

    // Indicates if grid data is loaded
    var isLoaded: Boolean = false

    val edgeCoordsOffsets: IntArray
    val edgeCoordsLat: FloatArray
    val edgeCoordsLon: FloatArray

    /**
     * Creates loaded map grid
     */
    init {
        var gridReader: ObjectInputStream? = null
        try {
            gridReader = ObjectInputStream(BufferedInputStream(FileInputStream(gridFile)))
            edgeCoordsOffsets = gridReader.readObject() as IntArray
            edgeCoordsLat = gridReader.readObject() as FloatArray
            edgeCoordsLon = gridReader.readObject() as FloatArray
            gridReader.close()
            isLoaded = true
        } finally {
            gridReader?.close()
        }
    }
}
