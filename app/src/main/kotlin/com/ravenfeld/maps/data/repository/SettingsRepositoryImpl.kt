package com.ravenfeld.maps.data.repository

import com.ravenfeld.maps.data.preference.Preference
import com.ravenfeld.maps.domain.place.model.Type
import com.ravenfeld.maps.domain.routing.RoutingMode
import com.ravenfeld.maps.domain.settings.SettingsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SettingsRepositoryImpl @Inject constructor(private val preference: Preference) : SettingsRepository {
    override fun getRoutingMode(): RoutingMode = preference.getRoutingMode()
    override fun getFlowRoutingMode(): Flow<RoutingMode> = preference.getFlowRoutingMode()
    override fun setRoutingMode(routingMode: RoutingMode) = preference.setRoutingMode(routingMode)
    override fun getMapsOfflineEnable() = preference.getMapsOfflineEnable()
    override fun setMapsOfflineEnable(enable: Boolean) = preference.setMapsOfflineEnable(enable)
    override fun getFlowMapsOfflineEnable(): Flow<Boolean> = preference.getFlowMapsOfflineEnable()
    override fun getDisplayPlace(): List<Type> = preference.getDisplayPlace()
    override fun setDisplayPlace(displayTypes: List<Type>) = preference.setDisplayPlace(displayTypes)
}
