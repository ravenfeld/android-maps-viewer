package com.ravenfeld.maps.data.remote.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OverpassResponseDto(
    @SerialName("elements")
    val elements: List<ElementDto>
)

@Serializable
data class ElementDto(
    @SerialName("lat")
    val latitude: Double? = null,
    @SerialName("lon")
    val longitude: Double? = null,
    @SerialName("geometry")
    val geometry: List<LatLngDto>? = null,
    @SerialName("tags")
    val tags: Map<String, String>? = null
)

@Serializable
data class LatLngDto(
    @SerialName("lat")
    val latitude: Double,
    @SerialName("lon")
    val longitude: Double
)
