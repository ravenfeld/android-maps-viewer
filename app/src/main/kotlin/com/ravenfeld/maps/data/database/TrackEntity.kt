package com.ravenfeld.maps.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ravenfeld.maps.domain.geo.Coordinate
import com.ravenfeld.maps.domain.geo.CoordinateLegacy
import com.ravenfeld.maps.domain.geo.Track
import com.ravenfeld.maps.domain.geo.TrackPoint
import kotlinx.serialization.json.Json
import java.io.File
import java.util.*

@Suppress("DataClassShouldBeImmutable", "DataClassContainsFunctions")
@Entity(tableName = "track")
data class TrackEntity(
    val name: String,
    val path: String,
    val visible: Boolean = true,
    val color: String,
    val created: Date = Calendar.getInstance().time,
    val updated: Date = Calendar.getInstance().time
) {

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    fun toTrack(coordinate: Coordinate? = null) = Track(
        id,
        name,
        visible,
        color,
        coordinate ?: coordinate(),
        updated
    )

    @Suppress("NestedBlockDepth")
    private fun coordinate(): Coordinate {
        val file = File(path)
        return if (file.exists()) {
            val json = Json {
                ignoreUnknownKeys = true
                encodeDefaults = true
                coerceInputValues = true
            }
            val fileText = file.readText()
            val decodeCoordinate =
                runCatching {
                    json.decodeFromString(Coordinate.serializer(), fileText)
                }.getOrElse {
                    val coordinateLegacy = json.decodeFromString(CoordinateLegacy.serializer(), fileText)
                    Coordinate(
                        waypoints = coordinateLegacy.waypoints,
                        segments = coordinateLegacy.segments.map { list ->
                            list.map {
                                TrackPoint(
                                    geoPoint = it
                                )
                            }
                        }.toMutableList(),
                        pois = coordinateLegacy.pois
                    )
                }
            // Code for backward compatibility
            if (decodeCoordinate.waypoints.isEmpty()) {
                val waypoints =
                    when (decodeCoordinate.segments.size) {
                        0 -> mutableListOf()
                        1 -> {
                            val segment = decodeCoordinate.segments.first()
                            when (segment.size) {
                                0 -> mutableListOf()
                                1 -> mutableListOf(segment.first())
                                else -> mutableListOf(segment.first(), segment.last())
                            }
                        }

                        else -> mutableListOf(
                            decodeCoordinate.segments.first().first(),
                            decodeCoordinate.segments.last().last()
                        )
                    }.map { it.geoPoint }
                decodeCoordinate.waypoints.addAll(waypoints)
                decodeCoordinate.segments.add(0, listOf(TrackPoint(geoPoint = waypoints.first())))
                decodeCoordinate
            } else {
                decodeCoordinate
            }
        } else {
            Coordinate(mutableListOf(), mutableListOf(), mutableListOf())
        }
    }
}
