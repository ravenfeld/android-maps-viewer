package com.ravenfeld.maps.data.repository

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.text.TextUtils
import androidx.annotation.RequiresApi
import com.garmin.android.connectiq.ConnectIQ
import com.garmin.android.connectiq.ConnectIQ.IQSdkErrorStatus
import com.garmin.android.connectiq.IQApp
import com.garmin.android.connectiq.IQDevice
import com.ravenfeld.maps.R
import com.ravenfeld.maps.data.database.TrackColorUpdateEntity
import com.ravenfeld.maps.data.database.TrackEntity
import com.ravenfeld.maps.data.database.TrackNameUpdateEntity
import com.ravenfeld.maps.data.database.TrackUpdateEntity
import com.ravenfeld.maps.data.database.TrackVisibleUpdateEntity
import com.ravenfeld.maps.data.database.UserAction
import com.ravenfeld.maps.data.database.dao.TrackDao
import com.ravenfeld.maps.data.database.dao.UserActionDao
import com.ravenfeld.maps.data.extension.findSliceSegment
import com.ravenfeld.maps.data.mapper.toPoint
import com.ravenfeld.maps.data.remote.GpxStudioBrouterWebService
import com.ravenfeld.maps.data.remote.NetworkHandler
import com.ravenfeld.maps.data.routing.OsmRouting
import com.ravenfeld.maps.data.service.LocalWebServer
import com.ravenfeld.maps.data.utils.RamerDouglasPeucker
import com.ravenfeld.maps.domain.altitude.AltitudeRepository
import com.ravenfeld.maps.domain.event.GetTrackEvent
import com.ravenfeld.maps.domain.event.LoadFileEvent
import com.ravenfeld.maps.domain.geo.Coordinate
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Poi
import com.ravenfeld.maps.domain.geo.PoiSymbol
import com.ravenfeld.maps.domain.geo.Tag
import com.ravenfeld.maps.domain.geo.Track
import com.ravenfeld.maps.domain.geo.TrackPoint
import com.ravenfeld.maps.domain.geo.Way
import com.ravenfeld.maps.domain.geo.distanceBetween
import com.ravenfeld.maps.domain.geo.split
import com.ravenfeld.maps.domain.routing.RoutingMode
import com.ravenfeld.maps.domain.routing.RoutingState
import com.ravenfeld.maps.domain.track.ExportTrackStatus
import com.ravenfeld.maps.domain.track.TrackRepository
import com.ravenfeld.maps.domain.track.UserActionEntity
import dagger.hilt.android.qualifiers.ApplicationContext
import io.ticofab.androidgpxparser.parser.GPXParser
import io.ticofab.androidgpxparser.parser.domain.Gpx
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import org.maplibre.turf.TurfConstants
import org.maplibre.turf.TurfMeasurement
import org.w3c.dom.Document
import org.w3c.dom.NodeList
import org.xml.sax.SAXException
import org.xmlpull.v1.XmlPullParserException
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.Calendar
import java.util.Locale
import javax.inject.Inject
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import kotlin.math.exp
import kotlin.random.Random

private const val APP_ID = "1bd67f23-d0ca-4439-8aa1-11ea7e0c3901"
private const val MESSAGE_SERVER_OK = "SERVER_OK"
private const val MESSAGE_STOP_SERVER = "STOP_SERVER"
private const val MESSAGE_STOP_SERVER_ERROR = "STOP_SERVER_ERROR"
private const val FILE_TRACK = "track.fit"
private const val FILE_GPX_EXTENSION = ".gpx"
private const val FILE_GPX_MIME_TYPE = "application/gpx+xml"
private const val DISTANCE_SPLIT_TRACK = 20.0
private const val TIME_OUT = 30_000L
private const val COORDINATES = "coordinates"
private const val EXTENSION_GPX = "gpx"
private const val EXTENSION_KML = "kml"
private const val EPSILON = 0.00001
private const val MAX_POINTS = 12_500

@Suppress("TooManyFunctions", "LargeClass")
class TrackRepositoryImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val trackDao: TrackDao,
    private val userActionDao: UserActionDao,
    private val gpxStudioBrouterWebService: GpxStudioBrouterWebService,
    private val altitudeRepository: AltitudeRepository,
    private val networkHandler: NetworkHandler
) : TrackRepository {
    private val loadFileStateFlow = MutableStateFlow<LoadFileEvent?>(null)
    private val statusExportTrackStateFlow = MutableStateFlow(ExportTrackStatus.LOADING)
    private val server = LocalWebServer(context)
    private val routeSolver = OsmRouting(context)

    @Volatile
    private var cacheTracks = hashMapOf<Int, Track>()

    override fun getStateRouting() = combine(
        routeSolver.getState(),
        networkHandler.flowNetworkAvailableFlow()
    ) { routingOfflineState, networkAvailable ->
        routingOfflineState != RoutingState.NotReady || networkAvailable
    }

    override suspend fun loadFile(uri: Uri): Boolean = withContext(Dispatchers.IO) {
        context.contentResolver.query(
            uri,
            arrayOf(
                OpenableColumns.DISPLAY_NAME
            ),
            null,
            null,
            null
        )?.use { cursor ->
            cursor.moveToFirst()
            val fileName = cursor.getString(0)
            val index: Int = fileName.replace(".txt", "").lastIndexOf('.')
            if (index > 0) {
                val extension = fileName.substring(index + 1).lowercase()
                when {
                    extension.contains(EXTENSION_GPX) -> loadGpx(uri)
                    extension.contains(EXTENSION_KML) -> loadKml(fileName, uri)
                    else -> {
                        loadFileStateFlow.emit(LoadFileEvent.ERROR)
                        false
                    }
                }
            } else {
                loadFileStateFlow.emit(LoadFileEvent.ERROR)
                false
            }
        } ?: run {
            loadFileStateFlow.emit(LoadFileEvent.ERROR)
            false
        }
    }

    override fun resetLoadFile() {
        loadFileStateFlow.update { null }
    }

    private inline fun String?.ifNullOrEmpty(defaultValue: () -> String): String = if (this.isNullOrEmpty()) {
        defaultValue()
    } else {
        this
    }

    @Suppress("CyclomaticComplexMethod", "LongMethod")
    private suspend fun loadGpx(uri: Uri): Boolean = withContext(Dispatchers.IO) {
        loadFileStateFlow.emit(LoadFileEvent.LOADING)
        val parser = GPXParser()
        var index = 0
        try {
            val input: InputStream? = context.contentResolver.openInputStream(uri)
            val parsedGpx: Gpx? = parser.parse(input, false) // consider using a background thread
            parsedGpx?.let { gpx ->
                val indexMax = gpx.tracks.size + gpx.routes.size

                val pois = gpx.wayPoints.mapNotNull { waypoint ->
                    waypoint.name?.let {
                        val symbol = waypoint.sym.ifNullOrEmpty { waypoint.type.ifNullOrEmpty { "" } }
                        Poi(
                            name = waypoint.name,
                            point = GeoPoint(
                                latitude = waypoint.latitude,
                                longitude = waypoint.longitude,
                                altitude = waypoint.elevation
                            ),
                            symbol = PoiSymbol.entries.find { it.name == symbol }
                                ?: run {
                                    when (symbol) {
                                        "Alert" -> PoiSymbol.DANGER
                                        "Drinking Water" -> PoiSymbol.WATER
                                        "House" -> PoiSymbol.HOUSE
                                        "Restaurant", "Picnic Area" -> PoiSymbol.FOOD
                                        "Left" -> PoiSymbol.TURN_LEFT
                                        "Right" -> PoiSymbol.TURN_RIGHT
                                        "Straight" -> PoiSymbol.STRAIGHT
                                        else -> PoiSymbol.GENERIC
                                    }
                                }
                        )
                    }
                }
                gpx.tracks.forEach { trackGpx ->
                    trackGpx.trackSegments.forEach { trackSegment ->
                        if (trackSegment.trackPoints.size >= 2) {
                            val name = context.getString(R.string.track)
                            createTrack(
                                name = if (indexMax > 1) {
                                    trackGpx.trackName ?: gpx.metadata.name ?: "$name ($index)"
                                } else {
                                    trackGpx.trackName ?: gpx.metadata.name ?: name
                                },
                                color = getRandomColor(),
                                points = executeRamerDouglasPeucker(
                                    trackSegment.trackPoints.map {
                                        GeoPoint(
                                            latitude = it.latitude,
                                            longitude = it.longitude,
                                            altitude = it.elevation
                                        )
                                    }
                                ),
                                pois = pois
                            )
                            index++
                        }
                    }
                }
                gpx.routes.forEach { route ->
                    if (route.routePoints.size >= 2) {
                        val name = context.getString(R.string.route)
                        createTrack(
                            name = if (indexMax > 1) {
                                route.routeName ?: gpx.metadata.name ?: "$name ($index)"
                            } else {
                                route.routeName ?: gpx.metadata.name ?: name
                            },
                            color = getRandomColor(),
                            points = route.routePoints.map {
                                GeoPoint(
                                    latitude = it.latitude,
                                    longitude = it.longitude,
                                    altitude = it.elevation
                                )
                            },
                            pois = pois
                        )
                        index++
                    }
                }
            }
        } catch (e: FileNotFoundException) {
            Timber.e("File not found fileGpx ${e.message}")
        } catch (e: XmlPullParserException) {
            Timber.e("Parse file fileGpx ${e.message}")
        }
        @Suppress("LabeledExpression") return@withContext if (index == 0) {
            loadFileStateFlow.emit(LoadFileEvent.ERROR)
            false
        } else {
            loadFileStateFlow.emit(LoadFileEvent.SUCCESS)
            true
        }
    }

    private fun executeRamerDouglasPeucker(points: List<GeoPoint>): List<GeoPoint> {
        var epsilon = EPSILON
        var pointsAfterRDP = RamerDouglasPeucker.process(
            points,
            epsilon
        )

        while (pointsAfterRDP.size >= MAX_POINTS) {
            epsilon *= 2
            pointsAfterRDP = RamerDouglasPeucker.process(
                points,
                epsilon
            )
        }
        return pointsAfterRDP
    }

    private suspend fun loadKml(name: String, uri: Uri): Boolean = withContext(Dispatchers.IO) {
        loadFileStateFlow.emit(LoadFileEvent.LOADING)
        var success = false
        try {
            val input: InputStream? = context.contentResolver.openInputStream(uri)
            val docBuilder: DocumentBuilder = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
            val document: Document = docBuilder.parse(input)
            val coordinates: NodeList = document.getElementsByTagName(COORDINATES)
            val points = ArrayList<GeoPoint>()
            for (i in 0 until coordinates.length) {
                val coordinatePairs: List<String> = coordinates.item(i).firstChild.nodeValue.trim()
                    .split(" ")
                for (coord in coordinatePairs) {
                    val splitLine = coord.split(",")
                    val longitude = splitLine[0].toDouble()
                    val latitude = splitLine[1].toDouble()
                    val altitude = splitLine.getOrNull(2).let {
                        if (it.isNullOrEmpty()) {
                            0.0
                        } else {
                            it.toDouble()
                        }
                    }
                    points.add(GeoPoint(latitude, longitude, altitude))
                }
            }
            createTrack(
                name = name,
                color = "#FF6520D5",
                points = executeRamerDouglasPeucker(points),
                pois = emptyList()
            )
            success = true
        } catch (e: FileNotFoundException) {
            Timber.e("File not found fileGpx ${e.message}")
        } catch (e: SAXException) {
            Timber.e("Parse file fileGpx ${e.message}")
        } catch (e: IOException) {
            e.printStackTrace()
        }
        if (success) {
            loadFileStateFlow.emit(LoadFileEvent.SUCCESS)
        } else {
            loadFileStateFlow.emit(LoadFileEvent.ERROR)
        }
        @Suppress("LabeledExpression") return@withContext success
    }

    override suspend fun remove(trackId: Int) = withContext(Dispatchers.IO) {
        cacheTracks.remove(trackId)
        val route = trackDao.getTrack(trackId)
        File(route.trackEntity.path).delete()
        trackDao.delete(trackId)
    }

    override suspend fun color(trackId: Int, color: String) = withContext(Dispatchers.IO) {
        trackDao.update(TrackColorUpdateEntity(trackId, color))
    }

    override suspend fun visible(trackId: Int, visible: Boolean) = withContext(Dispatchers.IO) {
        trackDao.update(TrackVisibleUpdateEntity(trackId, visible))
    }

    override suspend fun name(trackId: Int, name: String) = withContext(Dispatchers.IO) {
        trackDao.update(TrackNameUpdateEntity(trackId, name))
    }

    override fun getLoadFileState(): Flow<LoadFileEvent> = loadFileStateFlow.asStateFlow().filterNotNull()

    override fun getTracks(): Flow<List<Track>> = trackDao.getTracks().distinctUntilChanged()
        .map { list ->
            list.map { trackEntity ->
                if (cacheTracks.contains(trackEntity.id) && trackEntity.updated == cacheTracks.getValue(
                        trackEntity.id
                    ).updated
                ) {
                    cacheTracks.getValue(trackEntity.id)
                } else {
                    val track = trackEntity.toTrack()
                    cacheTracks[trackEntity.id] = track
                    track
                }
            }
        }.flowOn(Dispatchers.IO)

    override suspend fun startTrack(start: GeoPoint): Int {
        val name = SimpleDateFormat(
            "dd/MM HH:mm:ss",
            Locale.getDefault()
        ).format(Calendar.getInstance().time).toString()
        return createTrack(
            name = name,
            color = getRandomColor(),
            points = mutableListOf(start),
            pois = emptyList()
        ).toInt()
    }

    @Suppress("MagicNumber")
    private fun getRandomColor(): String = String.format(
        Locale.getDefault(),
        "#%08X",
        0xFFFFFFFF and Color.rgb(
            Random.nextInt(255), Random.nextInt(255), Random.nextInt(255)
        ).toLong()
    )

    override fun getSegment(
        trackId: Int,
        arrival: GeoPoint,
        backHome: Boolean,
        zoom: Double,
        routingMode: RoutingMode
    ): Flow<GetTrackEvent> = flow {
        val trackEntity = trackDao.getTrack(trackId)
        val track = trackEntity.trackEntity.toTrack()
        val newTrack = track.coordinate.segments.lastOrNull().isNullOrEmpty()
        val newSegment = if (newTrack) {
            listOf(TrackPoint(geoPoint = arrival))
        } else {
            emit(GetTrackEvent.LOADING)
            val departure = track.coordinate.segments.last().last()
            getSegment(
                departure = departure.geoPoint,
                arrival = arrival,
                zoom = zoom,
                routingMode = routingMode
            )
        }?.toMutableList()
        if (newSegment != null) {
            if (track.coordinate.segments.size <= 1) {
                track.coordinate.segments.clear()
                track.coordinate.segments.add(listOf(newSegment.first()))
                track.coordinate.waypoints.clear()
                track.coordinate.waypoints.add(newSegment.first().geoPoint)
            }
            if (!newTrack) {
                if (backHome) {
                    track.coordinate.waypoints.add(arrival)
                    newSegment.removeAt(newSegment.lastIndex)
                    track.coordinate.segments.add(newSegment + TrackPoint(geoPoint = arrival))
                } else {
                    track.coordinate.waypoints.add(newSegment.last().geoPoint)
                    track.coordinate.segments.add(newSegment)
                }
            }
            userActionDao.insert(
                UserActionEntity(
                    trackId = trackEntity.trackEntity.id,
                    timestamp = Instant.now().toEpochMilli(),
                    type = UserAction.CreateWaypoint(point = track.coordinate.waypoints.last())
                )
            )
            writeTrack(
                path = trackEntity.trackEntity.path,
                trackId = trackEntity.trackEntity.id,
                coordinate = track.coordinate
            )
            emit(GetTrackEvent.SUCCESS)
        } else {
            emit(GetTrackEvent.ERROR)
        }
    }.flowOn(Dispatchers.IO)

    override fun moveWaypoint(
        trackId: Int,
        oldWayPoint: GeoPoint,
        newWayPoint: GeoPoint,
        zoom: Double,
        routingMode: RoutingMode
    ): Flow<GetTrackEvent> = flow {
        emit(GetTrackEvent.LOADING)

        moveWaypointInternal(
            trackId = trackId,
            oldWayPoint = oldWayPoint,
            newWayPoint = newWayPoint,
            zoom = zoom,
            routingMode = routingMode,
            onMoveWaypointFinished = { trackId, waypointFinal ->
                userActionDao.insert(
                    UserActionEntity(
                        trackId = trackId,
                        timestamp = Instant.now().toEpochMilli(),
                        type = UserAction.MoveWaypoint(
                            startPoint = oldWayPoint,
                            endPoint = waypointFinal,
                            zoom = zoom,
                            routingMode = routingMode
                        )
                    )
                )
            }
        )

        emit(GetTrackEvent.SUCCESS)
    }.flowOn(Dispatchers.IO)

    @Suppress("LongParameterList")
    private suspend fun moveWaypointInternal(
        trackId: Int,
        oldWayPoint: GeoPoint,
        newWayPoint: GeoPoint,
        zoom: Double,
        routingMode: RoutingMode,
        onMoveWaypointFinished: (
            suspend (trackId: Int, wayPointFinal: GeoPoint) -> Unit
        )? = null
    ) {
        val trackEntity = trackDao.getTrack(trackId)
        val track = trackEntity.trackEntity.toTrack()

        val index = track.coordinate.waypoints.indexOfLast {
            it.latitude == oldWayPoint.latitude && it.longitude == oldWayPoint.longitude
        }
        when (index) {
            0 -> {
                if (track.coordinate.waypoints.size == 1) {
                    track.coordinate.waypoints[0] = newWayPoint
                    track.coordinate.segments[0] = listOf(TrackPoint(geoPoint = newWayPoint))
                } else {
                    val arrival = track.coordinate.waypoints[1]
                    val list = getSegment(newWayPoint, arrival, zoom, routingMode)
                    list?.let {
                        track.coordinate.waypoints[0] = it.first().geoPoint
                        track.coordinate.segments[0] = listOf(it.first())
                        track.coordinate.segments[1] = it
                    }
                }
            }

            track.coordinate.waypoints.size - 1 -> {
                val departure = track.coordinate.waypoints[index - 1]

                val list = getSegment(departure, newWayPoint, zoom, routingMode)
                list?.let {
                    track.coordinate.waypoints[index] = it.last().geoPoint
                    track.coordinate.segments[index] = it
                }
            }

            else -> {
                val departure = track.coordinate.waypoints[index - 1]
                val arrival = track.coordinate.waypoints[index + 1]
                track.coordinate.waypoints[index] = newWayPoint
                val list1 = getSegment(departure, newWayPoint, zoom, routingMode)
                list1?.let {
                    track.coordinate.waypoints[index] = it.last().geoPoint
                    track.coordinate.segments[index] = it
                }

                val list2 = getSegment(newWayPoint, arrival, zoom, routingMode)
                list2?.let {
                    track.coordinate.segments[index + 1] = it
                }
            }
        }
        writeTrack(
            path = trackEntity.trackEntity.path,
            trackId = trackId,
            coordinate = track.coordinate
        )
        onMoveWaypointFinished?.invoke(trackId, track.coordinate.waypoints[index])
    }

    override suspend fun addWaypoint(trackId: Int, waypoint: GeoPoint) {
        val trackEntity = trackDao.getTrack(trackId)
        val track = trackEntity.trackEntity.toTrack()

        track.coordinate.segments.findSliceSegment(cutPoint = waypoint)?.let { sliceSegment ->
            track.coordinate.segments[sliceSegment.index] = sliceSegment.startSegment
            track.coordinate.segments.add(sliceSegment.index + 1, sliceSegment.endSegment)
            track.coordinate.waypoints.add(sliceSegment.index, sliceSegment.geoPoint)

            userActionDao.insert(
                UserActionEntity(
                    trackId = trackEntity.trackEntity.id,
                    timestamp = Instant.now().toEpochMilli(),
                    type = UserAction.CreateWaypoint(point = sliceSegment.geoPoint)
                )
            )

            writeTrack(
                path = trackEntity.trackEntity.path,
                trackId = trackEntity.trackEntity.id,
                coordinate = track.coordinate
            )
        }
    }

    private suspend fun writeTrack(path: String, trackId: Int, coordinate: Coordinate) {
        val file = File(path)

        val json = Json { ignoreUnknownKeys = true }
        val element = json.encodeToString(Coordinate.serializer(), coordinate)
        file.writeText(element)
        trackDao.update(TrackUpdateEntity(trackId, Calendar.getInstance().time))
    }

    private suspend fun getSegment(
        departure: GeoPoint,
        arrival: GeoPoint,
        zoom: Double,
        routingMode: RoutingMode
    ): List<TrackPoint>? = if (routingMode != RoutingMode.FREE && networkHandler.isNetworkAvailable()) {
        gpxStudioBrouterWebService.getTrack(departure, arrival, routingMode).getOrNull()
    } else {
        val route = if (routingMode != RoutingMode.FREE) {
            val route = routeSolver.startCalculateRoute(
                departure,
                arrival
            ).toMutableList()

            if (route.isEmpty()) {
                route.add(departure)
                route.add(arrival)
            }
            route
        } else {
            val list = mutableListOf<GeoPoint>()
            list.add(departure)
            list.add(arrival)
            list
        }.split(DISTANCE_SPLIT_TRACK).run {
            altitudeRepository.getAltitudes(this)
        }.toMutableList()

        val delta = distanceBetween(route.last(), arrival)
        @Suppress("MagicNumber") if (delta >= 150 * exp(-0.1 * zoom)) {
            route.add(arrival.copy(altitude = route.last().altitude))
        }
        route.map {
            TrackPoint(
                geoPoint = it,
                way = Way(
                    start = route.first(),
                    end = route.last(),
                    startDistance = 0.0,
                    endDistance = 0.0,
                    tag = Tag()
                )
            )
        }
    }

    override suspend fun removeLastPoint(trackId: Int) = withContext(Dispatchers.IO) {
        val trackEntity = trackDao.getTrack(trackId)

        val track = trackEntity.trackEntity.toTrack()
        track.coordinate.waypoints.removeAt(track.coordinate.waypoints.lastIndex)
        track.coordinate.segments.removeAt(track.coordinate.segments.lastIndex)

        val file = File(trackEntity.trackEntity.path)

        val json = Json { ignoreUnknownKeys = true }
        val element = json.encodeToString(Coordinate.serializer(), track.coordinate)
        file.writeText(element)
        trackDao.update(TrackUpdateEntity(trackEntity.trackEntity.id, Calendar.getInstance().time))
    }

    override fun cancelLastUserAction(trackId: Int): Flow<GetTrackEvent> = flow {
        emit(GetTrackEvent.LOADING)
        trackDao.cancelLastUserAction(trackId = trackId)
        emit(GetTrackEvent.SUCCESS)
    }.flowOn(Dispatchers.IO)

    @Suppress("LongMethod")
    private suspend fun TrackDao.cancelLastUserAction(trackId: Int) {
        val trackEntity = trackDao.getTrack(trackId)
        val userActionEntity = trackEntity.userActions.lastOrNull()
        when (val userAction = userActionEntity?.type) {
            is UserAction.CreateWaypoint -> {
                val track = trackEntity.trackEntity.toTrack()
                val index = track.coordinate.waypoints.indexOfLast {
                    it == userAction.point || TurfMeasurement.distance(
                        it.toPoint(), userAction.point.toPoint(), TurfConstants.UNIT_METERS
                    ) < 1
                }
                if (index < 0) {
                    userActionDao.delete(userActionEntity.id)
                    this.cancelLastUserAction(trackId)
                } else {
                    if (index == track.coordinate.waypoints.size - 1) {
                        track.coordinate.waypoints.removeAt(track.coordinate.waypoints.lastIndex)
                        track.coordinate.segments.removeAt(track.coordinate.segments.lastIndex)
                    } else {
                        track.coordinate.waypoints.removeAt(index)
                        track.coordinate.segments[index + 1] = track.coordinate.segments[index] +
                            track.coordinate.segments[index + 1]
                        track.coordinate.segments.removeAt(index)
                    }
                    val file = File(trackEntity.trackEntity.path)
                    val json = Json { ignoreUnknownKeys = true }
                    val element = json.encodeToString(Coordinate.serializer(), track.coordinate)
                    file.writeText(element)
                    trackDao.update(
                        TrackUpdateEntity(
                            trackEntity.trackEntity.id,
                            Calendar.getInstance().time
                        )
                    )
                }
            }

            is UserAction.MoveWaypoint -> {
                val track = trackEntity.trackEntity.toTrack()
                val index = track.coordinate.waypoints.indexOfLast { it == userAction.endPoint }
                if (index < 0) {
                    userActionDao.delete(userActionEntity.id)
                    this.cancelLastUserAction(trackId)
                } else {
                    moveWaypointInternal(
                        trackId = trackId,
                        oldWayPoint = userAction.endPoint,
                        newWayPoint = userAction.startPoint,
                        zoom = userAction.zoom,
                        routingMode = userAction.routingMode
                    )
                }
            }

            is UserAction.CreatePoi -> {
                removePoiInternal(
                    trackId = trackId,
                    point = userAction.poi.point
                )
            }

            null -> removeLastPoint(trackId = trackId)
        }
        userActionEntity?.let {
            userActionDao.delete(it.id)
        }
    }

    override suspend fun reverse(routeId: Int) = withContext(Dispatchers.IO) {
        val routeEntity = trackDao.getTrack(routeId)
        val editRoute = routeEntity.trackEntity.toTrack()
        editRoute.coordinate.waypoints.reverse()

        val reverseSegments: MutableList<MutableList<TrackPoint>> = mutableListOf()

        editRoute.coordinate.segments.reversed().forEachIndexed { index, geoPoints ->
            when (index) {
                0 -> {
                    reverseSegments.add(mutableListOf(geoPoints.last()))
                    val newPoints = geoPoints.dropLast(1).toMutableList()
                    if (newPoints.isNotEmpty()) {
                        reverseSegments.add(newPoints.reversed().toMutableList())
                    }
                }

                editRoute.coordinate.segments.size - 1 -> {
                    reverseSegments.last().addAll(geoPoints)
                }

                else -> reverseSegments.add(geoPoints.reversed().toMutableList())
            }
        }

        editRoute.coordinate.segments.clear()
        editRoute.coordinate.segments.addAll(reverseSegments)

        editRoute.coordinate.pois.reverse()

        val file = File(routeEntity.trackEntity.path)

        val json = Json { ignoreUnknownKeys = true }
        val element = json.encodeToString(Coordinate.serializer(), editRoute.coordinate)
        file.writeText(element)
        trackDao.update(TrackUpdateEntity(routeEntity.trackEntity.id, Calendar.getInstance().time))
    }

    override fun sendTrack(name: String, trackId: Int): Flow<ExportTrackStatus> {
        statusExportTrackStateFlow.update { ExportTrackStatus.LOADING }
        val connectIQ = ConnectIQ.getInstance(context, ConnectIQ.IQConnectType.WIRELESS)
        connectIQ.initialize(
            context,
            false,
            object : ConnectIQ.ConnectIQListener {
                override fun onSdkShutDown() {
                    connectIQ.unregisterAllForEvents()
                    server.stop()
                    val fileFIT = File(context.getExternalFilesDir(null)!!.absolutePath + File.separator + FILE_TRACK)
                    fileFIT.delete()
                }

                override fun onInitializeError(p0: IQSdkErrorStatus?) {
                    if (p0 == IQSdkErrorStatus.SERVICE_ERROR) {
                        statusExportTrackStateFlow.update { ExportTrackStatus.ERROR_SERVICE }
                    } else {
                        statusExportTrackStateFlow.update { ExportTrackStatus.ERROR_GARMIN_CONNECT }
                    }
                    server.stop()
                }

                override fun onSdkReady() {
                    val devicesConnected = connectIQ.connectedDevices
                    val devicesUnknown = connectIQ.knownDevices.filter {
                        it.status == IQDevice.IQDeviceStatus.UNKNOWN
                    }
                    if (devicesConnected.isEmpty() && devicesUnknown.isEmpty()) {
                        statusExportTrackStateFlow.update { ExportTrackStatus.ERROR_EMPTY_WATCH }
                        connectIQ.shutdown(context)
                    } else if (devicesConnected.size == 1 || devicesUnknown.size == 1) {
                        CoroutineScope(Job() + Dispatchers.IO).launch {
                            sendFit(
                                connectIQ,
                                if (devicesConnected.isEmpty()) {
                                    devicesUnknown[0]
                                } else {
                                    devicesConnected[0]
                                },
                                name,
                                trackDao.getTrack(trackId).trackEntity.toTrack()
                            )
                        }
                    } else {
                        statusExportTrackStateFlow.update { ExportTrackStatus.ERROR_MANY_WATCH }
                        connectIQ.shutdown(context)
                    }
                }
            }
        )
        return statusExportTrackStateFlow
    }

    @Suppress("TooGenericExceptionCaught", "SwallowedException")
    private suspend fun sendFit(
        connectIQ: ConnectIQ,
        device: IQDevice,
        name: String,
        track: Track
    ) = withContext(Dispatchers.IO) {
        val job: Job = CoroutineScope(Dispatchers.IO).launch {
            delay(TIME_OUT)
            statusExportTrackStateFlow.update { ExportTrackStatus.ERROR_SERVICE }
            connectIQ.shutdown(context)
        }
        val fileFIT = File(context.getExternalFilesDir(null)!!.absolutePath + File.separator + FILE_TRACK)
        track.writeFit(name, fileFIT)
        if (!server.isAlive) {
            server.start()
        }
        connectIQ.getApplicationInfo(
            APP_ID,
            device,
            object : ConnectIQ.IQApplicationInfoListener {
                override fun onApplicationInfoReceived(iqApp: IQApp?) {
                    try {
                        connectIQ.sendMessage(
                            device,
                            iqApp,
                            MESSAGE_SERVER_OK
                        ) { _, _, statusMessage ->
                            if (statusMessage == ConnectIQ.IQMessageStatus.SUCCESS) {
                                job.cancel()
                                statusExportTrackStateFlow.update { ExportTrackStatus.WAITING_WATCH_APP }
                                connectIQ.registerForAppEvents(
                                    device,
                                    iqApp
                                ) { _, _, message, _ ->
                                    if (message.isNotEmpty()) {
                                        if (TextUtils.equals(
                                                message[0].toString(),
                                                MESSAGE_STOP_SERVER
                                            )
                                        ) {
                                            connectIQ.shutdown(context)
                                            statusExportTrackStateFlow.update { ExportTrackStatus.SUCCESS }
                                        } else if (TextUtils.equals(
                                                message[0].toString(),
                                                MESSAGE_STOP_SERVER_ERROR
                                            )
                                        ) {
                                            connectIQ.shutdown(context)
                                            statusExportTrackStateFlow.update { ExportTrackStatus.ERROR_TRANSFER }
                                        }
                                    }
                                }
                            } else {
                                job.cancel()
                                connectIQ.shutdown(context)
                                statusExportTrackStateFlow.update { ExportTrackStatus.ERROR_EMPTY_WATCH }
                            }
                        }
                    } catch (e: Exception) {
                        job.cancel()
                        statusExportTrackStateFlow.update { ExportTrackStatus.ERROR_EMPTY_WATCH }
                    }
                }

                override fun onApplicationNotInstalled(p0: String?) {
                    job.cancel()
                    statusExportTrackStateFlow.update { ExportTrackStatus.ERROR_APP_WATCH_NOT_INSTALLED }
                }
            }
        )
    }

    override fun saveGpxFile(name: String, trackId: Int): Flow<ExportTrackStatus> = flow {
        emit(ExportTrackStatus.LOADING)
        val track = trackDao.getTrack(trackId).trackEntity.toTrack()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            saveFile(track = track, name = name)
        } else {
            saveFileLegacy(track = track, name = name)
        }

        emit(ExportTrackStatus.SUCCESS)
    }.flowOn(Dispatchers.IO)

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun saveFile(track: Track, name: String) {
        val fileName = (name + FILE_GPX_EXTENSION).replace("/", "_").replace("/", "_")
        val values = ContentValues()
        values.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
        values.put(MediaStore.MediaColumns.MIME_TYPE, FILE_GPX_MIME_TYPE)
        values.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS)
        val fileUri = context.contentResolver.insert(
            MediaStore.Downloads.EXTERNAL_CONTENT_URI,
            values
        )
        val outputStream = fileUri?.let {
            context.contentResolver.openOutputStream(fileUri)
        }
        outputStream?.let {
            track.writeGpx(name, it)
        }
        outputStream?.flush()
    }

    @Suppress("DEPRECATION")
    private fun saveFileLegacy(track: Track, name: String) {
        val fileName = (name + FILE_GPX_EXTENSION).replace("/", "_").replace("/", "_")
        val target = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            fileName
        )

        val outputStream = FileOutputStream(target)
        outputStream.let {
            track.writeGpx(name, it)
        }
        outputStream.flush()

        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        mediaScanIntent.data = Uri.fromFile(target)
        context.sendBroadcast(mediaScanIntent)
    }

    override suspend fun savePoi(trackId: Int, poi: Poi) {
        val trackEntity = trackDao.getTrack(trackId)
        val track = trackEntity.trackEntity.toTrack()

        val oldPoi = track.coordinate.pois.find {
            it.point.latitude == poi.point.latitude && it.point.longitude == poi.point.longitude
        }
        oldPoi?.let {
            track.coordinate.pois[track.coordinate.pois.indexOf(it)] = it.copy(
                name = poi.name, symbol = poi.symbol
            )
        } ?: run {
            track.coordinate.segments.findSliceSegment(cutPoint = poi.point)?.let { sliceSegment ->
                track.coordinate.segments[sliceSegment.index] = sliceSegment.startSegment
                track.coordinate.segments.add(sliceSegment.index + 1, sliceSegment.endSegment)
                track.coordinate.waypoints.add(sliceSegment.index, sliceSegment.geoPoint)

                val insertPoi = poi.copy(point = sliceSegment.geoPoint)
                track.coordinate.pois.add(insertPoi)

                userActionDao.insert(
                    UserActionEntity(
                        trackId = trackEntity.trackEntity.id,
                        timestamp = Instant.now().toEpochMilli(),
                        type = UserAction.CreatePoi(poi = insertPoi)
                    )
                )
            }
        }
        writeTrack(
            path = trackEntity.trackEntity.path,
            trackId = trackEntity.trackEntity.id,
            coordinate = track.coordinate
        )
    }

    override suspend fun removePoi(trackId: Int, point: GeoPoint) {
        removePoiInternal(
            trackId = trackId,
            point = point
        )
    }

    private suspend fun removePoiInternal(trackId: Int, point: GeoPoint) {
        val trackEntity = trackDao.getTrack(trackId)
        val track = trackEntity.trackEntity.toTrack()

        val poi = track.coordinate.pois.find {
            it.point == point || TurfMeasurement.distance(
                it.point.toPoint(), point.toPoint(), TurfConstants.UNIT_METERS
            ) < 1
        }
        poi?.let { poiSelected ->
            track.coordinate.pois.remove(poiSelected)

            val index = track.coordinate.waypoints.indexOfLast {
                it == point || TurfMeasurement.distance(
                    it.toPoint(), point.toPoint(), TurfConstants.UNIT_METERS
                ) < 1
            }
            if (index != 0 && index != track.coordinate.waypoints.lastIndex) {
                track.coordinate.waypoints.removeAt(index)
                if (track.coordinate.segments.size > index + 1) {
                    track.coordinate.segments[index + 1] = track.coordinate.segments[index] +
                        track.coordinate.segments[index + 1]
                }
                track.coordinate.segments.removeAt(index)
            }
            val userActionEntity = trackEntity.userActions.find {
                it.type is UserAction.CreatePoi && it.type.poi == poiSelected
            }
            userActionEntity?.let {
                userActionDao.delete(it.id)
            }

            val file = File(trackEntity.trackEntity.path)

            val json = Json { ignoreUnknownKeys = true }
            val element = json.encodeToString(Coordinate.serializer(), track.coordinate)
            file.writeText(element)
            trackDao.update(
                TrackUpdateEntity(
                    trackEntity.trackEntity.id,
                    Calendar.getInstance().time
                )
            )
        }
    }

    private suspend fun createTrack(
        name: String,
        color: String,
        points: List<GeoPoint>,
        pois: List<Poi>,
    ): Long {
        File("${context.filesDir}/tracks").mkdir()

        val waypoints = when (points.size) {
            0 -> mutableListOf()
            1 -> mutableListOf(points.first())
            else -> mutableListOf(points.first(), points.last())
        }

        val segments = when (points.size) {
            0 -> mutableListOf(emptyList())
            1 -> mutableListOf(points.map { TrackPoint(geoPoint = it) })
            else -> mutableListOf(
                listOf(TrackPoint(geoPoint = points.first())),
                points.map { TrackPoint(geoPoint = it) }
            )
        }

        val insertPois: MutableList<Poi> = mutableListOf()
        pois.forEach { poi ->
            segments.findSliceSegment(cutPoint = poi.point)?.let { sliceSegment ->
                if (!sliceSegment.geoPoint.isSamePosition(points.first()) &&
                    !sliceSegment.geoPoint.isSamePosition(points.last())
                ) {
                    insertPois.add(
                        poi.copy(point = sliceSegment.geoPoint)
                    )
                    segments[sliceSegment.index] = sliceSegment.startSegment
                    segments.add(sliceSegment.index + 1, sliceSegment.endSegment)
                    waypoints.add(sliceSegment.index, sliceSegment.geoPoint)
                }
            }
        }

        val trackRoute = Coordinate(waypoints, segments, insertPois)

        val path = "${context.filesDir}/tracks/track_${(trackDao.getTrackMaxId() ?: 0) + 1}"
        val file = File(path)

        val json = Json { ignoreUnknownKeys = true }
        val element = json.encodeToString(Coordinate.serializer(), trackRoute)
        file.writeText(element)
        return trackDao.insert(
            TrackEntity(
                name = name,
                path = path,
                color = color
            )
        )
    }
}
