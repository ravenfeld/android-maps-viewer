package com.ravenfeld.maps.data.repository

import com.ravenfeld.maps.data.database.PresetEntity
import com.ravenfeld.maps.data.database.PresetMapEntity
import com.ravenfeld.maps.data.database.PresetNameUpdateEntity
import com.ravenfeld.maps.data.database.dao.PresetDao
import com.ravenfeld.maps.data.database.dao.PresetMapDao
import com.ravenfeld.maps.data.mapper.toDomain
import com.ravenfeld.maps.data.preference.Preference
import com.ravenfeld.maps.domain.preset.model.Preset
import com.ravenfeld.maps.domain.preset.repository.PresetRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

private const val DEFAULT_ID: Int = 0

class PresetRepositoryImpl @Inject constructor(
    private val presetDao: PresetDao,
    private val presetMapDao: PresetMapDao,
    private val preference: Preference
) : PresetRepository {
    override fun getPresets(): Flow<List<Preset>> =
        combine(
            presetDao.getPresets(),
            preference.getFlowPresetSelectedId()
        ) { presets, selectedId ->
            presets.toDomain(selectedId).toMutableList()
        }

    override suspend fun select(presetId: Int) {
        preference.setPresetSelected(presetId)
    }

    override suspend fun remove(presetId: Int) {
        presetDao.delete(presetId)
        if (preference.getPresetSelectedId() == presetId) {
            preference.setPresetSelected(DEFAULT_ID)
        }
    }

    override suspend fun rename(presetId: Int, name: String) {
        presetDao.update(PresetNameUpdateEntity(id = presetId, name = name))
    }

    override suspend fun insert(name: String) {
        val presetSelected = presetDao.insert(PresetEntity(name = name))
        presetMapDao.getPresetMapSync(preference.getPresetSelectedId()).forEach {
            presetMapDao.insert(
                PresetMapEntity(
                    presetId = presetSelected.toInt(),
                    mapName = it.mapName,
                    enable = it.enable,
                    order = it.order,
                    visible = it.visible,
                    alpha = it.alpha
                )
            )
        }
        preference.setPresetSelected(presetSelected.toInt())
    }

    override suspend fun getPresetSelected(): Preset =
        presetDao.getPreset(preference.getPresetSelectedId()).toDomain(true)
}
