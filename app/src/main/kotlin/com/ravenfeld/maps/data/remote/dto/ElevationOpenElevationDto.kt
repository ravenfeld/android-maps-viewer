package com.ravenfeld.maps.data.remote.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ElevationsOpenElevationDto(
    @SerialName("results")
    val elevations: List<ElevationOpenElevationDto>
)

@Serializable
data class ElevationOpenElevationDto(
    @SerialName("latitude")
    val latitude: Double,
    @SerialName("longitude")
    val longitude: Double,
    @SerialName("elevation")
    val altitude: Double
)
