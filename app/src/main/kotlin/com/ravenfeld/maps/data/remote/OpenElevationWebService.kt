package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.domain.geo.GeoPoint
import java.io.IOException
import java.math.RoundingMode
import javax.inject.Inject
import javax.inject.Singleton

// http://wiki.gis.com/wiki/index.php/Decimal_degrees
private const val MAX_LOCATION_DECIMAL = 6

@Singleton
class OpenElevationWebService
@Inject constructor(
    private val openElevationApi: OpenElevationApi
) {

    suspend fun getElevation(
        point: GeoPoint
    ): Result<Double> =
        when (
            val response = openElevationApi.elevations(
                locations = "${point.latitude.round()},${point.longitude.round()}",
            )
        ) {
            is NetworkResponse.Success ->
                Result.success(
                    response.body.elevations.first().altitude
                )

            is NetworkResponse.Error ->
                Result.failure(response.error ?: IOException("NetworkResponse is on error"))
        }

    suspend fun getElevations(
        points: List<GeoPoint>,
    ): Result<List<GeoPoint>> =
        when (
            val response = openElevationApi.elevations(
                locations = points.joinToString("|") { "${it.latitude.round()},${it.longitude.round()}" },
            )
        ) {
            is NetworkResponse.Success ->
                Result.success(
                    response.body.elevations.map {
                        GeoPoint(
                            latitude = it.latitude,
                            longitude = it.longitude,
                            altitude = it.altitude,
                        )
                    }
                )

            is NetworkResponse.Error ->
                Result.failure(response.error ?: IOException("NetworkResponse is on error"))
        }

    private fun Double.round(numberOfDecimal: Int = MAX_LOCATION_DECIMAL): Double =
        this.toBigDecimal().setScale(numberOfDecimal, RoundingMode.HALF_EVEN).toDouble()
}
