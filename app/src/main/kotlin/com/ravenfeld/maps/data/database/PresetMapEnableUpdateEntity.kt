package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PresetMapEnableUpdateEntity(
    @PrimaryKey val presetId: Int,
    @PrimaryKey val mapName: String,
    val enable: Boolean
)
