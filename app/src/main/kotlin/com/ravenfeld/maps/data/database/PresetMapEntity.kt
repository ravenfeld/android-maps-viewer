package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "preset_map",
    foreignKeys = [
        ForeignKey(
            entity = PresetEntity::class,
            parentColumns = ["id"],
            childColumns = ["presetId"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = MapEntity::class,
            parentColumns = ["name"],
            childColumns = ["mapName"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    primaryKeys = ["presetId", "mapName"]
)
data class PresetMapEntity(
    val presetId: Int,
    val mapName: String,
    val enable: Boolean,
    val order: Int,
    val visible: Boolean,
    val alpha: Float = 0f,
)
