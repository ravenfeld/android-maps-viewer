package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.domain.geo.GeoPoint
import java.io.IOException
import java.math.RoundingMode
import javax.inject.Inject
import javax.inject.Singleton

// http://wiki.gis.com/wiki/index.php/Decimal_degrees
private const val MAX_LOCATION_DECIMAL = 6

private const val IGN_ZONE_NOT_COVERED = -99999

@Singleton
class IgnWebService
@Inject constructor(
    private val ignApi: IgnApi
) {

    suspend fun getElevation(
        point: GeoPoint
    ): Result<Double> =
        when (
            val response = ignApi.elevation(
                latitude = point.latitude.round(),
                longitude = point.longitude.round()
            )
        ) {
            is NetworkResponse.Success -> {
                if (response.body.elevations.first().altitude > IGN_ZONE_NOT_COVERED) {
                    Result.success(
                        response.body.elevations.first().altitude
                    )
                } else {
                    Result.failure(IOException("No altitude value"))
                }
            }

            is NetworkResponse.Error ->
                Result.failure(response.error ?: IOException("NetworkResponse is on error"))
        }

    suspend fun getElevations(
        points: List<GeoPoint>,
    ): Result<List<GeoPoint>> {
        val latitudes = points.map { it.latitude }
        val longitudes = points.map { it.longitude }
        return when (
            val response = ignApi.elevations(
                latitudes = latitudes.map { it.round() }.joinToString("|"),
                longitudes = longitudes.map { it.round() }.joinToString("|")
            )
        ) {
            is NetworkResponse.Success -> {
                if (response.body.elevations.all { it.altitude > IGN_ZONE_NOT_COVERED }) {
                    Result.success(
                        response.body.elevations.map {
                            GeoPoint(
                                latitude = it.latitude,
                                longitude = it.longitude,
                                altitude = it.altitude,
                            )
                        }
                    )
                } else {
                    Result.failure(IOException("No altitudes value"))
                }
            }

            is NetworkResponse.Error ->
                Result.failure(response.error ?: IOException("NetworkResponse is on error"))
        }
    }

    private fun Double.round(numberOfDecimal: Int = MAX_LOCATION_DECIMAL): Double =
        this.toBigDecimal().setScale(numberOfDecimal, RoundingMode.HALF_EVEN).toDouble()
}
