package com.ravenfeld.maps.data.mapper

import com.ravenfeld.maps.domain.geo.GeoPoint
import org.maplibre.geojson.Point

internal fun List<GeoPoint>.toPoint() = map(GeoPoint::toPoint)

internal fun GeoPoint.toPoint() = Point.fromLngLat(
    this.longitude,
    this.latitude,
    this.altitude
        ?: 0.0
)
