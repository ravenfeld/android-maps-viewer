package com.ravenfeld.maps.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ravenfeld.maps.domain.track.UserActionEntity

@Dao
interface UserActionDao {

    @Insert
    suspend fun insert(userAction: UserActionEntity): Long

    @Query("DELETE FROM user_action WHERE id = :id")
    suspend fun delete(id: Int)
}
