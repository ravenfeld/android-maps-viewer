package com.ravenfeld.maps.data.service

import android.content.Context
import android.content.pm.ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC
import android.os.Build
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkerParameters
import com.ravenfeld.maps.R
import com.ravenfeld.maps.data.Const.FOLDER_TILES
import com.ravenfeld.maps.data.database.MapEntity
import com.ravenfeld.maps.data.database.PresetMapEntity
import com.ravenfeld.maps.data.database.dao.MapDao
import com.ravenfeld.maps.data.database.dao.PresetDao
import com.ravenfeld.maps.data.database.dao.PresetMapDao
import com.ravenfeld.maps.data.remote.DownloadFileWebService
import com.ravenfeld.maps.domain.map.getTiles
import com.ravenfeld.maps.extension.getExternalFile
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream

const val EXTRA_NAME = "EXTRA_NAME"
const val EXTRA_MAPS_NAME = "EXTRA_MAPS_NAME"
const val EXTRA_URLS = "EXTRA_URLS"
const val EXTRA_NORTH_WEST = "EXTRA_NORTH_WEST"
const val EXTRA_SOUTH_EST = "EXTRA_SOUTH_EST"
const val EXTRA_ZOOMS = "EXTRA_ZOOMS"

@HiltWorker
class DownloadTilesService @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val downloadFileWebService: DownloadFileWebService,
    private val mapDao: MapDao,
    private val presetMapDao: PresetMapDao,
    private val presetDao: PresetDao,
) : CoroutineWorker(appContext, workerParams) {

    private val notificationManager = NotificationManagerCompat.from(appContext)

    @Suppress("MagicNumber", "ComplexMethod", "LongMethod")
    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        runCatching {
            val name = inputData.getString(EXTRA_NAME)
            val mapsName = inputData.getStringArray(EXTRA_MAPS_NAME)
            val baseUrls = inputData.getStringArray(EXTRA_URLS)
            val northWest = inputData.getDoubleArray(EXTRA_NORTH_WEST)
            val southEst = inputData.getDoubleArray(EXTRA_SOUTH_EST)
            val zooms = inputData.getIntArray(EXTRA_ZOOMS)
            val notificationId = name.hashCode()
            @Suppress("ComplexCondition")
            if (baseUrls != null &&
                name != null &&
                mapsName != null &&
                northWest != null &&
                southEst != null &&
                zooms != null
            ) {
                val tiles = getTiles(northWest, southEst, zooms)

                var oldProgress = 0.0f
                var oldName = name
                val numbTiles = tiles.size * baseUrls.size
                val nameFolder = name.replace(" ", "_")
                val pathExternal = applicationContext.getExternalFile()
                showNotification(id = notificationId, name = name, progress = 0)
                baseUrls.forEachIndexed { indexUrl, baseUrl ->
                    val mapName = "$name ${mapsName[indexUrl]}"
                    val mapStreamFolder = mapsName[indexUrl].replace(" ", "_")

                    if (mapDao.getSync(mapName) == null) {
                        tiles.sortedBy { it.z }.forEachIndexed { index, tileCoordinate ->

                            val url = baseUrl
                                .replace("{z}", tileCoordinate.z.toString())
                                .replace("{x}", tileCoordinate.x.toString())
                                .replace("{y}", tileCoordinate.y.toString())
                            val response = downloadFileWebService.downloadTile(url)
                            if (response.isSuccessful) {
                                val inputStream = response.body()!!.byteStream()
                                val pathFolder = pathExternal.absolutePath +
                                    "/$FOLDER_TILES/" +
                                    "$nameFolder/" +
                                    "$mapStreamFolder/" +
                                    "${tileCoordinate.z}/" +
                                    "${tileCoordinate.x}"
                                File(pathFolder).mkdirs()
                                val outputStream = FileOutputStream(
                                    "$pathFolder/" +
                                        "${tileCoordinate.y}.png"
                                )
                                val fileReader = ByteArray(4096)
                                val fileSize: Long = response.body()!!.contentLength()
                                var fileSizeDownloaded: Long = 0

                                while (true) {
                                    val read: Int = inputStream.read(fileReader)
                                    if (read == -1) {
                                        break
                                    }
                                    outputStream.write(fileReader, 0, read)
                                    fileSizeDownloaded += read.toLong()

                                    var progress = index / numbTiles.toFloat()
                                    progress += fileSizeDownloaded.toFloat() / fileSize.toFloat() / numbTiles.toFloat()
                                    progress *= 100
                                    if (progress - oldProgress >= 5 || oldName != mapName) {
                                        oldProgress = progress
                                        oldName = mapName
                                        showNotification(
                                            id = notificationId,
                                            name = "$name (${mapsName[indexUrl]})",
                                            progress = oldProgress.toInt()
                                        )
                                    }
                                }
                                outputStream.flush()
                            }
                            delay(150)
                        }

                        val insertId = mapDao.insert(
                            MapEntity(
                                name = mapName,
                                path = "file://${pathExternal.absolutePath}/" +
                                    "$FOLDER_TILES/" +
                                    "$nameFolder/" +
                                    "$mapStreamFolder/" +
                                    "{z}/{x}/{y}.png",
                                minZoom = zooms[0].toFloat(),
                                maxZoom = zooms[1].toFloat(),
                                groupName = name
                            )
                        )
                        if (insertId != -1L) {
                            presetMapDao.insert(
                                presetDao.getPresetsSync().map {
                                    PresetMapEntity(
                                        presetId = it.id,
                                        mapName = mapName,
                                        enable = true,
                                        order = mapDao.getCountMaps(),
                                        visible = true,
                                        alpha = 1.0f,
                                    )
                                }
                            )
                        }
                    }
                }
                notificationManager.cancel(notificationId)
            }
        }.fold(
            onSuccess = {
                Result.success()
            },
            onFailure = {
                Result.retry()
            }
        )
    }

    private suspend fun showNotification(
        id: Int,
        name: String,
        progress: Int
    ) {
        val views = RemoteViews(
            applicationContext.packageName,
            R.layout.notification_download
        )
        views.setTextViewText(R.id.notification_title, name)
        @Suppress("MagicNumber")
        views.setProgressBar(R.id.notification_progress, 100, progress, false)
        val customNotification =
            NotificationCompat.Builder(
                applicationContext,
                applicationContext.getString(R.string.notification_channel_id_download)
            )
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setColor(
                    ContextCompat.getColor(applicationContext, R.color.colorIconNotification)
                )
                .setStyle(NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(views)
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setAutoCancel(false)
                .setOngoing(true)
                .build()

        val foregroundInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            ForegroundInfo(id, customNotification, FOREGROUND_SERVICE_TYPE_DATA_SYNC)
        } else {
            ForegroundInfo(id, customNotification)
        }

        setForeground(foregroundInfo)
    }
}
