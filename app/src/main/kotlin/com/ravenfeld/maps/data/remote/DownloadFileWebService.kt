package com.ravenfeld.maps.data.remote

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DownloadFileWebService @Inject constructor(private val downloadFileApi: DownloadFileApi) {

    suspend fun downloadTile(fileUrl: String) = downloadFileApi.downloadFile(fileUrl)
}
