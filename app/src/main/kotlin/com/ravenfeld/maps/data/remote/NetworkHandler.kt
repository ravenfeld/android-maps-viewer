package com.ravenfeld.maps.data.remote

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.net.NetworkCapabilities.NET_CAPABILITY_NOT_SUSPENDED
import android.net.NetworkCapabilities.NET_CAPABILITY_VALIDATED
import android.net.NetworkRequest
import android.os.Build
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkHandler
@Inject constructor(@ApplicationContext private val context: Context) {

    private val networkCallback: ConnectivityManager.NetworkCallback

    private val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private val isNetworkAvailableStateFlow = MutableStateFlow(false)

    init {
        networkCallback = createNetworkCallback()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(networkCallback)
        } else {
            val request = NetworkRequest.Builder()
                .addCapability(NET_CAPABILITY_INTERNET)
                .build()
            connectivityManager.registerNetworkCallback(request, networkCallback)
        }
    }

    private fun createNetworkCallback() = object : ConnectivityManager.NetworkCallback() {

        override fun onCapabilitiesChanged(network: Network, networkCapabilities: NetworkCapabilities) {
            super.onCapabilitiesChanged(network, networkCapabilities)
            isNetworkAvailableStateFlow.update { hasInternet(networkCapabilities) }
        }

        override fun onAvailable(network: Network) {
            val networkCapabilities = connectivityManager.getNetworkCapabilities(network)
            isNetworkAvailableStateFlow.update { hasInternet(networkCapabilities) }
        }

        override fun onLost(network: Network) {
            isNetworkAvailableStateFlow.update { false }
        }
    }

    fun flowNetworkAvailableFlow(): StateFlow<Boolean> = isNetworkAvailableStateFlow.asStateFlow()

    fun isNetworkAvailable(): Boolean = isNetworkAvailableStateFlow.value

    private fun hasInternet(networkCapabilities: NetworkCapabilities?): Boolean {
        val hasInternet = networkCapabilities?.hasCapability(NET_CAPABILITY_INTERNET) == true &&
            networkCapabilities.hasCapability(NET_CAPABILITY_VALIDATED)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            hasInternet && networkCapabilities?.hasCapability(NET_CAPABILITY_NOT_SUSPENDED) == true
        } else {
            hasInternet
        }
    }
}
