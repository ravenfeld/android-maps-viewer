package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class TrackUpdateEntity(
    @PrimaryKey val id: Int,
    val updated: Date
)
