package com.ravenfeld.maps.data.repository

import android.content.Context
import androidx.work.BackoffPolicy
import androidx.work.Constraints
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.WorkRequest
import com.ravenfeld.maps.data.Const.FOLDER_TILES
import com.ravenfeld.maps.data.database.MapEntity
import com.ravenfeld.maps.data.database.MapSettingsUpdateEntity
import com.ravenfeld.maps.data.database.PresetMapAlphaUpdateEntity
import com.ravenfeld.maps.data.database.PresetMapEnableUpdateEntity
import com.ravenfeld.maps.data.database.PresetMapEntity
import com.ravenfeld.maps.data.database.PresetMapOrderUpdateEntity
import com.ravenfeld.maps.data.database.PresetMapVisibleUpdateEntity
import com.ravenfeld.maps.data.database.dao.MapDao
import com.ravenfeld.maps.data.database.dao.PresetDao
import com.ravenfeld.maps.data.database.dao.PresetMapDao
import com.ravenfeld.maps.data.entity.MapLocal
import com.ravenfeld.maps.data.preference.Preference
import com.ravenfeld.maps.data.service.DownloadTilesService
import com.ravenfeld.maps.data.service.EXTRA_MAPS_NAME
import com.ravenfeld.maps.data.service.EXTRA_NAME
import com.ravenfeld.maps.data.service.EXTRA_NORTH_WEST
import com.ravenfeld.maps.data.service.EXTRA_SOUTH_EST
import com.ravenfeld.maps.data.service.EXTRA_URLS
import com.ravenfeld.maps.data.service.EXTRA_ZOOMS
import com.ravenfeld.maps.domain.map.MAX_ZOOM
import com.ravenfeld.maps.domain.map.MIN_ZOOM
import com.ravenfeld.maps.domain.map.Map
import com.ravenfeld.maps.domain.map.MapBase
import com.ravenfeld.maps.domain.map.MapGroup
import com.ravenfeld.maps.domain.map.MapInput
import com.ravenfeld.maps.domain.map.MapRepository
import com.ravenfeld.maps.domain.map.MapStorage
import com.ravenfeld.maps.extension.getExternalFile
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import org.maplibre.android.geometry.LatLng
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val STREAM = "http"
private const val MBTILES = "mbtiles"
private const val FILE = "file"
private const val MBTILES_VECTOR = "_vector.mbtiles"
private const val MAX_ZOOM_OPEN_STREET_MAP = 19f
private const val OPENSTREETMAP = "OpenStreetMap"
private const val URL_OPENSTREETMAP = "https://tile.openstreetmap.org/{z}/{x}/{y}.png"
private const val OPENSTREETMAP_CYCLOSM = "CyclOSM"
private const val URL_OPENSTREETMAP_CYCLOSM = "https://a.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png"
private const val OPEN_TOPO_MAP = "OpenTopoMap"
private const val URL_OPEN_TOPO_MAP = "https://a.tile.opentopomap.org/{z}/{x}/{y}.png"
private const val OPEN_HIKING = "OpenHiking"
private const val URL_OPEN_HIKING = "https://maps.refuges.info/hiking/{z}/{x}/{y}.png"

@Suppress("TooManyFunctions")
class MapRepositoryImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val mapDao: MapDao,
    private val presetMapDao: PresetMapDao,
    private val presetDao: PresetDao,
    private val preference: Preference
) : MapRepository {

    private val mutex = Mutex()

    @Suppress("LongMethod")
    override suspend fun init() = withContext(Dispatchers.IO) {
        val mapInLocal: ArrayList<MapLocal> = ArrayList()
        val mapInDatabase = mapDao.getAllOrderSync()
        if (mapInDatabase.isEmpty()) {
            mapInLocal.add(
                MapLocal(
                    OPENSTREETMAP,
                    URL_OPENSTREETMAP,
                    MIN_ZOOM,
                    MAX_ZOOM_OPEN_STREET_MAP
                )
            )
            mapInLocal.add(
                MapLocal(
                    OPENSTREETMAP_CYCLOSM,
                    URL_OPENSTREETMAP_CYCLOSM,
                    MIN_ZOOM,
                    MAX_ZOOM_OPEN_STREET_MAP
                )
            )
            mapInLocal.add(
                MapLocal(
                    OPEN_TOPO_MAP,
                    URL_OPEN_TOPO_MAP,
                    MIN_ZOOM,
                    MAX_ZOOM_OPEN_STREET_MAP
                )
            )
            mapInLocal.add(
                MapLocal(
                    OPEN_HIKING,
                    URL_OPEN_HIKING,
                    MIN_ZOOM,
                    MAX_ZOOM_OPEN_STREET_MAP
                )
            )
        }
        context.getExternalFilesDirs(null).forEach {
            it?.listFiles()
                ?.filter { file ->
                    file.isFile &&
                        !file.name.startsWith(".") &&
                        file.name.contains(".$MBTILES")
                }
                ?.forEach { file ->
                    mapInLocal.add(
                        MapLocal(
                            file.name.removeSuffix(MBTILES_VECTOR).removeSuffix(".$MBTILES"),
                            "$MBTILES://" + file.path,
                            MIN_ZOOM,
                            MAX_ZOOM
                        )
                    )
                }
        }

        val removeTiles = mapInDatabase
            .filter { it.path.contains(MBTILES) && it.groupName == null }
            .map { it.name }
            .subtract(
                mapInLocal.map { it.name }
                    .toSet()
            )
        mapDao.deleteByNames(removeTiles.toList())

        add(
            mapInLocal.filter { local ->
                mapInDatabase.none { it.name == local.name }
            }
                .map {
                    MapInput(
                        name = it.name,
                        path = it.path,
                        minZoom = it.minZoom,
                        maxZoom = it.maxZoom
                    )
                }
        )
    }

    private suspend fun add(maps: List<MapInput>) = withContext(
        Dispatchers.IO
    ) {
        val countMaps = mapDao.getCountMaps()
        val insertIds = mapDao.insert(
            maps.map {
                MapEntity(
                    name = it.name,
                    path = it.path,
                    minZoom = it.minZoom,
                    maxZoom = it.maxZoom
                )
            }
        )
        val presetMaps = mutableListOf<PresetMapEntity>()
        presetDao.getPresetsSync().forEach { preset ->
            maps.forEachIndexed { index, map ->
                if (insertIds[index] != -1L) {
                    presetMaps.add(
                        PresetMapEntity(
                            presetId = preset.id,
                            mapName = map.name,
                            enable = true,
                            order = countMaps + index,
                            visible = true,
                            alpha = 1.0f,
                        )
                    )
                }
            }
        }
        presetMapDao.insert(presetMaps)
        Unit
    }

    override suspend fun enable(name: String, enable: Boolean) = withContext(Dispatchers.IO) {
        val map = mapDao.getSync(name)
        val presetId = preference.getPresetSelectedId()
        map?.let {
            enablePreset(
                presetId = presetId,
                name = map.name,
                enable = enable
            )
        } ?: run {
            val mapList = mapDao.getGroupNameSync(name)
            mapList.forEach { map ->
                enablePreset(
                    presetId = presetId,
                    name = map.name,
                    enable = enable
                )
            }
        }
        Unit
    }

    private suspend fun enablePreset(presetId: Int, name: String, enable: Boolean) {
        val presetMap = presetMapDao.getPresetMapSync(
            presetId = presetId,
            mapName = name
        )
        presetMap?.let {
            if (it.enable != enable) {
                presetMapDao.update(
                    PresetMapEnableUpdateEntity(
                        presetId = presetId,
                        mapName = name,
                        enable = enable
                    )
                )
            }
        } ?: run {
            presetMapDao.insert(
                PresetMapEntity(
                    presetId = presetId,
                    mapName = name,
                    enable = enable,
                    order = mapDao.getCountMaps(),
                    visible = true,
                    alpha = 1.0f,
                )
            )
        }
    }

    override suspend fun moverOrder(from: Int, to: Int) = withContext(Dispatchers.IO) {
        mutex.withLock {
            val presetId = preference.getPresetSelectedId()
            val mapFrom = presetMapDao.getByOrderSync(presetId = presetId, order = from)
            val mapTo = presetMapDao.getByOrderSync(presetId = presetId, order = to)
            if (mapFrom != null && mapTo != null) {
                val maps = ArrayList(presetMapDao.getAllOrderSync(presetId = presetId))
                val indexFrom = maps.indexOf(mapFrom)
                val indexTo = maps.indexOf(mapTo)
                maps.removeAt(indexFrom)
                maps.add(indexTo, mapFrom)
                presetMapDao.update(
                    maps.mapIndexed { index, map ->
                        PresetMapOrderUpdateEntity(
                            presetId = presetId,
                            mapName = map.mapName,
                            order = index
                        )
                    }
                )
            }
        }
    }

    override suspend fun alpha(name: String, alpha: Float) = withContext(Dispatchers.IO) {
        presetMapDao.update(
            PresetMapAlphaUpdateEntity(
                presetId = preference.getPresetSelectedId(),
                mapName = name,
                alpha = alpha
            )
        )
    }

    override suspend fun visible(name: String, visible: Boolean) = withContext(Dispatchers.IO) {
        presetMapDao.update(
            PresetMapVisibleUpdateEntity(
                preference.getPresetSelectedId(),
                mapName = name,
                visible = visible
            )
        )
    }

    override suspend fun save(
        name: String,
        url: String,
        minZoom: Float,
        maxZoom: Float
    ) = withContext(Dispatchers.IO) {
        mapDao.update(MapSettingsUpdateEntity(name, url, minZoom, maxZoom))
    }

    override suspend fun delete(name: String) = withContext(Dispatchers.IO) {
        val map = mapDao.getSync(name)
        map?.let {
            deleteMap(it)
        } ?: run {
            val mapList = mapDao.getGroupNameSync(name)
            mapList.forEach {
                deleteMap(it)
            }
            File(context.getExternalFile().absolutePath + "/$FOLDER_TILES/$name").delete()
            Unit
        }
    }

    private suspend fun deleteMap(mapEntity: MapEntity) {
        val path = mapEntity.path
        when {
            path.startsWith(MBTILES) -> {
                val file = File(path.removePrefix("$MBTILES://"))
                file.delete()
            }

            path.startsWith(FILE) -> {
                val file = File(path.removePrefix("$FILE://").removeSuffix("/{z}/{x}/{y}.png"))
                file.deleteRecursively()
            }
        }
        mapDao.deleteByName(mapEntity.name)
    }

    override suspend fun add(
        name: String,
        url: String,
        minZoom: Float,
        maxZoom: Float
    ): Boolean = withContext(
        Dispatchers.IO
    ) {
        mutex.withLock {
            val insertId = mapDao.insert(
                MapEntity(
                    name = name,
                    path = url,
                    minZoom = minZoom,
                    maxZoom = maxZoom
                )
            )
            val added = insertId != -1L
            if (added) {
                presetMapDao.insert(
                    presetDao.getPresetsSync().map {
                        PresetMapEntity(
                            presetId = it.id,
                            mapName = name,
                            enable = true,
                            order = mapDao.getCountMaps(),
                            visible = true,
                            alpha = 1.0f,
                        )
                    }
                )
            }
            added
        }
    }

    override suspend fun addOrUpdate(maps: List<MapInput>) = withContext(
        Dispatchers.IO
    ) {
        val countMaps = mapDao.getCountMaps()
        val insertIds = mapDao.insertOrUpdate(
            maps.map {
                MapEntity(
                    name = it.name,
                    path = it.path,
                    minZoom = it.minZoom,
                    maxZoom = it.maxZoom
                )
            }
        )
        val presetMaps = mutableListOf<PresetMapEntity>()
        presetDao.getPresetsSync().forEach { preset ->
            maps.forEachIndexed { index, map ->
                if (insertIds[index] != -1L) {
                    presetMaps.add(
                        PresetMapEntity(
                            presetId = preset.id,
                            mapName = map.name,
                            enable = false,
                            order = countMaps + index,
                            visible = true,
                            alpha = 1.0f,
                        )
                    )
                }
            }
        }
        presetMapDao.insert(presetMaps)
        Unit
    }

    override suspend fun downloadTiles(
        name: String,
        mapsName: List<String>,
        area: Array<LatLng>,
        minZoom: Int,
        maxZoom: Int
    ): Boolean = withContext(Dispatchers.IO) {
        val maps = mapDao.getAllOrderByNameSync(STREAM)
        if (maps.any { it.name == name }) {
            false
        } else {
            val northWest = doubleArrayOf(area[0].latitude, area[0].longitude)
            val southEst = doubleArrayOf(area[2].latitude, area[2].longitude)

            val mapsNameDatabase = maps.filter { mapsName.contains(it.name) }
            val workManager = WorkManager.getInstance(context)
            workManager
                .beginUniqueWork(
                    name,
                    ExistingWorkPolicy.KEEP,
                    OneTimeWorkRequest.Builder(DownloadTilesService::class.java)
                        .setConstraints(
                            Constraints.Builder()
                                .setRequiresCharging(false)
                                .setRequiredNetworkType(NetworkType.CONNECTED)
                                .build()
                        )
                        .setBackoffCriteria(
                            BackoffPolicy.EXPONENTIAL,
                            WorkRequest.MIN_BACKOFF_MILLIS,
                            TimeUnit.MILLISECONDS
                        )
                        .setInputData(
                            Data.Builder()
                                .putString(EXTRA_NAME, name)
                                .putStringArray(
                                    EXTRA_MAPS_NAME,
                                    mapsNameDatabase.map { it.name }.toTypedArray()
                                )
                                .putStringArray(
                                    EXTRA_URLS,
                                    mapsNameDatabase.map { it.path }.toTypedArray()
                                )
                                .putDoubleArray(EXTRA_NORTH_WEST, northWest)
                                .putDoubleArray(EXTRA_SOUTH_EST, southEst)
                                .putIntArray(EXTRA_ZOOMS, intArrayOf(minZoom, maxZoom))
                                .build()
                        )
                        .build()
                ).enqueue()
            true
        }
    }

    override fun getListMapStream(): Flow<List<Map>> = mapDao.getAllOrderByName(STREAM).map {
        it.mapIndexedNotNull { index, mapEntity ->
            val presetId = preference.getPresetSelectedId()
            val presetMap = presetMapDao.getPresetMapSync(
                preference.getPresetSelectedId(),
                mapName = mapEntity.name
            )
            presetMap?.let {
                mapEntity.toMap(
                    mapStorage = filePathToMapStorage(mapEntity.path),
                    enable = presetMap.enable,
                    order = presetMap.order,
                    visible = presetMap.visible,
                    alpha = presetMap.alpha,
                )
            } ?: run {
                presetMapDao.insert(
                    PresetMapEntity(
                        presetId = presetId + index,
                        mapName = mapEntity.name,
                        enable = true,
                        order = mapDao.getCountMaps(),
                        visible = true,
                        alpha = 1.0f,
                    )
                )
                null
            }
        }
    }.distinctUntilChanged()

    @Suppress("CyclomaticComplexMethod")
    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getListMapGroupName(stream: Boolean): Flow<List<MapBase>> =
        combine(
            preference.getFlowPresetSelectedId()
                .flatMapLatest { presetId -> presetMapDao.getPresetMap(presetId) },
            mapDao.getAllOrderByName().map { maps ->
                maps.filter {
                    val mapStorage = filePathToMapStorage(it.path)
                    if (stream) {
                        mapStorage == MapStorage.STREAM
                    } else {
                        mapStorage == MapStorage.FILE || mapStorage == MapStorage.MBTILES_RASTER
                    }
                }
            }
        ) { presets, maps ->
            val hashMap = HashMap<String, MapBase>()
            maps.forEach { mapEntity ->
                val mapStorage = filePathToMapStorage(mapEntity.path)
                val presetMap = presets.find { it.mapName == mapEntity.name }
                if (mapEntity.groupName == null) {
                    hashMap[mapEntity.name] = mapEntity.toMap(
                        mapStorage = mapStorage,
                        enable = presetMap?.enable ?: false,
                        order = presetMap?.order ?: 0,
                        visible = presetMap?.visible ?: true,
                        alpha = presetMap?.alpha ?: 1.0f
                    )
                } else {
                    if (!hashMap.contains(mapEntity.groupName)) {
                        hashMap[mapEntity.groupName] = mapEntity.toMapGroup(
                            mapStorage = MapStorage.GROUP,
                            enable = presetMap?.enable ?: false
                        )
                    }
                    (hashMap[mapEntity.groupName] as MapGroup).enable =
                        (hashMap[mapEntity.groupName] as MapGroup).enable ||
                        presetMap?.enable ?: false
                    (hashMap[mapEntity.groupName] as MapGroup).listMap.add(
                        mapEntity.toMap(
                            mapStorage = mapStorage,
                            enable = presetMap?.enable ?: false,
                            order = presetMap?.order ?: 0,
                            visible = presetMap?.visible ?: true,
                            alpha = presetMap?.alpha ?: 1.0f
                        )
                    )
                }
            }
            hashMap.values.sortedBy { map -> map.name.lowercase() }
        }.distinctUntilChanged()

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getListMapEnable(): Flow<List<Map>> =
        preference.getFlowPresetSelectedId().flatMapLatest { presetId ->
            presetMapDao.getPresetMapEnable(presetId)
                .flatMapLatest { presetMaps ->
                    mapDao.getMapByName(presetMaps.map { it.mapName })
                        .map { maps ->
                            maps.map { mapEntity ->
                                val presetMap = presetMaps.find { it.mapName == mapEntity.name }
                                mapEntity.toMap(
                                    mapStorage = filePathToMapStorage(mapEntity.path),
                                    enable = presetMap?.enable ?: false,
                                    order = presetMap?.order ?: 0,
                                    visible = presetMap?.visible ?: true,
                                    alpha = presetMap?.alpha ?: 1.0f
                                )
                            }.sortedBy { it.order }
                        }
                }
        }
            .distinctUntilChanged()

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getListMapStreamEnable(): Flow<List<Map>> =
        preference.getFlowPresetSelectedId().flatMapLatest { presetId ->
            presetMapDao.getPresetMapEnable(presetId)
                .flatMapLatest { presetMaps ->
                    mapDao.getMapByName(presetMaps.map { it.mapName })
                        .map { maps ->
                            maps.mapNotNull { mapEntity ->
                                val presetMap = presetMaps.find { it.mapName == mapEntity.name }
                                val storage = filePathToMapStorage(mapEntity.path)
                                if (storage == MapStorage.STREAM) {
                                    mapEntity.toMap(
                                        mapStorage = storage,
                                        enable = presetMap?.enable ?: false,
                                        order = presetMap?.order ?: 0,
                                        visible = presetMap?.visible ?: true,
                                        alpha = presetMap?.alpha ?: 1.0f
                                    )
                                } else {
                                    null
                                }
                            }.sortedBy { it.order }
                        }
                }
        }
            .distinctUntilChanged()

    override fun getMap(name: String): Flow<Map> = mapDao.get(name).map { mapEntity ->
        val presetId = preference.getPresetSelectedId()
        val presetMap = presetMapDao.getPresetMapSync(
            presetId = presetId,
            mapName = mapEntity.name
        )

        if (presetMap == null) {
            presetMapDao.insert(
                PresetMapEntity(
                    presetId = presetId,
                    mapName = mapEntity.name,
                    enable = true,
                    order = mapDao.getCountMaps(),
                    visible = true,
                    alpha = 1.0f,
                )
            )
        }

        mapEntity.toMap(
            mapStorage = filePathToMapStorage(mapEntity.path),
            enable = presetMap?.enable ?: true,
            order = presetMap?.order ?: mapDao.getCountMaps(),
            visible = presetMap?.visible ?: true,
            alpha = presetMap?.alpha ?: 1.0f
        )
    }

    private fun filePathToMapStorage(path: String) =
        when {
            path.contains(MBTILES_VECTOR) -> MapStorage.MBTILES_VECTOR
            path.startsWith(MBTILES) -> MapStorage.MBTILES_RASTER
            path.startsWith(FILE) -> MapStorage.FILE
            else -> MapStorage.STREAM
        }
}
