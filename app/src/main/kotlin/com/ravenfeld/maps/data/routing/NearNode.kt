package com.ravenfeld.maps.data.routing

import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.distanceBetween

@Suppress("LongParameterList", "DataClassContainsFunctions")
data class NearNode(
    val searchGeoPoint: GeoPoint,
    val gridIndex: Int,
    val nodeIndex: Int,
    val nodeGeoPoint: GeoPoint,
    val nodeDistance: Double,
    val edgeIndex: Int? = null,
    val edgeGeoPoint: GeoPoint? = null,
    val edgeDistance: Double? = null
) {

    @Suppress("MagicNumber")
    fun getIndex(): Long = RoutingUtils.combineGridIndexNodeIndex(gridIndex, nodeIndex)

    fun distanceNodeEdge(): Double {
        return edgeGeoPoint?.let {
            distanceBetween(nodeGeoPoint, edgeGeoPoint)
        } ?: run {
            0.0
        }
    }

    fun copy(
        edgeIndex: Int? = null,
        edgeGeoPoint: GeoPoint? = null,
        edgeDistance: Double? = null
    ) =
        NearNode(
            searchGeoPoint,
            gridIndex,
            nodeIndex,
            nodeGeoPoint,
            nodeDistance,
            edgeIndex,
            edgeGeoPoint,
            edgeDistance
        )

    override fun toString(): String {
        return "NearNode searchGeoPoint: $searchGeoPoint " +
            "gridIndex: $gridIndex " +
            "nodeIndex:$nodeIndex " +
            "nodeGeoPoint:$nodeGeoPoint " +
            "edgeIndex:$edgeIndex " +
            "edgeGeoPoint:$edgeGeoPoint"
    }
}
