package com.ravenfeld.maps.data.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.ravenfeld.maps.data.database.Database
import com.ravenfeld.maps.data.database.dao.MapDao
import com.ravenfeld.maps.data.database.dao.PresetDao
import com.ravenfeld.maps.data.database.dao.PresetMapDao
import com.ravenfeld.maps.data.database.dao.TrackDao
import com.ravenfeld.maps.data.database.dao.UserActionDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Object that groups the singletons of database.
 */
@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Suppress("MagicNumber")
    private val MIGRATION_1_2 = object : Migration(1, 2) {
        override fun migrate(db: SupportSQLiteDatabase) {
            db.execSQL("ALTER TABLE map ADD COLUMN minZoom REAL DEFAULT(0.0) NOT NULL")
            db.execSQL("ALTER TABLE map ADD COLUMN maxZoom REAL DEFAULT(0.0) NOT NULL")
        }
    }

    @Suppress("MagicNumber")
    private val MIGRATION_2_3 = object : Migration(2, 3) {
        override fun migrate(db: SupportSQLiteDatabase) {
            db.execSQL("ALTER TABLE map ADD COLUMN visible INTEGER DEFAULT(1) NOT NULL")
        }
    }

    @Suppress("MagicNumber")
    private val MIGRATION_3_4 = object : Migration(3, 4) {
        override fun migrate(db: SupportSQLiteDatabase) {
            db.execSQL("ALTER TABLE map ADD COLUMN groupName TEXT DEFAULT(null)")
        }
    }

    @Suppress("MagicNumber")
    private val MIGRATION_4_5 = object : Migration(4, 5) {
        override fun migrate(db: SupportSQLiteDatabase) {
            db.execSQL(
                "CREATE TABLE IF NOT EXISTS route " +
                    "(`name` TEXT NOT NULL, `path` TEXT NOT NULL, `type` TEXT NOT NULL, `visible` " +
                    "INTEGER NOT NULL, `color` TEXT NOT NULL, `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL)"
            )
        }
    }

    @Suppress("MagicNumber")
    private val MIGRATION_7_8 = object : Migration(7, 8) {
        override fun migrate(db: SupportSQLiteDatabase) {
            db.execSQL(
                "CREATE TABLE IF NOT EXISTS `preset` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL)"
            )
            db.execSQL("INSERT INTO preset (`id`,`name`) VALUES(0,'')")
            db.execSQL(
                "CREATE TABLE IF NOT EXISTS `preset_map` " +
                    "(`presetId` INTEGER NOT NULL," +
                    " `mapName` TEXT NOT NULL, " +
                    "`enable` INTEGER NOT NULL, `order` INTEGER NOT NULL, " +
                    "PRIMARY KEY(`presetId`, `mapName`)," +
                    " FOREIGN KEY(`presetId`) REFERENCES `preset`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE ," +
                    " FOREIGN KEY(`mapName`) REFERENCES `map`(`name`) ON UPDATE NO ACTION ON DELETE CASCADE )"
            )

            val cursor = db.query("SELECT * from map")
            cursor.moveToFirst()
            do {
                val indexName = cursor.getColumnIndex("name")
                val indexEnable = cursor.getColumnIndex("enable")
                val indexOrder = cursor.getColumnIndex("order")
                if (indexName >= 0 && indexEnable >= 0 && indexOrder >= 0) {
                    val name = cursor.getString(indexName)
                    val enable = cursor.getInt(indexEnable)
                    val order = cursor.getInt(indexOrder)
                    db.execSQL(
                        "INSERT INTO `preset_map` " +
                            "(`presetId`,`mapName`,`enable`,`order`) " +
                            "VALUES(0,'$name',$enable,$order)"
                    )
                }
            }
            while (cursor.moveToNext())

            db.renameMapTableToOld()
            db.createMapTable()
            db.copyOldMapDataToMapTable()
            db.dropOldMapTable()
        }

        private fun SupportSQLiteDatabase.renameMapTableToOld() {
            execSQL("ALTER TABLE `map` RENAME TO `map_old`")
        }

        private fun SupportSQLiteDatabase.createMapTable() {
            execSQL(
                """CREATE TABLE IF NOT EXISTS `map` 
                    |(`name` TEXT NOT NULL,
                    | `path` TEXT NOT NULL,
                    |  `visible` INTEGER NOT NULL, 
                    |  `alpha` REAL NOT NULL, 
                    |  `minZoom` REAL NOT NULL, 
                    |  `maxZoom` REAL NOT NULL, 
                    |  `groupName` TEXT, PRIMARY KEY(`name`))"""
                    .trimMargin()
            )
        }

        private fun SupportSQLiteDatabase.copyOldMapDataToMapTable() {
            execSQL(
                """INSERT INTO `map` 
                    |(`name`,
                    |`path`,
                    |`visible`,
                    |`alpha`,
                    |`minZoom`,
                    |`maxZoom`,
                    |`groupName`
                    |) 
                    |SELECT 
                    |`name`,
                    |`path`,
                    |`visible`,
                    |`alpha`,
                    |`minZoom`,
                    |`maxZoom`,
                    |`groupName`
                    | FROM `map_old`
                """.trimMargin()
            )
        }

        private fun SupportSQLiteDatabase.dropOldMapTable() {
            execSQL("DROP TABLE `map_old`")
        }
    }

    @Suppress("MagicNumber")
    private val MIGRATION_8_9 = object : Migration(8, 9) {
        override fun migrate(db: SupportSQLiteDatabase) {
            db.execSQL("ALTER TABLE `preset_map` ADD COLUMN `visible` INTEGER DEFAULT(0) NOT NULL")
            db.execSQL("ALTER TABLE `preset_map` ADD COLUMN `alpha` REAL DEFAULT(0.0) NOT NULL")

            val cursor = db.query("SELECT * from map")
            cursor.moveToFirst()
            do {
                val indexName = cursor.getColumnIndex("name")
                val indexVisible = cursor.getColumnIndex("visible")
                val indexAlpha = cursor.getColumnIndex("alpha")
                if (indexName >= 0 && indexVisible >= 0 && indexAlpha >= 0) {
                    val name = cursor.getString(indexName)
                    val visible = cursor.getInt(indexVisible)
                    val alpha = cursor.getFloat(indexAlpha)
                    db.execSQL(
                        "UPDATE `preset_map` SET " +
                            "`visible`= $visible, `alpha`= $alpha " +
                            "WHERE `mapName` = '$name'"
                    )
                }
            }
            while (cursor.moveToNext())

            db.renameMapTableToOld()
            db.createMapTable()
            db.copyOldMapDataToMapTable()
            db.dropOldMapTable()
        }

        private fun SupportSQLiteDatabase.renameMapTableToOld() {
            execSQL("ALTER TABLE `map` RENAME TO `map_old`")
        }

        private fun SupportSQLiteDatabase.createMapTable() {
            execSQL(
                """CREATE TABLE IF NOT EXISTS `map` 
                    |(`name` TEXT NOT NULL,
                    | `path` TEXT NOT NULL,
                    |  `minZoom` REAL NOT NULL, 
                    |  `maxZoom` REAL NOT NULL, 
                    |  `groupName` TEXT, PRIMARY KEY(`name`))"""
                    .trimMargin()
            )
        }

        private fun SupportSQLiteDatabase.copyOldMapDataToMapTable() {
            execSQL(
                """INSERT INTO `map` 
                    |(`name`,
                    |`path`,
                    |`minZoom`,
                    |`maxZoom`,
                    |`groupName`
                    |) 
                    |SELECT 
                    |`name`,
                    |`path`,
                    |`minZoom`,
                    |`maxZoom`,
                    |`groupName`
                    | FROM `map_old`
                """.trimMargin()
            )
        }

        private fun SupportSQLiteDatabase.dropOldMapTable() {
            execSQL("DROP TABLE `map_old`")
        }
    }

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): Database {
        val builder = Room.databaseBuilder(
            context,
            Database::class.java,
            Database.DATABASE_NAME
        ).addCallback(object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                db.execSQL("INSERT INTO preset (`id`,`name`) VALUES(0,'')")
            }
        })
            .addMigrations(MIGRATION_1_2)
            .addMigrations(MIGRATION_2_3)
            .addMigrations(MIGRATION_3_4)
            .addMigrations(MIGRATION_4_5)
            .addMigrations(MIGRATION_7_8)
            .addMigrations(MIGRATION_8_9)
        return builder.build()
    }

    @Singleton
    @Provides
    fun provideMapDao(database: Database): MapDao = database.mapDao()

    @Singleton
    @Provides
    fun provideTrackDao(database: Database): TrackDao = database.trackDao()

    @Singleton
    @Provides
    fun provideUserActionDao(database: Database): UserActionDao = database.userActionDao()

    @Singleton
    @Provides
    fun providePresetDao(database: Database): PresetDao = database.presetDao()

    @Singleton
    @Provides
    fun providePresetMapDao(database: Database): PresetMapDao = database.presetMapDao()
}
