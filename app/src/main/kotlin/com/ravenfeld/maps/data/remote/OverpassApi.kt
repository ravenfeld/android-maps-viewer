package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.data.remote.dto.ErrorDto
import com.ravenfeld.maps.data.remote.dto.OverpassResponseDto
import retrofit2.http.GET
import retrofit2.http.Query

interface OverpassApi {
    @GET("interpreter")
    suspend fun getWay(
        @Query("data", encoded = true) data: String
    ): NetworkResponse<OverpassResponseDto, ErrorDto>
}
