package com.ravenfeld.maps.data.routing

data class ExtractIndex(val gridIndex: Int, val nodeIndex: Int)
