package com.ravenfeld.maps.data.remote.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GpxStudioBrouterDto(
    @SerialName("features") val features: List<FeatureDto>
)

@Serializable
data class FeatureDto(
    @SerialName("geometry") val geometry: GeometryDto,
    @SerialName("properties") val properties: PropertiesDto,
)

@Serializable
data class GeometryDto(
    @SerialName("coordinates") val coordinates: List<DoubleArray>
)

@Serializable
data class PropertiesDto(
    @SerialName("messages") val messages: List<List<String>>
)
