package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.data.remote.dto.ElevationsIgnDto
import com.ravenfeld.maps.data.remote.dto.ErrorDto
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

@Suppress("MaximumLineLength")
interface IgnApi {
    @Headers("Accept: application/json")
    @GET(
        "altimetrie/1.0/calcul/alti/rest/elevation.json?resource=ign_rge_alti_wld&delimiter=|&indent=false&measures=false&zonly=false"
    )
    suspend fun elevation(
        @Query("lon") longitude: Double,
        @Query("lat") latitude: Double
    ): NetworkResponse<ElevationsIgnDto, ErrorDto>

    @Headers("Accept: application/json")
    @GET(
        "altimetrie/1.0/calcul/alti/rest/elevation.json?resource=ign_rge_alti_wld&delimiter=|&indent=false&measures=false&zonly=false"
    )
    suspend fun elevations(
        @Query("lon") longitudes: String,
        @Query("lat") latitudes: String
    ): NetworkResponse<ElevationsIgnDto, ErrorDto>
}
