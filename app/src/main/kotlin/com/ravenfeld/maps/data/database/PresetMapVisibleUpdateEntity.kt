package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PresetMapVisibleUpdateEntity(
    @PrimaryKey val presetId: Int,
    @PrimaryKey val mapName: String,
    val visible: Boolean = true
)
