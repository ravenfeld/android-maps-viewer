package com.ravenfeld.maps.data.routing

/**
 * Min heap for map nodes
 * @author Jonas Grunert
 */
@Suppress("TooManyFunctions")
class NodeDistHeap(private val maxCapacity: Int) {
    private val valuesArray: FloatArray = FloatArray(maxCapacity + 1)

    // Combined gridIndex(63-32)+nodeIndex(31-0)
    private val nodeGridIndexArray: LongArray = LongArray(maxCapacity + 1)
    var size: Int = 0
        private set
    var sizeUsageMax: Int = 0
        private set

    /**
     * Resets and fills with nodes
     */
    fun resetEmpty() {
        size = 0
        sizeUsageMax = 0
    }

    fun add(nodeGridIndex: Long, value: Float) {
        check(size < maxCapacity) { "Heap capacity exceeded" }

        // place element into heap at bottom
        size++
        val indexInHeap = size
        valuesArray[indexInHeap] = value
        nodeGridIndexArray[indexInHeap] = nodeGridIndex
        if (size > sizeUsageMax) {
            sizeUsageMax = size
        }
        bubbleUp(size)
    }

    /**
     * Decreases key if new key smaller than existing key
     * Slow linear time complexity (n=heapsize) but isnt called very often.
     */
    fun decreaseKeyIfSmaller(nodeGridIndex: Long, newKey: Float): Boolean {
        val heapIndex = findNode(nodeGridIndex)
        return if (newKey < valuesArray[heapIndex]) {
            valuesArray[heapIndex] = newKey
            bubbleUp(heapIndex)
            true
        } else {
            false
        }
    }

    /**
     * Changes key, assumes that newKey<oldKey></oldKey>
     */
    fun decreaseKey(heapIndex: Int, newKey: Int) {
        valuesArray[heapIndex] = newKey.toFloat()
        bubbleUp(heapIndex)
    }

    private fun findNode(nodeGridIndex: Long): Int = nodeGridIndexArray.indexOfFirst { it == nodeGridIndex }

    fun isEmpty() = size == 0

    fun removeFirst(): Long {
        assert(!isEmpty())

        val nodeGridIndex = nodeGridIndexArray[1]

        // get rid of the last leaf/decrement
        valuesArray[1] = valuesArray[size]
        valuesArray[size] = (-1).toFloat()
        nodeGridIndexArray[1] = nodeGridIndexArray[size]
        size--
        bubbleDown()
        return nodeGridIndex
    }

    private fun bubbleDown() {
        var i = 1

        // bubble down
        while (hasLeftChild(i)) {
            // which of my children is smaller?
            var smallerChild = leftIndex(i)

            // bubble with the smaller child, if I have a smaller child
            if (hasRightChild(i) &&
                valuesArray[leftIndex(i)] > valuesArray[rightIndex(i)]
            ) {
                smallerChild = rightIndex(i)
            }
            if (valuesArray[i] > valuesArray[smallerChild]) {
                swap(i, smallerChild)
            } else {
                // otherwise, get outta here!
                break
            }

            // make sure to update loop counter/index of where last el is put
            i = smallerChild
        }
    }

    private fun bubbleUp(index: Int) {
        var i = index
        while (hasParent(i) &&
            parent(i) > valuesArray[i]
        ) {
            // parent/child are out of order; swap them
            swap(i, parentIndex(i))
            i = parentIndex(i)
        }
    }

    private fun hasParent(i: Int): Boolean = i > 1

    private fun leftIndex(i: Int): Int =
        i * 2

    private fun rightIndex(i: Int): Int = i * 2 + 1

    private fun hasLeftChild(i: Int): Boolean = leftIndex(i) <= size

    private fun hasRightChild(i: Int): Boolean = rightIndex(i) <= size

    private fun parent(i: Int): Float = valuesArray[parentIndex(i)]

    private fun parentIndex(i: Int): Int = i / 2

    private fun swap(index1: Int, index2: Int) {
        val tmp1 = valuesArray[index1]
        valuesArray[index1] = valuesArray[index2]
        valuesArray[index2] = tmp1
        val tmp2 = nodeGridIndexArray[index1]
        nodeGridIndexArray[index1] = nodeGridIndexArray[index2]
        nodeGridIndexArray[index2] = tmp2
    }
}
