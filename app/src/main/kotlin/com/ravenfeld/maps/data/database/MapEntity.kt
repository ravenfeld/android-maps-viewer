package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ravenfeld.maps.domain.map.Map
import com.ravenfeld.maps.domain.map.MapGroup
import com.ravenfeld.maps.domain.map.MapStorage

@Entity(tableName = "map")
data class MapEntity(
    @PrimaryKey val name: String,
    val path: String,
    val minZoom: Float = 0f,
    val maxZoom: Float = 0f,
    val groupName: String? = null
) {
    fun toMap(mapStorage: MapStorage, enable: Boolean, order: Int, visible: Boolean, alpha: Float) = Map(
        name,
        path,
        mapStorage,
        path,
        enable,
        order,
        visible,
        alpha,
        minZoom,
        maxZoom
    )

    fun toMapGroup(mapStorage: MapStorage, enable: Boolean) = groupName?.let { MapGroup(groupName, mapStorage, enable) }
        ?: run { throw ClassCastException("Unable to convert a mapEntity to a mapGroup") }
}
