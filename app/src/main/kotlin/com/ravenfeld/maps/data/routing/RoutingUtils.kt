package com.ravenfeld.maps.data.routing

object RoutingUtils {

    @Suppress("MagicNumber")
    fun extractGridIndexNodeIndex(value: Long): ExtractIndex {
        val gridIndex = (value shr 32).toInt()
        val nodeIndex = value.toInt()
        return ExtractIndex(gridIndex, nodeIndex)
    }

    @Suppress("MagicNumber")
    fun combineGridIndexNodeIndex(
        gridIndex: Int,
        nodeIndex: Int
    ): Long = gridIndex.toLong() shl 32 or (nodeIndex and 0xffffffffL.toInt()).toLong()
}
