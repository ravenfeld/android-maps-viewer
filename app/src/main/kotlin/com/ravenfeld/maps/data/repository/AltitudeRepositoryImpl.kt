package com.ravenfeld.maps.data.repository

import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationManager
import android.location.OnNmeaMessageListener
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import androidx.core.content.ContextCompat
import com.ravenfeld.maps.data.elevation.HgtReader
import com.ravenfeld.maps.data.remote.IgnWebService
import com.ravenfeld.maps.data.remote.NetworkHandler
import com.ravenfeld.maps.data.remote.OpenElevationWebService
import com.ravenfeld.maps.domain.altitude.AltitudeRepository
import com.ravenfeld.maps.domain.geo.GeoPoint
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.emptyFlow
import timber.log.Timber
import javax.inject.Inject

private const val ALTITUDE_INDEX = 9

class AltitudeRepositoryImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val ignWebService: IgnWebService,
    private val openElevationWebService: OpenElevationWebService,
    private val networkHandler: NetworkHandler
) : AltitudeRepository {

    private val hgtReader = HgtReader(context)

    @Suppress("TooGenericExceptionCaught")
    @SuppressLint("MissingPermission")
    private val _altitudeMslUpdates =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            callbackFlow {
                val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val callback = OnNmeaMessageListener { message: String, _: Long ->
                    if (message.startsWith("\$GPGGA") ||
                        message.startsWith("\$GNGNS") ||
                        message.startsWith("\$GNGGA")
                    ) {
                        getAltitudeMeanSeaLevel(message)?.let {
                            trySend(it)
                        }
                    }
                }

                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        locationManager.addNmeaListener(
                            ContextCompat.getMainExecutor(context),
                            callback
                        )
                    } else {
                        locationManager.addNmeaListener(callback, Handler(Looper.getMainLooper()))
                    }
                } catch (e: Exception) {
                    Timber.e(e, "Exception in location flow")
                    close(e) // in case of exception, close the Flow
                }

                awaitClose {
                    Timber.d("Stopping NMEA updates")
                    locationManager.removeNmeaListener(callback) // clean up when Flow collection ends
                }
            }
        } else {
            emptyFlow()
        }

    override fun getAltitudeMsl(): Flow<Double> = _altitudeMslUpdates

    override suspend fun getAltitude(point: GeoPoint): Double? {
        return if (networkHandler.isNetworkAvailable()) {
            ignWebService.getElevation(point = point).getOrNull()
                ?: openElevationWebService.getElevation(point = point).getOrNull()
        } else if (hgtReader.isReady()) {
            hgtReader.getGeoPointWithElevationFromHgt(
                latitude = point.latitude,
                longitude = point.longitude
            ).altitude
        } else {
            null
        }
    }

    override suspend fun getAltitudes(points: List<GeoPoint>): List<GeoPoint> {
        val result = mutableListOf<GeoPoint>()
        if (networkHandler.isNetworkAvailable()) {
            result.addAll(
                ignWebService.getElevations(points).getOrNull()
                    ?: openElevationWebService.getElevations(points).getOrDefault(points)
            )
        } else if (hgtReader.isReady()) {
            points.forEach {
                result.add(hgtReader.getGeoPointWithElevationFromHgt(it.latitude, it.longitude))
            }
        } else {
            result.addAll(points)
        }

        return result
    }

    @Suppress("TooGenericExceptionCaught")
    private fun getAltitudeMeanSeaLevel(nmeaSentence: String): Double? {
        val tokens = nmeaSentence.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return if (nmeaSentence.startsWith("\$GPGGA") ||
            nmeaSentence.startsWith("\$GNGNS") ||
            nmeaSentence.startsWith("\$GNGGA")
        ) {
            val altitude: String = try {
                tokens[ALTITUDE_INDEX]
            } catch (e: ArrayIndexOutOfBoundsException) {
                Timber.e("Bad NMEA sentence for geoid altitude - $nmeaSentence :$e")
                return null
            }
            if (!TextUtils.isEmpty(altitude)) {
                var altitudeParsed: Double? = null
                try {
                    altitudeParsed = altitude.toDouble()
                } catch (e: NumberFormatException) {
                    Timber.e(
                        e,
                        "Bad geoid altitude value of '$altitude' in NMEA sentence $nmeaSentence"
                    )
                }
                altitudeParsed
            } else {
                Timber.w("Couldn't parse geoid altitude from NMEA: $nmeaSentence")
                null
            }
        } else {
            Timber.w("Input must be \$GPGGA, \$GNGNS, or \$GNGGA NMEA: $nmeaSentence")
            null
        }
    }
}
