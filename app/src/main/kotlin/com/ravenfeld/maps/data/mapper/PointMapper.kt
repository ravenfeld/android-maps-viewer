package com.ravenfeld.maps.data.mapper

import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.TrackPoint
import com.ravenfeld.maps.domain.geo.Way
import org.maplibre.geojson.Point

internal fun Point.toTrackPoint(way: Way? = null) =
    TrackPoint(
        geoPoint = GeoPoint(
            latitude = this.latitude(),
            longitude = this.longitude(),
            altitude = if (this.hasAltitude()) {
                this.altitude()
            } else {
                0.0
            }
        ),
        way = way
    )
