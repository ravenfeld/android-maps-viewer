package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Calendar
import java.util.Date

@Entity
data class TrackColorUpdateEntity(
    @PrimaryKey val id: Int,
    val color: String,
    val updated: Date = Calendar.getInstance().time
)
