package com.ravenfeld.maps.data.repository

import com.ravenfeld.maps.data.mapper.toDomain
import com.ravenfeld.maps.data.remote.SearchWebService
import com.ravenfeld.maps.domain.search.model.Suggestion
import com.ravenfeld.maps.domain.search.repository.SearchRepository
import org.maplibre.android.geometry.LatLng
import javax.inject.Inject

class SearchRepositoryImpl @Inject constructor(
    private val searchWebService: SearchWebService,
) : SearchRepository {
    override suspend fun getSuggestions(query: String, locationUser: LatLng?): List<Suggestion> =
        searchWebService
            .getSuggestions(query = query, location = locationUser)
            .map { response ->

                response.results
                    .filterNot { item ->
                        item.properties.osmValue == "administrative" && response.results.any {
                            it.properties.osmValue == "city" || it.properties.osmValue == "town"
                        }
                    }
                    .toDomain()
            }
            .getOrNull()
            ?: emptyList()
}
