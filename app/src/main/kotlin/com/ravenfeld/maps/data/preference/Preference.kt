package com.ravenfeld.maps.data.preference

import android.content.Context
import android.content.SharedPreferences
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import androidx.security.crypto.MasterKey.Builder
import com.ravenfeld.maps.domain.place.model.Type
import com.ravenfeld.maps.domain.routing.RoutingMode
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject
import javax.inject.Singleton

private const val MAPS_VIEWER_PREFERENCES = "MAPS_VIEWER_PREFERENCES"
private const val KEY_ROUTING_MODE_PREF = "ROUTING_MODE"
private const val KEY_MAPS_OFFLINE_ENABLE_PREF = "MAPS_OFFLINE_ENABLE"
private const val KEY_PRESET_SELECTED_ID_PREF = "PRESET_SELECTED_ID"
private const val KEY_DISPLAY_PLACE_PREF = "DISPLAY_PLACE"
private const val DEFAULT_ID: Int = 0

@Suppress("TooManyFunctions")
@Singleton
class Preference
@Inject constructor(@ApplicationContext context: Context) {

    private var sharedPreferences: SharedPreferences
    private val stateflowRoutingMode: MutableStateFlow<RoutingMode> = MutableStateFlow(RoutingMode.FREE)
    private val stateflowMapsOfflineEnable: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val stateflowPresetSelectedId: MutableStateFlow<Int> = MutableStateFlow(DEFAULT_ID)

    init {
        val spec = KeyGenParameterSpec.Builder(
            MasterKey.DEFAULT_MASTER_KEY_ALIAS,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        )
            .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
            .setKeySize(MasterKey.DEFAULT_AES_GCM_MASTER_KEY_SIZE)
            .build()

        val masterKey = Builder(context)
            .setKeyGenParameterSpec(spec)
            .build()

        sharedPreferences = EncryptedSharedPreferences.create(
            context,
            MAPS_VIEWER_PREFERENCES,
            masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
        stateflowRoutingMode.update { getRoutingMode() }
        stateflowMapsOfflineEnable.update { getMapsOfflineEnable() }
        stateflowPresetSelectedId.update { getPresetSelectedId() }
    }

    fun setRoutingMode(value: RoutingMode) {
        stateflowRoutingMode.update { value }
        val editor = sharedPreferences.edit()
        with(editor) {
            putString(KEY_ROUTING_MODE_PREF, value.name)
            apply()
        }
    }

    fun getRoutingMode(): RoutingMode = RoutingMode.valueOf(
        sharedPreferences.getString(KEY_ROUTING_MODE_PREF, RoutingMode.HIKE.name) ?: RoutingMode.HIKE.name
    )

    fun getFlowRoutingMode(): StateFlow<RoutingMode> = stateflowRoutingMode.asStateFlow()

    fun setMapsOfflineEnable(value: Boolean) {
        stateflowMapsOfflineEnable.update { value }
        val editor = sharedPreferences.edit()
        with(editor) {
            putBoolean(KEY_MAPS_OFFLINE_ENABLE_PREF, value)
            apply()
        }
    }

    fun getMapsOfflineEnable(): Boolean = sharedPreferences.getBoolean(KEY_MAPS_OFFLINE_ENABLE_PREF, false)

    fun getFlowMapsOfflineEnable(): StateFlow<Boolean> = stateflowMapsOfflineEnable.asStateFlow()

    fun setPresetSelected(value: Int) {
        stateflowPresetSelectedId.update { value }
        val editor = sharedPreferences.edit()
        with(editor) {
            putInt(KEY_PRESET_SELECTED_ID_PREF, value)
            apply()
        }
    }

    fun getPresetSelectedId(): Int = sharedPreferences.getInt(KEY_PRESET_SELECTED_ID_PREF, DEFAULT_ID)

    fun getFlowPresetSelectedId(): StateFlow<Int> = stateflowPresetSelectedId.asStateFlow()

    fun setDisplayPlace(displayTypes: List<Type>) {
        val editor = sharedPreferences.edit()
        with(editor) {
            putString(KEY_DISPLAY_PLACE_PREF, displayTypes.joinToString(";"))
            apply()
        }
    }

    fun getDisplayPlace(): List<Type> =
        sharedPreferences.getString(KEY_DISPLAY_PLACE_PREF, null)?.let { pref ->
            if (pref.isNotEmpty()) {
                pref.split(";").map {
                    Type.valueOf(it)
                }
            } else {
                emptyList()
            }
        } ?: emptyList()
}
