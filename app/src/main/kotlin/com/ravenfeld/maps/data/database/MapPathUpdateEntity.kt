package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MapPathUpdateEntity(
    @PrimaryKey val name: String,
    val path: String,
)
