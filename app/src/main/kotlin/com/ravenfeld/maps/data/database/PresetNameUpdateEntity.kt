package com.ravenfeld.maps.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PresetNameUpdateEntity(
    @PrimaryKey val id: Int,
    val name: String,
)
