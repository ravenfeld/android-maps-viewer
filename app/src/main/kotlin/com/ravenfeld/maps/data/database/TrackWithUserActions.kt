package com.ravenfeld.maps.data.database

import androidx.room.Embedded
import androidx.room.Relation
import com.ravenfeld.maps.domain.track.UserActionEntity

data class TrackWithUserActions(
    @Embedded val trackEntity: TrackEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "track_id"
    ) val userActions: List<UserActionEntity>
)
