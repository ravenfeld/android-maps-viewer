package com.ravenfeld.maps.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.ravenfeld.maps.data.remote.dto.ErrorDto
import com.ravenfeld.maps.data.remote.dto.SearchResponseDto
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface SearchApi {
    @GET(
        "api/?limit=10&" +
            "location_bias_scale=0.3&" +
            "osm_tag=place&" +
            "osm_tag=natural&" +
            "osm_tag=!:information&" +
            "osm_tag=!place:country&" +
            "osm_tag=boundary:administrative&" +
            "osm_tag=tourism&" +
            "osm_tag=:forest&" +
            "osm_tag=:parking&" +
            "osm_tag=:train_station&" +
            "osm_tag=:place_of_worship&" +
            "osm_tag=:townhall&" +
            "osm_tag=building"
    )
    @Headers("Accept: application/json")
    suspend fun search(
        @Query("lang") lang: String,
        @Query("q") query: String,
        @Query("lat") lat: Double?,
        @Query("lon") lon: Double?,
    ): NetworkResponse<SearchResponseDto, ErrorDto>
}
