package com.ravenfeld.maps.ui.add

import android.util.Patterns
import androidx.databinding.BaseObservable
import com.ravenfeld.maps.domain.map.MAX_ZOOM
import com.ravenfeld.maps.domain.map.MIN_ZOOM
import com.ravenfeld.maps.ui.common.TextError

class AddEditMapUiModel(val mode: DialogMode, name: String? = null) : BaseObservable() {
    var name: String? = name
        set(value) {
            field = value
            nameError = validatorText(value)
            checkError()
            notifyChange()
        }
    var url: String? = null
        set(value) {
            field = value
            urlError = validatorUrlText(value)
            checkError()
            notifyChange()
        }

    var nameError: TextError? = null
        set(value) {
            field = value
            checkError()
            notifyChange()
        }

    var urlError: TextError? = null

    var isValidate: Boolean = false

    var zoom: IntArray = intArrayOf(MIN_ZOOM.toInt(), MAX_ZOOM.toInt())
        set(value) {
            field = value
            checkError()
            notifyChange()
        }

    private fun validatorText(value: String?) = if (value.isNullOrEmpty()) {
        TextError.EMPTY
    } else {
        null
    }

    private fun validatorUrlText(value: String?) = when {
        value.isNullOrEmpty() -> TextError.EMPTY
        !Patterns.WEB_URL.matcher(value).find() -> TextError.URL_MALFORMED
        else -> null
    }

    private fun checkError() {
        isValidate = !when {
            name == null -> true
            nameError != null -> true
            url == null -> true
            urlError != null -> true
            else -> false
        }
    }
}
