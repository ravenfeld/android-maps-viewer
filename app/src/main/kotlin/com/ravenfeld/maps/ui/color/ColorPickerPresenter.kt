package com.ravenfeld.maps.ui.color

interface ColorPickerPresenter {
    fun onColorChanged(color: Int)
    fun onButtonOkClick()
    fun onButtonCancelClick()
}
