package com.ravenfeld.maps.ui.edition

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.databinding.DialogAddOnTrackBinding
import com.ravenfeld.maps.ui.common.getOrThrow

class AddOnTrackDialog : AppCompatDialogFragment(), AddOnTrackDialogPresenter {

    private var _binding: DialogAddOnTrackBinding? = null

    private val binding by lazy {
        _binding.getOrThrow()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogAddOnTrackBinding.inflate(inflater, container, false)
        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        @SuppressWarnings("MagicNumber")
        val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
        dialog?.window?.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun addWaypoint() {
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            STATE_ADD_ON_TRACK_SELECTED,
            AddOnTrackResult(
                point = AddOnTrackDialogArgs.fromBundle(requireArguments()).point,
                type = AddOnTrackResult.Type.WAYPOINT
            )
        )
    }

    override fun addPoi() {
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            STATE_ADD_ON_TRACK_SELECTED,
            AddOnTrackResult(
                point = AddOnTrackDialogArgs.fromBundle(requireArguments()).point,
                type = AddOnTrackResult.Type.POI
            )
        )
    }

    companion object {
        const val STATE_ADD_ON_TRACK_SELECTED = "add_on_track_selected"
    }
}
