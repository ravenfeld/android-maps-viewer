package com.ravenfeld.maps.ui.download

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavBackStackEntry
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.R
import com.ravenfeld.maps.databinding.DialogDownloadMapBinding
import com.ravenfeld.maps.ui.common.RequestEvent
import com.ravenfeld.maps.ui.common.TextError
import com.ravenfeld.maps.ui.common.alert.AlertDialog.Companion.STATE_CLICK_TYPE_KEY
import com.ravenfeld.maps.ui.common.alert.AlertDialogDirections
import com.ravenfeld.maps.ui.common.alert.AlertResult
import com.ravenfeld.maps.ui.common.getOrThrow
import com.ravenfeld.maps.ui.common.safeNavigate
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

private const val DOWNLOAD_MAP = "DOWNLOAD_MAP"
private const val INFO_ZOOM = "INFO_ZOOM"

@AndroidEntryPoint
class DownloadMapDialog : AppCompatDialogFragment(), DownloadMapPresenter {

    private var _binding: DialogDownloadMapBinding? = null
    private val binding get() = _binding.getOrThrow()

    private val viewModel: DownloadMapViewModel by viewModels()

    private var navBackStackEntry: NavBackStackEntry? = null

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) {
            binding.uiModel?.let { uiModel ->
                uiModel.name?.let { name ->
                    viewModel.downloadTiles(
                        name,
                        uiModel.listUiModel.filter { it.checked }.map { it.name },
                        uiModel.area,
                        uiModel.zoom[0],
                        uiModel.zoom[1]
                    )
                }
            }
        }

    private val observerOnCreate = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_CREATE) {
            navBackStackEntry?.savedStateHandle?.let { savedStateHandle ->
                savedStateHandle.getLiveData<AlertResult>(STATE_CLICK_TYPE_KEY)
                    .observe(viewLifecycleOwner) { result ->
                        findNavController().navigateUp()
                        if (result.tag == DOWNLOAD_MAP && result.click == AlertResult.Click.CLICK_RIGHT) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU &&
                                ContextCompat.checkSelfPermission(
                                    requireContext(),
                                    Manifest.permission.POST_NOTIFICATIONS
                                ) != PackageManager.PERMISSION_GRANTED
                            ) {
                                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
                            } else {
                                binding.uiModel?.let { uiModel ->
                                    uiModel.name?.let { name ->
                                        viewModel.downloadTiles(
                                            name,
                                            uiModel.listUiModel.filter { it.checked }.map { it.name },
                                            uiModel.area,
                                            uiModel.zoom[0],
                                            uiModel.zoom[1]
                                        )
                                    }
                                }
                            }
                        }
                    }
            }
        }
    }

    private val observerOnDestroy = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_DESTROY) {
            navBackStackEntry?.lifecycle?.removeObserver(observerOnCreate)
            navBackStackEntry?.savedStateHandle?.remove<AlertResult>(STATE_CLICK_TYPE_KEY)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogDownloadMapBinding.inflate(inflater, container, false)
        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        navBackStackEntry = findNavController().getBackStackEntry(R.id.downloadMapDialog)
        navBackStackEntry?.lifecycle?.addObserver(observerOnCreate)
        viewLifecycleOwner.lifecycle.addObserver(observerOnDestroy)
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                launch {
                    viewModel.events.collect {
                        it.getContentIfNotHandled()?.let { event ->
                            when (event) {
                                RequestEvent.Error -> binding.uiModel?.nameError = TextError.NAME_EXIST
                                RequestEvent.Success -> findNavController().navigateUp()
                            }
                        }
                    }
                }
                launch {
                    viewModel.maps(DownloadMapDialogArgs.fromBundle(requireArguments()).area).collect { uiModel ->
                        binding.uiModel = uiModel
                    }
                }
            }
        }
    }

    override fun onDownloadClick() {
        findNavController().safeNavigate(
            AlertDialogDirections.actionToAlertOk(DOWNLOAD_MAP, getString(R.string.download_maps_explanation))
        )
    }

    override fun onInfoClick() {
        findNavController().safeNavigate(
            AlertDialogDirections.actionToAlertOk(INFO_ZOOM, getString(R.string.zoom_explanation))
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            @Suppress("deprecation")
            it.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            @Suppress("deprecation")
            val display = requireActivity().windowManager.defaultDisplay
            val size = Point()
            @Suppress("deprecation")
            display.getSize(size)
            val params = it.attributes
            val widthMax = resources.getDimensionPixelSize(R.dimen.max_width)

            @Suppress("MagicNumber")
            val width = (size.x * 0.9).toInt()
            params.width = if (width < widthMax) {
                width
            } else {
                widthMax
            }
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            it.attributes = params as WindowManager.LayoutParams
        }
    }
}
