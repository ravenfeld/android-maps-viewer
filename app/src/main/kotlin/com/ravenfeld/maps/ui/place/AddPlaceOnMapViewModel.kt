package com.ravenfeld.maps.ui.place

import androidx.lifecycle.ViewModel
import com.ravenfeld.maps.domain.place.model.Type
import com.ravenfeld.maps.domain.settings.SettingsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AddPlaceOnMapViewModel @Inject constructor(
    private val settingsRepository: SettingsRepository
) : ViewModel() {

    val uiModel = AddPlaceOnMapUiModel(
        PlaceOnMapUiModel.entries.map { place ->
            ItemPlaceOnMapUiModel(
                placeOnMapUiModel = place,
            ).apply {
                checked = settingsRepository.getDisplayPlace().any { it.name == place.name }
            }
        }
    )

    fun setDisplayPlace(displayTypes: List<Type>) = settingsRepository.setDisplayPlace(displayTypes)
}
