package com.ravenfeld.maps.ui.maps

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.ravenfeld.maps.domain.place.model.PlaceOnMap
import com.ravenfeld.maps.domain.place.model.Type
import org.maplibre.android.geometry.LatLng
import org.maplibre.geojson.Feature
import org.maplibre.geojson.Point

internal const val POI_ON_MAP_TYPE = "POI_ON_MAP_TYPE"

private const val TEXT = "TEXT"
private const val LAT = "LAT"
private const val LNG = "LNG"

internal fun PlaceOnMap.toFeature() =
    Feature.fromGeometry(
        Point.fromLngLat(center.longitude, center.latitude),
        JsonObject().apply {
            addProperty(LAT, center.latitude)
            addProperty(LNG, center.longitude)
            addProperty(POI_ON_MAP_TYPE, type.name)
            addProperty(TEXT, text)
        }
    )

internal fun JsonObject.toPlaceOnMap() =
    PlaceOnMap(
        text = this.getString(TEXT),
        center = LatLng(
            this.get(LAT).asDouble,
            this.get(LNG).asDouble
        ),
        type = Type.valueOf(this.get(POI_ON_MAP_TYPE).asString)
    )

private fun JsonObject.getString(key: String): String? {
    val propertyKey: JsonElement = get(key)
    return if (propertyKey.isJsonNull) {
        null
    } else {
        propertyKey.asString
    }
}
