package com.ravenfeld.maps.ui.place

import androidx.databinding.BaseObservable

class AddPlaceOnMapUiModel(val listUiModel: List<ItemPlaceOnMapUiModel>) : BaseObservable() {

    val onCheckedItem = object : ItemPlaceOnMapPresenter {
        override fun onCheckedChanged(placeOnMapUiModel: PlaceOnMapUiModel, checked: Boolean) {
            listUiModel.find { it.placeOnMapUiModel == placeOnMapUiModel }?.checked = checked
            notifyChange()
        }
    }
}
