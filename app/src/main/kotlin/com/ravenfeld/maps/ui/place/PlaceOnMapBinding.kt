package com.ravenfeld.maps.ui.place

import android.widget.CheckBox
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("items", "presenter")
fun RecyclerView.bindItemPoiOnMapUiModel(
    items: List<ItemPlaceOnMapUiModel>?,
    presenter: ItemPlaceOnMapPresenter
) {
    items?.let {
        val adapter = getOrCreateAdapter(this, presenter)
        adapter.submitList(items)
    }
}

private fun getOrCreateAdapter(
    recyclerView: RecyclerView,
    presenter: ItemPlaceOnMapPresenter
): ListPoiOnMapRecyclerViewAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ListPoiOnMapRecyclerViewAdapter) {
        recyclerView.adapter as ListPoiOnMapRecyclerViewAdapter
    } else {
        val bindableRecyclerAdapter = ListPoiOnMapRecyclerViewAdapter(presenter)
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}

@BindingAdapter("placeOnMapTitle")
fun CheckBox.setPlaceOnMapTitle(placeOnMapUiModel: PlaceOnMapUiModel) {
    setText(placeOnMapUiModel.textId)
}
