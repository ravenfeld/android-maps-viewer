package com.ravenfeld.maps.ui.info

data class ItemInfoUiModel(val title: String, val url: String)
