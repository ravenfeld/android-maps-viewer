package com.ravenfeld.maps.ui.maps

import android.view.View

@Suppress("TooManyFunctions", "ComplexInterface")
interface MapPresenter {
    fun onMapAlphaChanged(name: String, alpha: Float)
    fun onMapAlphaSave(name: String, alpha: Float)
    fun onMapVisibilityChanged(name: String, visible: Boolean)
    fun onMapMoveOrder(from: Int, to: Int)
    fun onMapsClick()
    fun onTrackVisibilityChanged(trackId: Int, visible: Boolean)
    fun onTrackColorClick(trackId: Int, color: String)
    fun onTrackNameClick(trackId: Int, name: String)
    fun onTrackSelected(trackId: Int)
    fun onTracksClick()
    fun onEditTrackClick()
    fun onTrackDeleteClick()
    fun onTrackDeleteLongClick(trackId: Int): Boolean
    fun onTrackCancelLastActionClick()
    fun onLocationClick()
    fun onCompassClick()
    fun onMenuClick(view: View)
    fun onSaveAreaClick()
    fun onInfoClick()
    fun onRoutingChange()
    fun onTrackEditClick()
    fun onMoreActionsClick()
    fun onSearchClick()
    fun onAddPoiOnMapClick()
    fun onDisplayTrackDetails()
}
