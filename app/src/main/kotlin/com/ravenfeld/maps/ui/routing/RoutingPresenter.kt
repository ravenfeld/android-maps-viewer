package com.ravenfeld.maps.ui.routing

import com.ravenfeld.maps.domain.routing.RoutingMode

interface RoutingPresenter {
    fun onRoutingClick(routingMode: RoutingMode)
}
