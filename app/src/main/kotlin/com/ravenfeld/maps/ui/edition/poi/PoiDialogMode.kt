package com.ravenfeld.maps.ui.edition.poi

enum class PoiDialogMode {
    ADD, EDIT
}
