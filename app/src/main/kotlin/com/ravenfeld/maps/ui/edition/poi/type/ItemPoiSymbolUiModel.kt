package com.ravenfeld.maps.ui.edition.poi.type

import com.ravenfeld.maps.domain.geo.PoiSymbol

data class ItemPoiSymbolUiModel(val symbol: PoiSymbol, val selected: Boolean)
