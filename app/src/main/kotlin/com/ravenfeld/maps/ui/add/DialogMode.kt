package com.ravenfeld.maps.ui.add

enum class DialogMode {
    ADD, EDIT
}
