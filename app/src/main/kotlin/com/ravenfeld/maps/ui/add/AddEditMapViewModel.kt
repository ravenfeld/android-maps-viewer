package com.ravenfeld.maps.ui.add

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ravenfeld.maps.domain.map.MapRepository
import com.ravenfeld.maps.ui.common.RequestEvent
import com.ravenfeld.maps.ui.download.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddEditMapViewModel @Inject constructor(private val mapRepository: MapRepository) :
    ViewModel() {

    private val _events = MutableStateFlow<Event<RequestEvent>?>(null)

    val events = _events.filterNotNull()

    fun uiModel(mode: DialogMode, name: String?) = flow {
        name?.let {
            mapRepository.getMap(it).collect { map ->
                emit(
                    AddEditMapUiModel(mode, map.name).apply {
                        url = map.url
                        zoom = intArrayOf(map.minZoom.toInt(), map.maxZoom.toInt())
                    }
                )
            }
        } ?: run {
            emit(AddEditMapUiModel(mode))
        }
    }

    fun add(name: String, url: String, minZoom: Int, maxZoom: Int) {
        viewModelScope.launch {
            if (mapRepository.add(name, url, minZoom.toFloat(), maxZoom.toFloat())) {
                _events.value = Event(RequestEvent.Success)
            } else {
                _events.value = Event(RequestEvent.Error)
            }
        }
    }

    fun save(name: String, url: String, minZoom: Int, maxZoom: Int) {
        viewModelScope.launch {
            mapRepository.save(name, url, minZoom.toFloat(), maxZoom.toFloat())
            _events.value = Event(RequestEvent.Success)
        }
    }
}
