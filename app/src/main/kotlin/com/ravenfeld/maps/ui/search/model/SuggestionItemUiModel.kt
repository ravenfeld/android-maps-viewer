package com.ravenfeld.maps.ui.search.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.runtime.Immutable

@Immutable
data class SuggestionItemUiModel(
    val id: Long,
    val title: String?,
    val subTitle: String,
    @DrawableRes val iconResId: Int,
    @StringRes val typeResId: Int? = null,
    val distance: Int?
)
