package com.ravenfeld.maps.ui.info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ravenfeld.maps.domain.map.MapRepository
import com.ravenfeld.maps.domain.map.mapsSuperFriend
import com.ravenfeld.maps.ui.common.RequestEvent
import com.ravenfeld.maps.ui.download.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InfoViewModel @Inject constructor(
    private val mapRepository: MapRepository
) : ViewModel() {

    private val _events = MutableStateFlow<Event<RequestEvent>?>(null)

    val events = _events.filterNotNull()

    fun uiModel(resource: Array<String>): InfoUiModel {
        val listItemInfoUiModel = resource.map {
            val split = it.split(";")
            ItemInfoUiModel(split[0], split[1])
        }
        return InfoUiModel(listItemInfoUiModel)
    }

    fun setSuperFriend() {
        viewModelScope.launch {
            mapRepository.addOrUpdate(mapsSuperFriend)
            _events.value = Event(RequestEvent.Success)
        }
    }
}
