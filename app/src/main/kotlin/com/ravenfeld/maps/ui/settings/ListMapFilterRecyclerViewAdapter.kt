package com.ravenfeld.maps.ui.settings

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.databinding.ItemConfigMapBinding
import com.ravenfeld.maps.databinding.ItemConfigMapGroupBinding
import com.ravenfeld.maps.databinding.ItemConfigSubMapBinding

private val DIFF_UTILS = object : DiffUtil.ItemCallback<ItemMapFilterBaseUiModel>() {

    override fun areItemsTheSame(
        oldItem: ItemMapFilterBaseUiModel,
        newItem: ItemMapFilterBaseUiModel
    ): Boolean = oldItem.name == newItem.name

    override fun areContentsTheSame(
        oldItem: ItemMapFilterBaseUiModel,
        newItem: ItemMapFilterBaseUiModel
    ): Boolean =
        if (oldItem is ItemMapGroupFilterUiModel && newItem is ItemMapGroupFilterUiModel) {
            oldItem.name == newItem.name &&
                oldItem.enable == newItem.enable &&
                oldItem.expand == newItem.expand
        } else if (oldItem is ItemSubMapFilterUiModel && newItem is ItemSubMapFilterUiModel) {
            oldItem.name == newItem.name &&
                oldItem.enable == newItem.enable &&
                oldItem.url == newItem.url
        } else if (oldItem is ItemMapFilterUiModel && newItem is ItemMapFilterUiModel) {
            oldItem.name == newItem.name &&
                oldItem.enable == newItem.enable &&
                oldItem.url == newItem.url
        } else {
            throw ClassCastException("The type is not known")
        }
}

private const val MAP_TYPE = 0
private const val MAP_GROUP_TYPE = 1
private const val SUB_MAP_TYPE = 2

class ListMapFilterRecyclerViewAdapter(private val presenter: MapFilterPresenter) :
    ListAdapter<ItemMapFilterBaseUiModel, RecyclerView.ViewHolder>(DIFF_UTILS) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            MAP_TYPE -> {
                val binding = ItemConfigMapBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                MapFilterViewHolder(binding, presenter)
            }
            MAP_GROUP_TYPE -> {
                val binding = ItemConfigMapGroupBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                MapGroupFilterViewHolder(binding, presenter)
            }
            SUB_MAP_TYPE -> {
                val binding = ItemConfigSubMapBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                SubMapFilterViewHolder(binding, presenter)
            }
            else -> throw ClassCastException("The type is not known")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        when (holder) {
            is MapFilterViewHolder -> holder.bind(getItem(position) as ItemMapFilterUiModel)
            is MapGroupFilterViewHolder -> holder.bind(getItem(position) as ItemMapGroupFilterUiModel)
            is SubMapFilterViewHolder -> holder.bind(getItem(position) as ItemSubMapFilterUiModel)
            else -> throw ClassCastException("The class is not known")
        }

    override fun getItemViewType(position: Int): Int =
        when (getItem(position)) {
            is ItemMapFilterUiModel -> MAP_TYPE
            is ItemMapGroupFilterUiModel -> MAP_GROUP_TYPE
            is ItemSubMapFilterUiModel -> SUB_MAP_TYPE
            else ->
                throw ClassCastException("The ItemViewType is not known")
        }
}

class MapFilterViewHolder(
    private val binding: ItemConfigMapBinding,
    private val presenter: MapFilterPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemMapFilterUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}

class MapGroupFilterViewHolder(
    private val binding: ItemConfigMapGroupBinding,
    private val presenter: MapFilterPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemMapGroupFilterUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        setState(uiModel.expand)
        binding.buttonExpandCollapse.setOnClickListener {
            setState(!uiModel.expand)
            presenter.onExpandedCollapsed(uiModel.name)
        }
        binding.executePendingBindings()
    }

    private fun setState(expand: Boolean) {
        val stateSet = intArrayOf(android.R.attr.state_checked * if (expand) 1 else -1)
        binding.buttonExpandCollapse.setImageState(stateSet, true)
    }
}

class SubMapFilterViewHolder(
    private val binding: ItemConfigSubMapBinding,
    private val presenter: MapFilterPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemSubMapFilterUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}
