package com.ravenfeld.maps.ui.settings

import com.ravenfeld.maps.domain.map.Map
import com.ravenfeld.maps.domain.map.MapStorage

class ItemMapFilterUiModel(
    name: String,
    val url: String,
    storage: MapStorage,
    enable: Boolean
) : ItemMapFilterBaseUiModel(name, storage, enable) {

    fun copy(): ItemMapFilterUiModel = ItemMapFilterUiModel(name, url, storage, enable)
}

fun Map.toItemMapFilterUiModel(): ItemMapFilterUiModel = ItemMapFilterUiModel(name, url, storage, enable)
