package com.ravenfeld.maps.ui.common

sealed class RequestEvent {
    object Error : RequestEvent()
    object Success : RequestEvent()
}
