package com.ravenfeld.maps.ui.maps

import com.ravenfeld.maps.domain.map.Map
import com.ravenfeld.maps.domain.map.MapStorage

data class ItemMapUiModel(
    val order: Int,
    val name: String,
    val visible: Boolean,
    val alpha: Int,
    val storage: MapStorage
)

@Suppress("MagicNumber")
fun Map.toItemMapUiModel() = ItemMapUiModel(order, name, visible, (alpha * 100).toInt(), storage)
