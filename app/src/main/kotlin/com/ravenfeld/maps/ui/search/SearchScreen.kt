package com.ravenfeld.maps.ui.search

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsDraggedAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBarDefaults
import androidx.compose.material3.SearchBarDefaults.InputFieldHeight
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldColors
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.ravenfeld.maps.R
import kotlinx.coroutines.flow.collectLatest

@Composable
fun SearchScreen(
    viewModel: SearchViewModel,
    onSuggestionSelected: (suggestion: SearchResult) -> Unit,
    onBack: () -> Unit,
    modifier: Modifier = Modifier,
) {
    val uiState by viewModel.uiState.collectAsState()

    LaunchedEffect(viewModel) {
        viewModel.event.collectLatest {
            when (val event = it) {
                is SearchEvent.SuggestionSelected -> onSuggestionSelected(event.suggestion)
                SearchEvent.OnBackClicked -> onBack()
            }
        }
    }
    BackHandler {
        viewModel.onBackClicked()
    }

    SearchContent(
        modifier = modifier,
        uiState = uiState,
        searchText = viewModel.searchField,
        onSuggestionSelected = viewModel::onSearchSelected,
        onSearchQuery = viewModel::onSearchChanged,
        onSearchClose = viewModel::onSearchClose,
        onBack = viewModel::onBackClicked,
    )
}

@Suppress("LongMethod")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun SearchContent(
    uiState: SearchUiState,
    searchText: String,
    onSuggestionSelected: (suggestionId: Long) -> Unit,
    onSearchQuery: (search: String) -> Unit,
    onSearchClose: () -> Unit,
    onBack: () -> Unit,
    modifier: Modifier = Modifier,
) {
    val keyboardController = LocalSoftwareKeyboardController.current

    val listState = rememberLazyListState()
    val isDragged by listState.interactionSource.collectIsDraggedAsState()
    LaunchedEffect(isDragged) {
        if (isDragged) {
            keyboardController?.hide()
        }
    }
    Scaffold(
        modifier = modifier,
        topBar = {
            Surface(shadowElevation = 6.dp) {
                TopAppBar(
                    title = {
                        SearchBarInputField(
                            modifier = Modifier,
                            query = searchText,
                            textStyle = MaterialTheme.typography.bodyLarge,
                            onQueryChange = onSearchQuery,
                            onSearch = {
                                keyboardController?.hide()
                            },
                            onActiveChange = {
                                if (!it) {
                                    onBack()
                                }
                            },
                            placeholder = {
                                Text(
                                    modifier = Modifier
                                        .padding(horizontal = 6.dp)
                                        .fillMaxWidth(),
                                    text = stringResource(id = R.string.button_search),
                                    style = MaterialTheme.typography.bodyLarge,
                                )
                            },
                            leadingIcon = {
                                Icon(imageVector = Icons.Default.Search, contentDescription = null)
                            },
                            trailingIcon =
                            {
                                if (searchText.isNotEmpty()) {
                                    Icon(
                                        modifier = Modifier
                                            .clip(CircleShape)
                                            .clickable {
                                                onSearchClose()
                                            }
                                            .padding(4.dp),
                                        imageVector = Icons.Default.Close,
                                        tint = MaterialTheme.colorScheme.onSecondary,
                                        contentDescription = null,
                                    )
                                }
                            }
                        )
                    },
                    navigationIcon = {
                        IconButton(
                            onClick = { onBack() },
                        ) {
                            Icon(
                                imageVector = Icons.AutoMirrored.Rounded.ArrowBack,
                                contentDescription = null,
                                tint = MaterialTheme.colorScheme.primary,
                            )
                        }
                    }
                )
            }
        },
        content = { paddingValues ->
            Box(modifier = Modifier.padding(paddingValues)) {
                LazyColumn(
                    state = listState,
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    if (uiState.isLoading) {
                        item {
                            LinearProgressIndicator(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(top = 8.dp, bottom = 4.dp),
                                color = MaterialTheme.colorScheme.primary,
                                trackColor = MaterialTheme.colorScheme.onTertiary,
                                strokeCap = StrokeCap.Square
                            )
                        }
                    }
                    uiState.message?.let {
                        item {
                            Text(
                                modifier = Modifier.padding(horizontal = 20.dp, vertical = 12.dp),
                                text = when (it) {
                                    SearchMessageError.NoInternet -> stringResource(R.string.search_no_internet)
                                    SearchMessageError.NoResults -> stringResource(
                                        R.string.search_no_results,
                                        searchText
                                    )

                                    SearchMessageError.SearchIsTooSmall -> stringResource(R.string.search_is_too_small)
                                },
                                color = MaterialTheme.colorScheme.onSecondary,
                                style = MaterialTheme.typography.bodyLarge
                            )
                        }
                    }
                    itemsIndexed(uiState.suggestions) { index, suggestion ->
                        Suggestion(
                            modifier = Modifier
                                .heightIn(56.dp)
                                .fillMaxWidth(),
                            suggestion = suggestion,
                            onClick = {
                                onSuggestionSelected(suggestion.id)
                            }
                        )
                        if (index < uiState.suggestions.lastIndex) {
                            HorizontalDivider(
                                color = MaterialTheme.colorScheme.onPrimaryContainer,
                                thickness = 1.dp
                            )
                        }
                    }
                }
            }
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun SearchBarInputField(
    query: String,
    onQueryChange: (String) -> Unit,
    onSearch: (String) -> Unit,
    onActiveChange: (Boolean) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    placeholder: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    textStyle: TextStyle = TextStyle.Default,
    colors: TextFieldColors = SearchBarDefaults.inputFieldColors(),
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
) {
    val focusRequester = remember { FocusRequester() }

    BasicTextField(
        value = query,
        onValueChange = onQueryChange,
        modifier = modifier
            .height(InputFieldHeight)
            .fillMaxWidth()
            .focusRequester(focusRequester)
            .onFocusChanged { if (it.isFocused) onActiveChange(true) },
        enabled = enabled,
        singleLine = true,
        textStyle = textStyle,
        cursorBrush = SolidColor(colors.cursorColor),
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
        keyboardActions = KeyboardActions(onSearch = { onSearch(query) }),
        interactionSource = interactionSource,
        decorationBox = @Composable { innerTextField ->
            TextFieldDefaults.DecorationBox(
                value = query,
                innerTextField = innerTextField,
                enabled = enabled,
                singleLine = true,
                visualTransformation = VisualTransformation.None,
                interactionSource = interactionSource,
                placeholder = placeholder,
                leadingIcon = leadingIcon?.let { leading ->
                    {
                        Box(Modifier.offset(x = 4.dp)) { leading() }
                    }
                },
                trailingIcon = trailingIcon?.let { trailing ->
                    {
                        Box(Modifier.offset(x = -4.dp)) { trailing() }
                    }
                },
                shape = SearchBarDefaults.inputFieldShape,
                colors = colors,
                contentPadding = TextFieldDefaults.contentPaddingWithoutLabel(),
                container = {},
            )
        }
    )
}
