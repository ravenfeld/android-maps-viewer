package com.ravenfeld.maps.ui.maps

import androidx.databinding.BaseObservable
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.map.DisplayInfo
import com.ravenfeld.maps.domain.routing.RoutingMode
import org.maplibre.android.location.modes.CameraMode

class MapUiModel : BaseObservable() {

    var hasLoadTrack: Boolean = false

    var listItemMapUiModel: List<ItemMapUiModel> = emptyList()
        set(value) {
            field = value
            notifyChange()
        }

    var listItemTrackUiModel: List<ItemTrackUiModel> = emptyList()
        set(value) {
            field = value
            notifyChange()
        }

    var listTrackUiModel: List<TrackUiModel> = emptyList()
        set(value) {
            field = value
            notifyChange()
        }

    var displayAreaDownload = false
        set(value) {
            field = value
            notifyChange()
        }

    var displayInfo: DisplayInfo = DisplayInfo.NONE
        set(value) {
            field = value
            notifyChange()
        }

    var userAltitudeMsl: Double? = null
        set(value) {
            field = value
            notifyChange()
        }
    var altitude: Double? = null
        set(value) {
            field = value
            notifyChange()
        }
    var cameraPosition: GeoPoint? = null
        set(value) {
            field = value
            notifyChange()
        }
    var userLocation: GeoPoint? = null
        set(value) {
            field = value
            notifyChange()
        }

    val editModeAndTrackWithSegment: Boolean
        get() = isEditMode && hasLeastTwoPoint

    val canBackHome: Boolean
        get() = editModeAndTrackWithSegment && track?.waypoints?.first() != track?.waypoints?.last()

    val canChooseRouting: Boolean
        get() = isEditMode

    var isEditMode: Boolean = false
        set(value) {
            field = value
            notifyChange()
        }

    val isTrackEditable: Boolean
        get() = displayInfo == DisplayInfo.TRACK &&
            !displayAreaDownload &&
            track?.waypoints?.isNotEmpty() == true

    private val hasLeastTwoPoint: Boolean
        get() = track?.waypoints?.size ?: 0 >= 2

    val canDeleteTrack: Boolean
        get() = !isEditMode &&
            displayInfo == DisplayInfo.TRACK &&
            !displayAreaDownload &&
            track?.trackPoints?.isNotEmpty() == true

    var track: TrackUiModel? = null
        set(value) {
            field = value
            notifyChange()
        }

    var touchInfo: TouchInfoUiModel? = null
        set(value) {
            field = value
            notifyChange()
        }

    var engineRoutingReady = false
        set(value) {
            field = value
            notifyChange()
        }

    var routingMode = RoutingMode.FREE
        set(value) {
            field = value
            notifyChange()
        }

    @CameraMode.Mode
    var cameraMode: Int = CameraMode.NONE
        set(value) {
            field = value
            notifyChange()
        }

    val trackingGPS: Boolean
        get() = cameraMode >= CameraMode.TRACKING

    var offlineEnable = false
        set(value) {
            field = value
            notifyChange()
        }

    var locationInfo: String = ""
        set(value) {
            field = value
            notifyChange()
        }

    fun hasTrackDisplay() = listTrackUiModel.isNotEmpty()

    fun routingModeEnable() = if (engineRoutingReady) {
        routingMode
    } else {
        RoutingMode.FREE
    }
}
