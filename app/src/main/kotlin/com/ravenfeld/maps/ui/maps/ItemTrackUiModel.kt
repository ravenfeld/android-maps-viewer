package com.ravenfeld.maps.ui.maps

import com.ravenfeld.maps.domain.geo.Track

data class ItemTrackUiModel(
    val id: Int,
    val name: String,
    val visible: Boolean,
    val color: String,
    val distance: Double,
    val upHill: Double,
    val downHill: Double,
)

@Suppress("MagicNumber")
fun Track.toItemTrackUiModel() = ItemTrackUiModel(
    id,
    name,
    visible,
    color,
    stats.measurement.distance,
    stats.measurement.upHill,
    stats.measurement.downHill
)
