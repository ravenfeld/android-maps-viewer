package com.ravenfeld.maps.ui.search.mapper

import com.ravenfeld.maps.R
import com.ravenfeld.maps.domain.search.model.Suggestion
import com.ravenfeld.maps.domain.search.model.Type
import com.ravenfeld.maps.ui.search.model.SuggestionItemUiModel
import org.maplibre.android.geometry.LatLng

@Suppress("LongMethod", "CyclomaticComplexMethod")
fun Suggestion.toUiModel(locationUser: LatLng?) =
    when (type) {
        Type.CITY ->
            SuggestionItemUiModel(
                id = id,
                title = name,
                subTitle = combineStateAndCountry,
                iconResId = R.drawable.ic_city,
                typeResId = R.string.city,
                distance = locationUser?.distanceTo(center)?.toInt()
            )

        Type.VILLAGE ->
            SuggestionItemUiModel(
                id = id,
                title = name,
                subTitle = combineStateAndCountry,
                iconResId = R.drawable.ic_village,
                typeResId = R.string.village,
                distance = locationUser?.distanceTo(center)?.toInt()
            )

        Type.PEAK -> SuggestionItemUiModel(
            id = id,
            title = name,
            subTitle = country,
            iconResId = R.drawable.ic_peak,
            typeResId = R.string.peak,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.SADDLE -> SuggestionItemUiModel(
            id = id,
            title = name,
            subTitle = country,
            iconResId = R.drawable.ic_saddle,
            typeResId = R.string.saddle,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.ALPINE_HUT -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_alpine_hut,
            typeResId = R.string.alpine_hut,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.WILDERNESS_HUT -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_wilderness_hut,
            typeResId = R.string.wilderness_hut,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.CAVE_ENTRANCE -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_cave_entrance,
            typeResId = R.string.cave_entrance,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.WATER -> SuggestionItemUiModel(
            id = id,
            title = name,
            subTitle = country,
            iconResId = R.drawable.ic_water,
            typeResId = R.string.water,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.SPRING -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_spring,
            typeResId = R.string.spring,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.HOTEL -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_hotel,
            typeResId = R.string.hotel,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.PLACE -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_place,
            typeResId = R.string.place,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.ATTRACTION -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_attraction,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.AMENITY -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_place,
            typeResId = R.string.place,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.VIEWPOINT -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_viewpoint,
            typeResId = R.string.viewpoint,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.PLACE_OF_WORSHIP -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_church,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.PARKING -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_parking,
            typeResId = R.string.parking,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.TRAIN_STATION -> SuggestionItemUiModel(
            id = id,
            title = combineNameAndCity,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_train_station,
            typeResId = R.string.train_station,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.FOREST -> SuggestionItemUiModel(
            id = id,
            title = name,
            subTitle = country,
            iconResId = R.drawable.ic_forest,
            typeResId = R.string.forest,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.VOLCANO -> SuggestionItemUiModel(
            id = id,
            title = name,
            subTitle = country,
            iconResId = R.drawable.ic_volcano,
            typeResId = R.string.volcano,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.BUILDING -> SuggestionItemUiModel(
            id = id,
            title = address,
            subTitle = combineStateAndCountry,
            iconResId = R.drawable.ic_building,
            typeResId = R.string.building,
            distance = locationUser?.distanceTo(center)?.toInt()
        )

        Type.OTHER -> SuggestionItemUiModel(
            id = id,
            title = name,
            subTitle = country,
            iconResId = R.drawable.ic_place,
            distance = locationUser?.distanceTo(center)?.toInt()
        )
    }

private val Suggestion.address: String
    get() = buildString {
        houseNumber?.let {
            append(it)
            append(" ")
        }
        street?.let {
            append(it)
        }
        city?.let {
            append(", ")
            append(it)
        }
    }

private val Suggestion.combineNameAndCity: String
    get() = buildString {
        if (city != null && name != null) {
            append(name)
            append(" ")
            append(city)
        } else {
            append(country)
        }
    }

private val Suggestion.combineStateAndCountry: String
    get() = state?.let { "$it, $country" } ?: country
