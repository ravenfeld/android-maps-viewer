package com.ravenfeld.maps.ui.download

interface ItemDownloadMapPresenter {
    fun onCheckedChanged(name: String, checked: Boolean)
}
