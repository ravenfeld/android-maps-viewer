package com.ravenfeld.maps.ui.preset

import androidx.compose.runtime.Immutable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ravenfeld.maps.domain.preset.repository.PresetRepository
import com.ravenfeld.maps.ui.preset.mapper.toUiModel
import com.ravenfeld.maps.ui.search.WhileSubscribedOrRetained
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PresetsViewModel @Inject constructor(
    private val presetRepository: PresetRepository
) : ViewModel() {

    private val _showDialogAddPreset = MutableStateFlow(false)

    val uiState: StateFlow<PresetsUiState> =

        combine(_showDialogAddPreset, presetRepository.getPresets()) { showDialogAddPreset, presets ->

            PresetsUiState(
                presets = presets.toUiModel(),
                showAddDialog = showDialogAddPreset
            )
        }.stateIn(
            scope = viewModelScope,
            started = WhileSubscribedOrRetained,
            initialValue = PresetsUiState.start()
        )

    fun onShowAddPreset() {
        _showDialogAddPreset.update { true }
    }

    fun onDismissAddPreset() {
        _showDialogAddPreset.update { false }
    }

    fun onSelect(presetId: Int) {
        viewModelScope.launch {
            presetRepository.select(presetId)
        }
    }

    fun onRename(presetId: Int, name: String) {
        viewModelScope.launch {
            presetRepository.rename(presetId = presetId, name = name)
        }
    }

    fun onDelete(presetId: Int) {
        viewModelScope.launch {
            presetRepository.remove(presetId)
        }
    }

    fun onAddPreset(name: String) {
        viewModelScope.launch {
            presetRepository.insert(name = name)
        }
    }
}

@Immutable
data class PresetsUiState(
    val presets: ImmutableList<PresetUiModel>,
    val showAddDialog: Boolean
) {

    companion object {
        fun start() = PresetsUiState(
            presets = persistentListOf(),
            showAddDialog = false,
        )
    }
}
