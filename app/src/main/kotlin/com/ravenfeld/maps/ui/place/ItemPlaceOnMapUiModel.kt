package com.ravenfeld.maps.ui.place

@Suppress("DataClassShouldBeImmutable")
data class ItemPlaceOnMapUiModel(
    val placeOnMapUiModel: PlaceOnMapUiModel,
) {

    var checked = false
}
