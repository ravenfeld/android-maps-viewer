package com.ravenfeld.maps.ui.info

import com.ravenfeld.maps.BuildConfig

@Suppress("DataClassShouldBeImmutable")
data class InfoUiModel(val listUiModel: List<ItemInfoUiModel>) {
    val versionName = BuildConfig.VERSION_NAME
    var clickSuperFriend = 0
}
