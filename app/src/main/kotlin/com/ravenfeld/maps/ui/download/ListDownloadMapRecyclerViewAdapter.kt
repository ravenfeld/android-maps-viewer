package com.ravenfeld.maps.ui.download

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.databinding.ItemDownloadMapBinding

private val DIFF_UTILS = object : DiffUtil.ItemCallback<ItemDownloadMapUiModel>() {

    override fun areItemsTheSame(
        oldItem: ItemDownloadMapUiModel,
        newItem: ItemDownloadMapUiModel
    ): Boolean = oldItem.name == newItem.name

    override fun areContentsTheSame(
        oldItem: ItemDownloadMapUiModel,
        newItem: ItemDownloadMapUiModel
    ) = oldItem.name == newItem.name &&
        oldItem.visible == newItem.visible &&
        oldItem.enable == newItem.enable
}

class ListDownloadMapRecyclerViewAdapter(private val presenter: ItemDownloadMapPresenter) :
    ListAdapter<ItemDownloadMapUiModel, DownloadMapViewHolder>(DIFF_UTILS) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DownloadMapViewHolder {
        val binding = ItemDownloadMapBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DownloadMapViewHolder(binding, presenter)
    }

    override fun onBindViewHolder(holder: DownloadMapViewHolder, position: Int) = holder.bind(getItem(position))
}

class DownloadMapViewHolder(
    private val binding: ItemDownloadMapBinding,
    private val presenter: ItemDownloadMapPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemDownloadMapUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}
