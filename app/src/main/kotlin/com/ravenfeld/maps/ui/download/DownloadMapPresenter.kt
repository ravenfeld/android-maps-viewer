package com.ravenfeld.maps.ui.download

interface DownloadMapPresenter {
    fun onDownloadClick()
    fun onInfoClick()
}
