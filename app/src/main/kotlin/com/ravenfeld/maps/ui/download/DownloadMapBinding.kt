package com.ravenfeld.maps.ui.download

import android.text.format.Formatter.formatFileSize
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.R
import com.ravenfeld.maps.data.utils.safeLet
import com.ravenfeld.maps.domain.map.MAX_TILES
import com.ravenfeld.maps.domain.map.SIZE_TILE

@BindingAdapter("nbTiles")
fun TextView.setNbTiles(nbTiles: Int?) {
    nbTiles?.let {
        text = if (MAX_TILES <= it) {
            context.getString(R.string.nb_tiles, "∞")
        } else {
            context.getString(R.string.nb_tiles, it.toString())
        }
    } ?: run {
        text = context.getString(R.string.nb_tiles, "∞")
    }
}

@BindingAdapter("estimateSize")
fun TextView.setEstimateSize(nbTiles: Int?) {
    nbTiles?.let {
        text = if (MAX_TILES <= it) {
            context.getString(R.string.estimate_size, "∞")
        } else {
            context.getString(R.string.estimate_size, formatFileSize(context, it * SIZE_TILE))
        }
    } ?: run {
        text = context.getString(R.string.estimate_size, "∞")
    }
}

@BindingAdapter("items", "presenter")
fun RecyclerView.bindItemDownloadMapUiModel(
    items: List<ItemDownloadMapUiModel>?,
    presenter: ItemDownloadMapPresenter?
) {
    safeLet(items, presenter) { list, onChecked ->
        val adapter = getOrCreateAdapter(this, onChecked)
        adapter.submitList(list)
    }
}

private fun getOrCreateAdapter(
    recyclerView: RecyclerView,
    presenter: ItemDownloadMapPresenter
): ListDownloadMapRecyclerViewAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ListDownloadMapRecyclerViewAdapter) {
        recyclerView.adapter as ListDownloadMapRecyclerViewAdapter
    } else {
        val bindableRecyclerAdapter = ListDownloadMapRecyclerViewAdapter(presenter)
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}
