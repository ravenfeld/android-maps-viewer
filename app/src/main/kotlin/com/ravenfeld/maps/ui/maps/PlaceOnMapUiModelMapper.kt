package com.ravenfeld.maps.ui.maps

import com.ravenfeld.maps.domain.place.model.Type
import com.ravenfeld.maps.ui.place.PlaceOnMapUiModel

internal fun List<PlaceOnMapUiModel>.toType() = map(PlaceOnMapUiModel::toType)

internal fun PlaceOnMapUiModel.toType() = when (this) {
    PlaceOnMapUiModel.PEAK -> Type.PEAK
    PlaceOnMapUiModel.SADDLE -> Type.SADDLE
    PlaceOnMapUiModel.ALPINE_HUT -> Type.ALPINE_HUT
    PlaceOnMapUiModel.WILDERNESS_HUT -> Type.WILDERNESS_HUT
    PlaceOnMapUiModel.WATER -> Type.WATER
    PlaceOnMapUiModel.PARKING -> Type.PARKING
    PlaceOnMapUiModel.PARKING_BICYCLE -> Type.PARKING_BICYCLE
    PlaceOnMapUiModel.TOILET -> Type.TOILET
    PlaceOnMapUiModel.SHOP -> Type.SHOP
    PlaceOnMapUiModel.CAMP_SITE -> Type.CAMP_SITE
}
