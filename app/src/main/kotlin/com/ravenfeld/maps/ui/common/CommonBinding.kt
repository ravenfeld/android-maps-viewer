package com.ravenfeld.maps.ui.common

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.google.android.material.slider.RangeSlider
import com.google.android.material.textfield.TextInputLayout
import com.ravenfeld.maps.R

@BindingAdapter("error")
fun TextInputLayout.setError(textError: TextError?) {
    error = when (textError) {
        TextError.EMPTY -> resources.getString(R.string.value_empty)
        TextError.URL_MALFORMED -> resources.getString(R.string.url_formatted)
        TextError.NAME_EXIST -> resources.getString(R.string.name_existed)
        null -> null
    }
}

@set:BindingAdapter("changedValues")
@get:InverseBindingAdapter(attribute = "changedValues")
var RangeSlider.changedValues: IntArray?
    get() = this.values.map { float -> float.toInt() }.toIntArray()
    set(value) {
        value?.let {
            this.values = it.toList().map { int -> int.toFloat() }
        }
    }

@BindingAdapter("changedValuesAttrChanged")
fun RangeSlider.setChangedValue(listener: InverseBindingListener) {
    addOnChangeListener { _, _, _ -> listener.onChange() }
}

@BindingAdapter("zoom")
fun TextView.setZoom(zoom: IntArray?) {
    zoom?.let {
        text = context.getString(R.string.zoom_values, zoom[0], zoom[1])
    } ?: run {
        setText(R.string.zoom)
    }
}

@BindingAdapter("imgRes")
fun ImageView.setImageResource(resource: Int) {
    setImageResource(resource)
}
