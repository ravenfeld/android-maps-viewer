package com.ravenfeld.maps.ui.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ravenfeld.maps.domain.map.MapRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ConfigMapsViewModel @Inject constructor(private val mapRepository: MapRepository) :
    ViewModel() {

    fun enable(name: String, enable: Boolean) {
        viewModelScope.launch {
            mapRepository.enable(name, enable)
        }
    }

    fun delete(name: String) {
        viewModelScope.launch {
            mapRepository.delete(name)
        }
    }
}
