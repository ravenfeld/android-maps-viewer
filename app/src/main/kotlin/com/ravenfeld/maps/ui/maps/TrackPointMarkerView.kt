package com.ravenfeld.maps.ui.maps

import android.annotation.SuppressLint
import android.content.Context
import android.widget.TextView
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.ravenfeld.maps.R
import com.ravenfeld.maps.domain.geo.TrackPoint
import com.ravenfeld.maps.ui.common.formatElevation
import com.ravenfeld.maps.ui.common.toResId

@SuppressLint("ViewConstructor")
class TrackPointMarkerView(context: Context, layoutResource: Int) : MarkerView(
    context,
    layoutResource
) {

    private val tvContent: TextView = findViewById(R.id.value)

    override fun refreshContent(e: Entry, highlight: Highlight) {
        val point = e.data as TrackPoint
        tvContent.text = buildString {
            append(formatElevation(point.geoPoint.altitude ?: 0.0))

            point.way?.tag?.let {
                appendLine()
                append(context.getString(it.highway.toResId()))
                appendLine()
                append(context.getString(it.surface.toResId()))
            }
        }
        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF = MPPointF(-(width / 2).toFloat(), -height.toFloat())
}
