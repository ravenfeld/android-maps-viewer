package com.ravenfeld.maps.ui.common

import com.ravenfeld.maps.R
import com.ravenfeld.maps.domain.geo.Highway
import com.ravenfeld.maps.domain.geo.Surface
import kotlin.math.roundToInt

@Suppress("MagicNumber")
fun formatElevation(ele: Double): String = "%.0f m".format(ele)

@Suppress("MagicNumber")
fun formatPercent(percent: Int): String {
    return if (percent > 1) {
        "%d %%".format(percent)
    } else {
        "≤1 %"
    }
}

@Suppress("MagicNumber")
fun formatDistance(dist: Double, displayDecimal: Boolean = true): String {
    return if (dist < 1_000) {
        "%.0f m".format(dist)
    } else {
        if (displayDecimal) {
            if (dist > 1_000_000) {
                "%.0f km".format(dist / 1_000.0)
            } else {
                "%.2f km".format(dist / 1_000.0)
            }
        } else {
            val roundoff = (dist * 100.0).roundToInt() / 100.0
            if (roundoff % 1_000.0 == 0.0) {
                "%.0f km".format(roundoff / 1_000.0)
            } else {
                "%.1f km".format(roundoff / 1_000.0)
            }
        }
    }
}

fun Highway.toResId() =
    when (this) {
        Highway.AlpineMountainPath -> R.string.way_type_mountain_path
        Highway.HikingPath -> R.string.way_type_hiking_path
        Highway.Path -> R.string.way_type_path
        Highway.Steps -> R.string.way_type_steps
        Highway.Cycleway -> R.string.way_type_cycleway
        Highway.Track -> R.string.way_type_track
        Highway.Street -> R.string.way_type_street
        Highway.Road -> R.string.way_type_road
        Highway.Footway -> R.string.way_type_footway
        Highway.Highway -> R.string.way_type_highway
        Highway.Unknown -> R.string.way_type_unknown
    }

fun Surface.toResId() =
    when (this) {
        Surface.Natural -> R.string.surface_natural
        Surface.Compacted -> R.string.surface_compacted
        Surface.Tarmac -> R.string.surface_tarmac
        Surface.Asphalt -> R.string.surface_asphalt
        Surface.Cobblestone -> R.string.surface_cobblestone
        Surface.Unknown -> R.string.surface_unknown
    }
