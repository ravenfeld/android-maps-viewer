package com.ravenfeld.maps.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.databinding.FragmentComposeBinding
import com.ravenfeld.maps.ui.common.getOrThrow
import com.ravenfeld.maps.ui.compose.theme.MapsViewerTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : Fragment() {

    private var _binding: FragmentComposeBinding? = null
    private val binding get() = _binding.getOrThrow()
    private val viewModel: SearchViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentComposeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.init(SearchFragmentArgs.fromBundle(requireArguments()).location)

        binding.composeView.apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                MapsViewerTheme {
                    SearchScreen(
                        modifier = Modifier.fillMaxSize(),
                        viewModel = viewModel,
                        onSuggestionSelected = {
                            setFragmentResult(
                                STATE_SUGGESTION_RESULT,
                                bundleOf(STATE_SUGGESTION_RESULT to it)
                            )
                            findNavController().navigateUp()
                        },
                        onBack = {
                            findNavController().navigateUp()
                        }
                    )
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val STATE_SUGGESTION_RESULT = "SUGGESTION_RESULT"
    }
}
