package com.ravenfeld.maps.ui.track.details

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ravenfeld.maps.R
import com.ravenfeld.maps.domain.geo.StatsSurface
import com.ravenfeld.maps.domain.geo.Surface
import com.ravenfeld.maps.ui.common.formatDistance
import com.ravenfeld.maps.ui.common.formatPercent
import com.ravenfeld.maps.ui.common.toResId
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf

@Composable
fun StatsSurface(
    statsSurface: ImmutableList<StatsSurface>,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(4.dp)
    ) {
        Text(
            text = stringResource(R.string.surface),
            style = MaterialTheme.typography.titleLarge
        )
        Surface.entries.forEach { type ->
            statsSurface.find { it.type == type }?.let {
                Text(
                    modifier = Modifier.padding(start = 8.dp),
                    text = "${stringResource(
                        it.type.toResId()
                    )}: ${formatDistance(it.distance)} (${formatPercent(it.percent)})"
                )
            }
        }
    }
}

@Preview
@Composable
private fun StatsSurfacePreview() {
    StatsSurface(
        statsSurface = persistentListOf(
            StatsSurface(Surface.Natural, 1.0, 100)
        )
    )
}
