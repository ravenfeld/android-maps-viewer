package com.ravenfeld.maps.ui.maps

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.InsetDrawable
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.MenuRes
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.widget.PopupMenu
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.clearFragmentResultListener
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavBackStackEntry
import androidx.navigation.fragment.findNavController
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.ravenfeld.maps.R
import com.ravenfeld.maps.data.mapper.toTrackPoint
import com.ravenfeld.maps.databinding.FragmentMapBinding
import com.ravenfeld.maps.domain.event.GetTrackEvent
import com.ravenfeld.maps.domain.event.LoadFileEvent
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Poi
import com.ravenfeld.maps.domain.geo.PoiSymbol
import com.ravenfeld.maps.domain.geo.TrackPoint
import com.ravenfeld.maps.domain.geo.toPoint
import com.ravenfeld.maps.domain.geo.toTrackMeasurement
import com.ravenfeld.maps.domain.map.DisplayInfo
import com.ravenfeld.maps.domain.place.model.Type
import com.ravenfeld.maps.domain.track.ExportTrack
import com.ravenfeld.maps.domain.track.ExportTrackStatus
import com.ravenfeld.maps.ui.color.ColorPickerDialog
import com.ravenfeld.maps.ui.color.ColorResult
import com.ravenfeld.maps.ui.common.WaitDialogDirections
import com.ravenfeld.maps.ui.common.alert.AlertDialog.Companion.STATE_CLICK_TYPE_KEY
import com.ravenfeld.maps.ui.common.alert.AlertDialogDirections
import com.ravenfeld.maps.ui.common.alert.AlertResult
import com.ravenfeld.maps.ui.common.formatDistance
import com.ravenfeld.maps.ui.common.formatElevation
import com.ravenfeld.maps.ui.common.getOrThrow
import com.ravenfeld.maps.ui.common.safeNavigate
import com.ravenfeld.maps.ui.edition.AddOnTrackDialog
import com.ravenfeld.maps.ui.edition.AddOnTrackResult
import com.ravenfeld.maps.ui.edition.poi.EditPoiDialog
import com.ravenfeld.maps.ui.edition.poi.EditPoiResult
import com.ravenfeld.maps.ui.edition.poi.PoiDialogMode
import com.ravenfeld.maps.ui.name.NameDialog
import com.ravenfeld.maps.ui.name.NameResult
import com.ravenfeld.maps.ui.options.Option
import com.ravenfeld.maps.ui.options.OptionsDialog
import com.ravenfeld.maps.ui.place.AddPlaceOnMapDialog
import com.ravenfeld.maps.ui.search.SearchFragment
import com.ravenfeld.maps.ui.search.SearchResult
import com.ravenfeld.maps.ui.track.export.ExportTrackDialog
import com.ravenfeld.maps.ui.track.export.ExportType
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import org.maplibre.android.MapLibre
import org.maplibre.android.geometry.LatLng
import org.maplibre.geojson.LineString
import org.maplibre.geojson.Point
import org.maplibre.turf.TurfConstants
import org.maplibre.turf.TurfMisc
import kotlin.math.roundToInt

private const val TAG_ALERT_OK = "TAG_ALERT_OK"
private const val TAG_ALERT_OK_SEND_TRACK = "TAG_ALERT_OK_SEND_TRACK"
private const val TAG_ALERT_DELETE = "TAG_ALERT_DELETE"
private const val TAG_ALERT_REMOVE_POI = "TAG_ALERT_REMOVE_POI"
private const val CHART_FULL = "CHART_FULL"
private const val CHART_PROGRESS = "CHART_PROGRESS"
private const val MIN_ELEVATION_RANGE = 50

@Suppress("LargeClass", "TooManyFunctions")
@AndroidEntryPoint
class MapFragment : Fragment(), OnChartValueSelectedListener, MapPresenter {

    private val viewModel: MapViewModel by activityViewModels()

    private var _binding: FragmentMapBinding? = null
    private val binding get() = _binding.getOrThrow()

    private var mapManager: MapManager? = null

    private var bottomSheetTracksBehavior: BottomSheetBehavior<LinearLayout>? = null
    private var bottomSheetMapsBehavior: BottomSheetBehavior<LinearLayout>? = null
    private var bottomSheetChartBehavior: BottomSheetBehavior<ConstraintLayout>? = null
    private val bottomSheetChartCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            when (newState) {
                BottomSheetBehavior.STATE_COLLAPSED -> {
                    mapManager?.trackPointUnselected()
                }

                BottomSheetBehavior.STATE_HIDDEN -> {
                    viewModel.uiModel.displayInfo = DisplayInfo.NONE
                    viewModel.uiModel.track = null
                    viewModel.uiModel.isEditMode = false
                    mapManager?.onEditModeChanged()
                    mapManager?.trackPointUnselected()
                }

                BottomSheetBehavior.STATE_DRAGGING,
                BottomSheetBehavior.STATE_EXPANDED,
                BottomSheetBehavior.STATE_HALF_EXPANDED,
                BottomSheetBehavior.STATE_SETTLING -> Unit
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            // no impl
        }
    }

    private var bottomSheetEditModeTutoBehavior: BottomSheetBehavior<LinearLayout>? = null
    private val bottomSheetEditModeTutoCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN && viewModel.uiModel.track == null) {
                viewModel.uiModel.displayInfo = DisplayInfo.NONE
                viewModel.uiModel.isEditMode = false
                mapManager?.onEditModeChanged()
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            // no impl
        }
    }

    private val callbackOnBack: OnBackPressedCallback =
        object : OnBackPressedCallback(false) {
            override fun handleOnBackPressed() {
                if (bottomSheetTracksBehavior?.state != BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetTracksBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                } else if (bottomSheetMapsBehavior?.state != BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetMapsBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                } else if (bottomSheetChartBehavior?.state != BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetChartBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                } else if (bottomSheetEditModeTutoBehavior?.state != BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                } else if (viewModel.uiModel.displayAreaDownload) {
                    viewModel.uiModel.displayAreaDownload = false
                }

                isEnabled = bottomSheetTracksBehavior?.state == BottomSheetBehavior.STATE_EXPANDED ||
                    bottomSheetTracksBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED ||
                    bottomSheetMapsBehavior?.state == BottomSheetBehavior.STATE_EXPANDED ||
                    bottomSheetMapsBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED ||
                    bottomSheetChartBehavior?.state == BottomSheetBehavior.STATE_EXPANDED ||
                    bottomSheetChartBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED ||
                    bottomSheetEditModeTutoBehavior?.state == BottomSheetBehavior.STATE_EXPANDED ||
                    bottomSheetEditModeTutoBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED ||
                    viewModel.uiModel.displayAreaDownload
            }
        }

    private var navBackStackEntry: NavBackStackEntry? = null

    private val locationManager by lazy {
        requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    private val googleRegisterResult = registerForActivityResult(
        ActivityResultContracts.StartIntentSenderForResult()
    ) { activityResult ->
        if (activityResult.resultCode == Activity.RESULT_OK) {
            onSuccessLocation()
        }
    }

    private val observerOnCreate = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_CREATE) {
            navBackStackEntry?.savedStateHandle?.getLiveData<AlertResult>(STATE_CLICK_TYPE_KEY)
                ?.observe(viewLifecycleOwner) { result ->
                    findNavController().navigateUp()
                    when {
                        result.tag == TAG_ALERT_DELETE &&
                            result.click == AlertResult.Click.CLICK_RIGHT ->
                            result.content?.toInt()
                                ?.let {
                                    hideDisplayInfo()
                                    viewModel.trackRemove(it)
                                    viewModel.uiModel.touchInfo = null
                                    mapManager?.trackPointUnselected()
                                }

                        result.tag == TAG_ALERT_OK_SEND_TRACK -> {
                            requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                            requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
                        }

                        result.tag == TAG_ALERT_REMOVE_POI && result.click == AlertResult.Click.CLICK_RIGHT -> {
                            result.content?.let {
                                val json = Json { ignoreUnknownKeys = true }
                                val point = json.decodeFromString(GeoPoint.serializer(), it)
                                viewModel.uiModel.track?.id?.let { id ->
                                    viewModel.removePoi(trackId = id, point = point)
                                }
                            }
                        }
                    }
                }

            navBackStackEntry?.savedStateHandle?.getLiveData<ExportTrack>(ExportTrackDialog.STATE_CLICK_SEND_TRACK)
                ?.observe(viewLifecycleOwner) { result ->
                    uiExportTrack(result)
                }

            navBackStackEntry?.savedStateHandle?.getLiveData<ColorResult>(ColorPickerDialog.STATE_COLOR_SELECTED)
                ?.observe(viewLifecycleOwner) { result ->
                    viewModel.trackColor(result.trackId, result.color)
                    findNavController().navigateUp()
                }

            navBackStackEntry?.savedStateHandle?.getLiveData<NameResult>(NameDialog.STATE_NAME_SELECTED)
                ?.observe(viewLifecycleOwner) { result ->
                    viewModel.trackName(result.trackId, result.name)
                    findNavController().navigateUp()
                }

            navBackStackEntry?.savedStateHandle?.getLiveData<Option>(OptionsDialog.STATE_OPTION_SELECTED)
                ?.observe(viewLifecycleOwner) { result ->
                    when (result) {
                        Option.REVERSE -> onTrackReverseClick()
                        Option.BACK_HOME -> onTrackBackHomeClick()
                        Option.REMOVE_LAST_POINT -> onTrackRemoveLastPointClick()
                    }
                    findNavController().navigateUp()
                }
            navBackStackEntry?.savedStateHandle?.getLiveData<AddOnTrackResult>(
                AddOnTrackDialog.STATE_ADD_ON_TRACK_SELECTED
            )
                ?.observe(viewLifecycleOwner) { result ->
                    val json = Json { ignoreUnknownKeys = true }
                    val point = json.decodeFromString(GeoPoint.serializer(), result.point)
                    viewModel.uiModel.track?.id?.let { id ->
                        when (result.type) {
                            AddOnTrackResult.Type.WAYPOINT -> {
                                viewModel.addWaypoint(
                                    trackId = id,
                                    point = point
                                )
                                findNavController().navigateUp()
                            }

                            AddOnTrackResult.Type.POI -> {
                                findNavController().navigateUp()
                                findNavController().safeNavigate(
                                    MapFragmentDirections.actionToEditPoiFragment(
                                        PoiDialogMode.ADD,
                                        null,
                                        result.point,
                                        PoiSymbol.GENERIC.name,
                                    )
                                )
                            }
                        }
                    }
                }

            navBackStackEntry?.savedStateHandle?.getLiveData<EditPoiResult>(EditPoiDialog.STATE_EDIT_POI_SELECTED)
                ?.observe(viewLifecycleOwner) { result ->
                    when (result) {
                        is EditPoiResult.Remove -> {
                            val json = Json { ignoreUnknownKeys = true }
                            val point = json.decodeFromString(
                                GeoPoint.serializer(),
                                result.point
                            )
                            viewModel.uiModel.track?.id?.let { id ->
                                viewModel.removePoi(trackId = id, point = point)
                            }
                        }

                        is EditPoiResult.Save -> {
                            val json = Json { ignoreUnknownKeys = true }
                            val point = json.decodeFromString(
                                GeoPoint.serializer(),
                                result.point
                            )
                            viewModel.uiModel.track?.id?.let { id ->
                                viewModel.savePoi(
                                    trackId = id,
                                    poi = Poi(
                                        point = point,
                                        name = result.name,
                                        symbol = result.symbol
                                    )
                                )
                            }
                        }
                    }
                    findNavController().navigateUp()
                }

            navBackStackEntry?.savedStateHandle?.getLiveData<List<Type>>(
                AddPlaceOnMapDialog.STATE_POI_ON_MAP_SELECTED
            )
                ?.observe(viewLifecycleOwner) { result ->
                    mapManager?.getCameraBounds()?.let {
                        viewModel.onPoiOnMapChanged(
                            typePoiOnMaps = result,
                            latLngBounds = it
                        )
                    }
                    findNavController().navigateUp()
                }
        }
    }

    private val observerOnDestroy = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_DESTROY) {
            navBackStackEntry?.lifecycle?.removeObserver(observerOnCreate)
            navBackStackEntry?.savedStateHandle?.remove<AlertResult>(STATE_CLICK_TYPE_KEY)
            navBackStackEntry?.savedStateHandle?.remove<ExportTrack>(ExportTrackDialog.STATE_CLICK_SEND_TRACK)
            navBackStackEntry?.savedStateHandle?.remove<ColorResult>(ColorPickerDialog.STATE_COLOR_SELECTED)
            navBackStackEntry?.savedStateHandle?.remove<NameResult>(NameDialog.STATE_NAME_SELECTED)
            navBackStackEntry?.savedStateHandle?.remove<Option>(OptionsDialog.STATE_OPTION_SELECTED)
            navBackStackEntry?.savedStateHandle?.remove<AddOnTrackResult>(AddOnTrackDialog.STATE_ADD_ON_TRACK_SELECTED)
            navBackStackEntry?.savedStateHandle?.remove<EditPoiResult>(EditPoiDialog.STATE_EDIT_POI_SELECTED)
            navBackStackEntry?.savedStateHandle?.remove<List<Type>>(
                AddPlaceOnMapDialog.STATE_POI_ON_MAP_SELECTED
            )
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                viewModel.uiModel.track?.let {
                    findNavController().safeNavigate(
                        MapFragmentDirections.actionToExportTrackDialog(
                            it.name,
                            ExportType.GPX
                        )
                    )
                } ?: run {
                    findNavController().safeNavigate(
                        AlertDialogDirections.actionToAlertOk(
                            TAG_ALERT_OK,
                            getString(R.string.no_select_track)
                        )
                    )
                }
            } else {
                findNavController().safeNavigate(
                    AlertDialogDirections.actionToAlertOk(
                        TAG_ALERT_OK,
                        getString(R.string.external_storage)
                    )
                )
            }
        }

    override fun onDestroy() {
        super.onDestroy()
        navBackStackEntry?.lifecycle?.removeObserver(observerOnDestroy)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapLibre.getInstance(requireContext())
        viewModel.init()

        val locationPermissionRequest = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { permissions ->
            if (permissions[Manifest.permission.ACCESS_FINE_LOCATION] == true ||
                permissions[Manifest.permission.ACCESS_COARSE_LOCATION] == true
            ) {
                mapManager?.activeLocation()
                viewModel.getAltitudeMsl()
            }
        }

        locationPermissionRequest.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapBinding.inflate(inflater, container, false)
        lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.loadFileState.collectLatest {
                    when (it) {
                        LoadFileEvent.LOADING -> findNavController().safeNavigate(
                            WaitDialogDirections.actionToWaitDialog(null)
                        )

                        LoadFileEvent.ERROR -> {
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK,
                                    getString(R.string.load_file_error)
                                )
                            )
                        }

                        LoadFileEvent.SUCCESS -> {
                            findNavController().navigateUp()
                        }
                    }
                    viewModel.resetLoadFile()
                }
            }
        }

        return binding.root
    }

    @Suppress("LongMethod", "CyclomaticComplexMethod")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navBackStackEntry = findNavController().getBackStackEntry(R.id.mapFragment)
        navBackStackEntry?.lifecycle?.addObserver(observerOnCreate)
        viewLifecycleOwner.lifecycle.addObserver(observerOnDestroy)
        bottomSheetTracksBehavior = BottomSheetBehavior.from(binding.bottomSheetTracks)
        bottomSheetMapsBehavior = BottomSheetBehavior.from(binding.bottomSheetMaps)
        bottomSheetChartBehavior = BottomSheetBehavior.from(binding.bottomSheetChart)
        bottomSheetChartBehavior?.addBottomSheetCallback(bottomSheetChartCallback)
        bottomSheetEditModeTutoBehavior = BottomSheetBehavior.from(binding.bottomSheetEditModeTuto)
        bottomSheetEditModeTutoBehavior?.addBottomSheetCallback(bottomSheetEditModeTutoCallback)
        bottomSheetTracksBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetMapsBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetChartBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetEditModeTutoBehavior?.isDraggable = false
        binding.presenter = this
        binding.uiModel = viewModel.uiModel
        mapManager = MapManager(
            context = requireContext(),
            mapView = binding.simpleMapView,
            compassView = binding.compass,
            uiModel = viewModel.uiModel,
            onMapReady = {
                lifecycleScope.launch {
                    viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                        viewModel.map.collectLatest { pairMaps ->
                            mapManager?.setStyle(pairMaps.first, pairMaps.second)
                        }
                    }
                }
            },
            onMapLoaded = {
                setFragmentResultListener(SearchFragment.STATE_SUGGESTION_RESULT) { _, bundle ->
                    @Suppress("DEPRECATION")
                    val result = bundle.getParcelable<SearchResult>(SearchFragment.STATE_SUGGESTION_RESULT)
                    result?.let {
                        result.bounds?.let {
                            mapManager?.cameraMove(
                                locationNorthEast = result.bounds.northEast,
                                locationSouthWest = result.bounds.southWest,
                                zoomMax = 15.0,
                                paddingDp = 0f
                            )
                        } ?: mapManager?.cameraMove(result.center)
                    }
                }
            },
            onLoadRoute = {
                lifecycleScope.launch {
                    viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                        viewModel.tracks.collectLatest {
                            mapManager?.loadTracks(it)
                        }
                    }
                }
                lifecycleScope.launch {
                    viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                        viewModel.placeOnMaps.collectLatest {
                            mapManager?.updatePlaceOnMap(it)
                        }
                    }
                }
            },
            onPoint = {
                bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                viewModel.trackStart(it)
            },
            onMovePoint = { oldPoint, newPoint, zoom ->
                lifecycleScope.launch {
                    viewModel.uiModel.track?.id?.let { id ->
                        viewModel.movePoint(
                            id,
                            oldPoint,
                            newPoint,
                            zoom,
                            viewModel.uiModel.routingModeEnable()
                        ).collectLatest {
                            if (it == GetTrackEvent.LOADING) {
                                findNavController().safeNavigate(
                                    WaitDialogDirections.actionToWaitDialog(
                                        null
                                    )
                                )
                            } else {
                                findNavController().navigateUp()
                                if (it == GetTrackEvent.ERROR) {
                                    findNavController().safeNavigate(
                                        AlertDialogDirections.actionToAlertOk(
                                            TAG_ALERT_OK,
                                            getString(R.string.routing_error)
                                        )
                                    )
                                }
                            }
                        }
                    }
                }
            },
            onAddPoint = {
                val json = Json { ignoreUnknownKeys = true }
                val point = json.encodeToString(GeoPoint.serializer(), it)
                findNavController().safeNavigate(
                    MapFragmentDirections.actionToAddOnTrackFragment(point)
                )
            },
            onSelectPoi = {
                val json = Json { ignoreUnknownKeys = true }
                val point = json.encodeToString(GeoPoint.serializer(), it.point)
                findNavController().safeNavigate(
                    MapFragmentDirections.actionToEditPoiFragment(
                        PoiDialogMode.EDIT,
                        it.name,
                        point,
                        it.symbol.name,
                    )
                )
            },
            onGetTrack = { point, zoom ->
                lifecycleScope.launch {
                    viewModel.uiModel.track?.id?.let { id ->
                        viewModel.getSegment(
                            id,
                            point,
                            zoom,
                            viewModel.uiModel.routingModeEnable()
                        ).collectLatest {
                            if (it == GetTrackEvent.LOADING) {
                                findNavController().safeNavigate(
                                    WaitDialogDirections.actionToWaitDialog(
                                        null
                                    )
                                )
                            } else {
                                findNavController().navigateUp()
                                if (it == GetTrackEvent.ERROR) {
                                    findNavController().safeNavigate(
                                        AlertDialogDirections.actionToAlertOk(
                                            TAG_ALERT_OK,
                                            getString(R.string.routing_error)
                                        )
                                    )
                                }
                            }
                        }
                    }
                }
            },
            onLocationInfo = { cameraPosition, userLocation ->
                viewModel.getLocationInfo(cameraPosition, userLocation)
            },
            onTrackSelected = { track, show ->
                onTrackUiModelSelected(track, show)
            },
            onTrackPointUnselected = {
                binding.chart.highlightValue(null)
                binding.chart.getLineDataSet(CHART_PROGRESS)?.values = emptyList()
                viewModel.uiModel.touchInfo = null
            },
            onCameraMoveEnded = {
                mapManager?.getCameraBounds()?.let {
                    viewModel.onCameraMoveEnded(it)
                }
            }
        )
        initChart(binding.chart)
        mapManager?.onCreate(savedInstanceState)
    }

    private fun initChart(chart: LineChart) {
        chart.legend.isEnabled = false
        val xAxis: XAxis = chart.xAxis
        xAxis.setDrawGridLines(false)
        xAxis.setDrawAxisLine(false)
        xAxis.valueFormatter = object : ValueFormatter() {
            override fun getAxisLabel(
                value: Float,
                axis: AxisBase
            ): String = formatDistance(value.toDouble(), false)
        }
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.axisLeft.valueFormatter = object : ValueFormatter() {
            override fun getAxisLabel(
                value: Float,
                axis: AxisBase
            ): String = formatElevation(value.toDouble())
        }
        chart.axisRight.isEnabled = false

        chart.description.isEnabled = false
        chart.setScaleEnabled(false)
        chart.setPinchZoom(false)
        chart.setTouchEnabled(true)
        chart.setOnChartValueSelectedListener(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapManager?.onDestroyView()
        mapManager = null
        bottomSheetChartBehavior?.removeBottomSheetCallback(bottomSheetChartCallback)
        bottomSheetEditModeTutoBehavior?.removeBottomSheetCallback(
            bottomSheetEditModeTutoCallback
        )
        _binding = null
        bottomSheetTracksBehavior = null
        bottomSheetMapsBehavior = null
        bottomSheetChartBehavior = null
        clearFragmentResultListener(SearchFragment.STATE_SUGGESTION_RESULT)
    }

    override fun onStart() {
        super.onStart()
        mapManager?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapManager?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapManager?.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapManager?.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapManager?.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapManager?.onLowMemory()
    }

    private fun displayTrack() {
        callbackOnBack.isEnabled = true
        if (bottomSheetChartBehavior?.state == BottomSheetBehavior.STATE_HIDDEN) {
            bottomSheetChartBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
        }
        bottomSheetMapsBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetTracksBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
    }

    @Suppress("LongMethod")
    private fun displayTrackInfo(track: TrackUiModel, show: Boolean) {
        val values = track.stats.chartElevation.mapIndexed { index, elePoint ->
            Entry(
                elePoint.dist.toFloat(),
                elePoint.elevation.toFloat(),
                track.trackPoints[index]
            )
        }
        if (values.isNotEmpty()) {
            val lineDataFull = LineDataSet(values, CHART_FULL)
            val highlight = binding.chart.highlighted?.last()
            val lineDataProgress = LineDataSet(
                values.takeWhile { it.x <= (highlight?.x ?: 0f) },
                CHART_PROGRESS
            )
            lineDataFull.axisDependency = YAxis.AxisDependency.LEFT
            lineDataFull.setDrawCircles(false)
            lineDataFull.highLightColor = ContextCompat.getColor(
                requireContext(),
                R.color.fill_chart
            )
            lineDataFull.setDrawHorizontalHighlightIndicator(false)
            lineDataFull.fillColor = ContextCompat.getColor(requireContext(), R.color.fill_chart)
            lineDataFull.setDrawFilled(true)
            lineDataFull.color = Color.BLACK
            lineDataFull.lineWidth = 1f
            lineDataFull.mode = LineDataSet.Mode.LINEAR
            lineDataFull.setDrawValues(false)

            lineDataProgress.axisDependency = YAxis.AxisDependency.LEFT
            lineDataProgress.setDrawCircles(false)
            lineDataProgress.isHighlightEnabled = false
            lineDataProgress.fillColor = ContextCompat.getColor(
                requireContext(),
                R.color.progressChart
            )
            lineDataProgress.setDrawFilled(true)
            lineDataProgress.color = Color.BLACK
            lineDataProgress.lineWidth = 1f
            lineDataProgress.mode = LineDataSet.Mode.LINEAR
            lineDataProgress.setDrawValues(false)

            val trackPointMarkerView = TrackPointMarkerView(
                context = this.requireContext(),
                layoutResource = R.layout.elevation_marker_view
            )
            trackPointMarkerView.chartView = binding.chart
            binding.chart.marker = trackPointMarkerView
            val data = LineData(lineDataFull, lineDataProgress)
            binding.chart.data = data

            val minElevation = track.stats.chartElevation.minOf { it.elevation }
            val maxElevation = track.stats.chartElevation.maxOf { it.elevation }
            val elevationRange = maxElevation - minElevation
            if (elevationRange > MIN_ELEVATION_RANGE) {
                binding.chart.axisLeft.axisMinimum = minElevation.toFloat()
                binding.chart.axisLeft.axisMaximum = maxElevation.toFloat()
            } else {
                val halfDistToMinElevationRange = ((MIN_ELEVATION_RANGE - elevationRange) / 2f).roundToInt()
                binding.chart.axisLeft.axisMinimum = (minElevation - halfDistToMinElevationRange).toFloat()
                binding.chart.axisLeft.axisMaximum = (maxElevation + halfDistToMinElevationRange).toFloat()
            }

            binding.chart.invalidate()
        }
        if (show) {
            displayTrack()
        }
    }

    private fun hideDisplayInfo() {
        bottomSheetChartBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        viewModel.uiModel.displayInfo = DisplayInfo.NONE
    }

    override fun onMapAlphaChanged(name: String, alpha: Float) {
        mapManager?.setAlpha(name, alpha)
    }

    override fun onMapAlphaSave(name: String, alpha: Float) {
        @Suppress("MagicNumber")
        viewModel.mapAlpha(name, (alpha / 100.0).toFloat())
    }

    override fun onMapVisibilityChanged(name: String, visible: Boolean) {
        viewModel.mapVisible(name, visible)
    }

    override fun onMapMoveOrder(from: Int, to: Int) {
        viewModel.mapMoveOrder(from, to)
    }

    override fun onTracksClick() {
        val state =
            if (bottomSheetTracksBehavior?.state == BottomSheetBehavior.STATE_EXPANDED ||
                bottomSheetTracksBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED
            ) {
                BottomSheetBehavior.STATE_HIDDEN
            } else {
                callbackOnBack.isEnabled = true
                BottomSheetBehavior.STATE_COLLAPSED
            }
        bottomSheetTracksBehavior?.state = state
        if (viewModel.uiModel.isEditMode && viewModel.uiModel.track == null) {
            bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    override fun onEditTrackClick() {
        callbackOnBack.isEnabled = true
        bottomSheetMapsBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetTracksBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        viewModel.uiModel.track = null
        viewModel.uiModel.touchInfo = null
        viewModel.uiModel.displayInfo = DisplayInfo.TRACK
        viewModel.uiModel.isEditMode = true
        bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        mapManager?.trackPointUnselected()
    }

    override fun onMapsClick() {
        val state =
            if (bottomSheetMapsBehavior?.state == BottomSheetBehavior.STATE_EXPANDED ||
                bottomSheetMapsBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED
            ) {
                BottomSheetBehavior.STATE_HIDDEN
            } else {
                callbackOnBack.isEnabled = true
                BottomSheetBehavior.STATE_COLLAPSED
            }
        bottomSheetMapsBehavior?.state = state
        if (viewModel.uiModel.isEditMode && viewModel.uiModel.track == null) {
            bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    override fun onTrackVisibilityChanged(trackId: Int, visible: Boolean) {
        viewModel.trackVisible(trackId, visible)
        if (!visible && viewModel.uiModel.track?.id == trackId) {
            hideDisplayInfo()
        }
    }

    override fun onTrackColorClick(trackId: Int, color: String) {
        findNavController().safeNavigate(
            MapFragmentDirections.actionToColorPickerFragment(
                trackId,
                Color.parseColor(
                    color
                )
            )
        )
    }

    override fun onTrackNameClick(trackId: Int, name: String) {
        findNavController().safeNavigate(
            MapFragmentDirections.actionToNameFragment(
                trackId,
                name
            )
        )
    }

    override fun onTrackSelected(trackId: Int) {
        viewModel.uiModel.listTrackUiModel.find { it.id == trackId }?.let {
            if (it.trackPoints.isNotEmpty()) {
                mapManager?.cameraMove(
                    locationNorthEast = it.locationNorthEast,
                    locationSouthWest = it.locationSouthWest
                )
            }
            viewModel.uiModel.isEditMode = false
            viewModel.trackVisible(trackId, true)
            onTrackUiModelSelected(it, true)
        }
    }

    private fun onTrackUiModelSelected(track: TrackUiModel, show: Boolean) {
        viewModel.uiModel.displayInfo = DisplayInfo.TRACK
        if (viewModel.uiModel.track?.id != track.id) {
            mapManager?.trackPointUnselected()
        }
        viewModel.uiModel.track = track
        if (track.trackPoints.isEmpty()) {
            callbackOnBack.isEnabled = true
            if (bottomSheetEditModeTutoBehavior?.state == BottomSheetBehavior.STATE_HIDDEN) {
                bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
            }
            bottomSheetMapsBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
            bottomSheetTracksBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        } else {
            bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
            displayTrackInfo(track, show)
        }
    }

    override fun onTrackDeleteClick() {
        viewModel.uiModel.track?.id?.let {
            findNavController().safeNavigate(
                AlertDialogDirections.actionToAlertCustom(
                    TAG_ALERT_DELETE,
                    getString(R.string.delete_draw_track),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
                ).setContent(it.toString())
            )
        }
    }

    override fun onTrackDeleteLongClick(trackId: Int): Boolean {
        findNavController().safeNavigate(
            AlertDialogDirections.actionToAlertCustom(
                TAG_ALERT_DELETE,
                getString(R.string.delete_draw_track),
                getString(android.R.string.ok),
                getString(android.R.string.cancel),
            ).setContent(trackId.toString())
        )
        return true
    }

    override fun onLocationClick() {
        val statusGpsNow = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (statusGpsNow) {
            mapManager?.setTrackingMode()
        } else {
            AskLocation(
                activity = requireActivity(),
                onSuccess = {
                    onSuccessLocation()
                },
                onLaunchSettingsRequest = { intent ->
                    googleRegisterResult.launch(intent)
                },
                onAirPlaneMode = {
                    onFailureLocation()
                }
            ).launch()
        }
    }

    private fun onSuccessLocation() {
        mapManager?.setTrackingMode()
    }

    private fun onFailureLocation() {
        startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
    }

    override fun onCompassClick() {
        mapManager?.setCameraMode()
    }

    override fun onMenuClick(view: View) {
        if (viewModel.uiModel.hasTrackDisplay()) {
            showMenu(view, R.menu.popup_menu_with_track)
        } else {
            showMenu(view, R.menu.popup_menu)
        }
    }

    @SuppressLint("RestrictedApi")
    @Suppress("ComplexMethod", "LongMethod")
    private fun showMenu(v: View, @MenuRes menuRes: Int) {
        val popup = PopupMenu(requireContext(), v)
        popup.menuInflater.inflate(menuRes, popup.menu)
        val offlineEnable = viewModel.getOfflineEnable()
        popup.menu.findItem(R.id.menu_maps_offline).isVisible = offlineEnable
        popup.menu.findItem(R.id.menu_maps_all).isVisible = !offlineEnable
        lifecycleScope.launch {
            val name = viewModel.getPresetSelected().name.ifEmpty { getString(R.string.presets_default_name) }
            popup.menu.findItem(R.id.menu_config_preset)
                .setTitle(getString(R.string.menu_config_preset, name))
        }
        if (popup.menu is MenuBuilder) {
            val menuBuilder = popup.menu as MenuBuilder
            menuBuilder.setOptionalIconsVisible(true)
            for (item in menuBuilder.visibleItems) {
                val iconMarginPx = requireContext().resources.getDimensionPixelSize(R.dimen.margin_icon_menu)
                if (item.icon != null) {
                    item.icon = InsetDrawable(item.icon, iconMarginPx, 0, iconMarginPx, 0)
                }
            }
        }
        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            when (menuItem.itemId) {
                R.id.menu_config_map ->
                    findNavController().safeNavigate(MapFragmentDirections.actionToConfigMapsFragment())

                R.id.menu_config_preset ->
                    findNavController().safeNavigate(MapFragmentDirections.actionToConfigPresetsFragment())

                R.id.menu_download_area -> {
                    callbackOnBack.isEnabled = true
                    viewModel.uiModel.displayAreaDownload = true
                    viewModel.uiModel.isEditMode = false
                    mapManager?.onEditModeChanged()
                    bottomSheetChartBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                    bottomSheetMapsBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                    bottomSheetTracksBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                    bottomSheetEditModeTutoBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                }

                R.id.menu_send_watch ->
                    if ((
                            requireContext().getSystemService(
                                Context.BLUETOOTH_SERVICE
                            ) as BluetoothManager
                            ).adapter.isEnabled
                    ) {
                        viewModel.uiModel.track?.let {
                            findNavController().safeNavigate(
                                MapFragmentDirections.actionToExportTrackDialog(
                                    it.name,
                                    ExportType.FIT
                                )
                            )
                        } ?: run {
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK,
                                    getString(R.string.no_select_track)
                                )
                            )
                        }
                    } else {
                        findNavController().safeNavigate(
                            AlertDialogDirections.actionToAlertOk(
                                TAG_ALERT_OK,
                                getString(R.string.ble_enable)
                            )
                        )
                    }

                R.id.menu_export ->
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q && ContextCompat.checkSelfPermission(
                            requireContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        requestPermissionLauncher.launch(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                    } else {
                        viewModel.uiModel.track?.let {
                            findNavController().safeNavigate(
                                MapFragmentDirections.actionToExportTrackDialog(
                                    it.name,
                                    ExportType.GPX
                                )
                            )
                        } ?: run {
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK,
                                    getString(R.string.no_select_track)
                                )
                            )
                        }
                    }

                R.id.menu_maps_offline -> viewModel.setMapsOfflineEnable(false)
                R.id.menu_maps_all -> viewModel.setMapsOfflineEnable(true)
            }
            true
        }
        popup.show()
    }

    override fun onSaveAreaClick() {
        val area = mapManager?.getProjectionPoints(binding.downloadArea.points)
        area?.let {
            callbackOnBack.isEnabled = false
            viewModel.uiModel.displayAreaDownload = false
            findNavController().safeNavigate(MapFragmentDirections.actionToDownloadMapDialog(it))
        }
    }

    override fun onInfoClick() {
        findNavController().safeNavigate(MapFragmentDirections.actionToInfoFragment())
    }

    override fun onRoutingChange() {
        if (viewModel.uiModel.engineRoutingReady) {
            findNavController().safeNavigate(MapFragmentDirections.actionToRoutingFragment())
        } else {
            findNavController().safeNavigate(
                AlertDialogDirections.actionToAlertOk(
                    TAG_ALERT_OK,
                    getString(R.string.routing_not_ready)
                )
            )
        }
    }

    private fun onTrackBackHomeClick() {
        lifecycleScope.launch {
            viewModel.getTrackBack(
                mapManager?.getCameraPosition()?.zoom ?: 0.0,
            ).collectLatest {
                if (it == GetTrackEvent.LOADING) {
                    findNavController().safeNavigate(
                        WaitDialogDirections.actionToWaitDialog(
                            null
                        )
                    )
                } else {
                    findNavController().navigateUp()
                    if (it == GetTrackEvent.ERROR) {
                        findNavController().safeNavigate(
                            AlertDialogDirections.actionToAlertOk(
                                TAG_ALERT_OK,
                                getString(R.string.routing_error)
                            )
                        )
                    }
                }
            }
        }
    }

    private fun onTrackRemoveLastPointClick() {
        viewModel.uiModel.track?.id?.let {
            viewModel.removeLastPoint(it)
        }
    }

    override fun onTrackCancelLastActionClick() {
        lifecycleScope.launch {
            viewModel.uiModel.track?.id?.let {
                mapManager?.trackPointUnselected()
                viewModel.cancelLastUserAction(it).collectLatest {
                    if (it == GetTrackEvent.LOADING) {
                        findNavController().safeNavigate(
                            WaitDialogDirections.actionToWaitDialog(
                                null
                            )
                        )
                    } else {
                        findNavController().navigateUp()
                    }
                }
            }
        }
    }

    private fun onTrackReverseClick() {
        viewModel.uiModel.track?.id?.let {
            viewModel.reverse(it)
        }
    }

    override fun onTrackEditClick() {
        viewModel.uiModel.isEditMode = !viewModel.uiModel.isEditMode
        mapManager?.onEditModeChanged()
    }

    override fun onMoreActionsClick() {
        val options = mutableListOf<Option>()
        options.add(Option.REMOVE_LAST_POINT)
        options.add(Option.REVERSE)
        if (viewModel.uiModel.canBackHome) {
            options.add(Option.BACK_HOME)
        }
        findNavController().safeNavigate(MapFragmentDirections.actionToOptionsFragment(options.toTypedArray()))
    }

    override fun onSearchClick() {
        val lastLocation = if (ActivityCompat.checkSelfPermission(
                this.requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                this.requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        } else {
            null
        }

        findNavController().safeNavigate(
            MapFragmentDirections.actionToSearchFragment(
                lastLocation?.let {
                    LatLng(latitude = lastLocation.latitude, longitude = lastLocation.longitude)
                }
            )
        )
    }

    override fun onAddPoiOnMapClick() {
        findNavController().safeNavigate(MapFragmentDirections.actionToAddPlaceOnMapDialog())
    }

    override fun onDisplayTrackDetails() {
        viewModel.uiModel.track?.let {
            findNavController()
                .safeNavigate(
                    MapFragmentDirections.actionToDisplayTrackDetailsDialog(
                        it.stats.statsHighway.toTypedArray(),
                        it.stats.statsSurface.toTypedArray()
                    )
                )
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callbackOnBack
        )
    }

    @Suppress("NestedBlockDepth")
    override fun onValueSelected(e: Entry?, h: Highlight?) {
        if (e != null) {
            val lineDataFull = binding.chart.getLineDataSet(CHART_FULL)
            val lineDataProgress = binding.chart.getLineDataSet(CHART_PROGRESS)
            lineDataProgress?.values = lineDataFull?.values?.takeWhile { it.x <= e.x }
            viewModel.uiModel.track?.let { track ->

                if (e.x == 0f) {
                    viewModel.uiModel.touchInfo = TouchInfoUiModel(
                        distance = 0.0,
                        upHill = 0.0,
                        downHill = 0.0
                    )
                } else {
                    val lineSlice = TurfMisc.lineSliceAlong(
                        LineString.fromLngLats(
                            track.trackPoints.toPoint()
                        ),
                        0.0,
                        e.x.toDouble(),
                        TurfConstants.UNIT_METERS
                    ).coordinates().toMutableList()

                    val lastPoint = lineSlice.last()
                    if (!lastPoint.hasAltitude()) {
                        lineSlice[lineSlice.size - 1] = Point.fromLngLat(
                            lastPoint.longitude(),
                            lastPoint.latitude(),
                            lineSlice[lineSlice.size - 2].altitude()
                        )
                    }
                    val measurement = lineSlice.map { it.toTrackPoint() }.toTrackMeasurement()
                    viewModel.uiModel.touchInfo = TouchInfoUiModel(
                        distance = measurement.distance,
                        upHill = measurement.upHill,
                        downHill = measurement.downHill
                    )
                }
                mapManager?.trackPointSelected(
                    trackPointSelected = e.data as TrackPoint,
                    track = track
                )
            }
        }
    }

    override fun onNothingSelected() {
        mapManager?.trackPointUnselected()
    }

    @Suppress("LongMethod")
    private fun uiExportTrack(result: ExportTrack) {
        findNavController().navigateUp()
        findNavController().safeNavigate(WaitDialogDirections.actionToWaitDialog(null))
        lifecycleScope.launch {
            viewModel.uiModel.track?.id?.let {
                if (result.type == ExportType.GPX) {
                    viewModel.exportTrack(result.name, it)
                } else {
                    viewModel.sendTrack(result.name, it)
                }.collectLatest { sendTrackStatus ->
                    when (sendTrackStatus) {
                        ExportTrackStatus.LOADING -> {
                            requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                            requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                WaitDialogDirections.actionToWaitDialog(
                                    null
                                )
                            )
                        }

                        ExportTrackStatus.ERROR_GARMIN_CONNECT -> {
                            cancel()
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK_SEND_TRACK,
                                    getString(
                                        R.string
                                            .garmin_connect_missing
                                    )
                                )
                            )
                        }

                        ExportTrackStatus.ERROR_SERVICE -> {
                            cancel()
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK_SEND_TRACK,
                                    getString(
                                        R.string
                                            .service_unavailable
                                    )
                                )
                            )
                        }

                        ExportTrackStatus.ERROR_EMPTY_WATCH -> {
                            cancel()
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK_SEND_TRACK,
                                    getString(
                                        R.string
                                            .empty_devices
                                    )
                                )
                            )
                        }

                        ExportTrackStatus.ERROR_MANY_WATCH -> {
                            cancel()
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK_SEND_TRACK,
                                    getString(
                                        R.string
                                            .many_devices
                                    )
                                )
                            )
                        }

                        ExportTrackStatus.ERROR_APP_WATCH_NOT_INSTALLED -> {
                            cancel()
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK_SEND_TRACK,
                                    getString(
                                        R.string
                                            .app_watch_not_installed
                                    )
                                )
                            )
                        }

                        ExportTrackStatus.WAITING_WATCH_APP -> {
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                WaitDialogDirections.actionToWaitDialog(
                                    getString(
                                        R.string
                                            .waiting_watch_app
                                    )
                                )
                            )
                        }

                        ExportTrackStatus.ERROR_TRANSFER -> {
                            cancel()
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK_SEND_TRACK,
                                    getString(R.string.error_transfer_file)
                                )
                            )
                        }

                        ExportTrackStatus.SUCCESS -> {
                            cancel()
                            findNavController().navigateUp()
                            findNavController().safeNavigate(
                                AlertDialogDirections.actionToAlertOk(
                                    TAG_ALERT_OK_SEND_TRACK,
                                    if (result.type == ExportType.GPX) {
                                        getString(
                                            R.string
                                                .success_eport_gpx
                                        )
                                    } else {
                                        getString(
                                            R.string
                                                .success_send_watch
                                        )
                                    }
                                )
                            )
                        }
                    }
                }
            }
        }
    }

    private fun LineChart.getLineDataSet(label: String): LineDataSet? =
        lineData?.dataSets?.find { it.label == label } as? LineDataSet
}
