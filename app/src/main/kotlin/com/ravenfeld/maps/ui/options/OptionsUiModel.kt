package com.ravenfeld.maps.ui.options

data class OptionsUiModel(
    val list: List<Option> = Option.entries
)
