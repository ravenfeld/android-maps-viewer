package com.ravenfeld.maps.ui.download

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ravenfeld.maps.domain.map.MapRepository
import com.ravenfeld.maps.ui.common.RequestEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.maplibre.android.geometry.LatLng
import javax.inject.Inject

@HiltViewModel
class DownloadMapViewModel @Inject constructor(private val mapRepository: MapRepository) :
    ViewModel() {

    private val _events = MutableStateFlow<Event<RequestEvent>?>(null)

    val events = _events.filterNotNull()

    fun maps(area: Array<LatLng>): Flow<DownloadMapUiModel> = mapRepository.getListMapStreamEnable()
        .map { maps ->
            maps.toListMapDownloadUiModel(area)
        }

    fun downloadTiles(name: String, mapsName: List<String>, area: Array<LatLng>, minZoom: Int, maxZoom: Int) {
        viewModelScope.launch {
            if (mapRepository.downloadTiles(name, mapsName, area, minZoom, maxZoom)) {
                _events.value = Event(RequestEvent.Success)
            } else {
                _events.value = Event(RequestEvent.Error)
            }
        }
    }
}
