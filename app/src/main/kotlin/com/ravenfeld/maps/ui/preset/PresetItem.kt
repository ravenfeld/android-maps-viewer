package com.ravenfeld.maps.ui.preset

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Create
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.ravenfeld.maps.R

@Suppress("LongMethod")
@Composable
fun PresetItem(
    preset: PresetUiModel,
    onSelect: (id: Int) -> Unit,
    onRename: (id: Int, name: String) -> Unit,
    onDelete: (id: Int) -> Unit,
    modifier: Modifier = Modifier
) {
    var showActionsDropdown by remember { mutableStateOf(false) }
    var showDeleteDialog by remember { mutableStateOf(false) }
    var showRenameDialog by remember { mutableStateOf(false) }

    val name = preset.name.ifEmpty { stringResource(R.string.presets_default_name) }

    Row(
        modifier = modifier.height(IntrinsicSize.Min),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = name,
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .weight(1f)
        )
        if (preset.name.isNotEmpty()) {
            Box {
                IconButton(
                    onClick = { showActionsDropdown = true },
                    modifier = Modifier,
                ) {
                    Icon(imageVector = Icons.Filled.MoreVert, null)
                }
                PresetDropdownMenu(
                    expanded = showActionsDropdown,
                    onDismissRequest = { showActionsDropdown = false },
                    onRename = { showRenameDialog = true },
                    onDelete = { showDeleteDialog = true },
                )
            }
        }
        Box(
            modifier = Modifier
                .padding(end = 16.dp),
            contentAlignment = Alignment.Center
        ) {
            RadioButton(selected = preset.selected, onClick = { onSelect(preset.id) })
        }
    }

    if (showRenameDialog) {
        TextInputDialog(
            onDismissRequest = { showRenameDialog = false },
            onConfirmed = { onRename(preset.id, it) },
            title = { Text(stringResource(R.string.presets_rename)) },
            text = name,
            textInputLabel = { Text(stringResource(R.string.presets_preset_name)) }
        )
    }
    if (showDeleteDialog) {
        AlertDialog(
            onDismissRequest = { showDeleteDialog = false },
            confirmButton = {
                TextButton(
                    onClick = {
                        onDelete(preset.id)
                        showDeleteDialog = false
                    }
                ) { Text(text = stringResource(R.string.delete_confirmation)) }
            },
            text = {
                Text(
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.primary,
                    text = stringResource(R.string.presets_delete_message, name)
                )
            },
        )
    }
}

@Composable
private fun PresetDropdownMenu(
    expanded: Boolean,
    onDismissRequest: () -> Unit,
    onRename: () -> Unit,
    onDelete: () -> Unit,
    modifier: Modifier = Modifier
) {
    DropdownMenu(
        expanded = expanded,
        onDismissRequest = onDismissRequest,
        modifier = modifier
    ) {
        DropdownMenuItem(
            onClick = {
                onDismissRequest()
                onRename()
            },
            text = {
                Text(stringResource(R.string.presets_rename))
            },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Filled.Create,
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.primary,
                )
            }
        )

        DropdownMenuItem(
            onClick = {
                onDismissRequest()
                onDelete()
            },
            text = {
                Text(stringResource(R.string.presets_delete))
            },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Filled.Delete,
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.primary,
                )
            }
        )
    }
}
