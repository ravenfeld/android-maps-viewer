package com.ravenfeld.maps.ui.edition.poi

import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavBackStackEntry
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.R
import com.ravenfeld.maps.data.utils.safeLet
import com.ravenfeld.maps.databinding.DialogEditPoiBinding
import com.ravenfeld.maps.domain.geo.PoiSymbol
import com.ravenfeld.maps.ui.common.getOrThrow
import com.ravenfeld.maps.ui.common.safeNavigate
import com.ravenfeld.maps.ui.edition.poi.type.PoiSymbolsDialog

class EditPoiDialog : AppCompatDialogFragment(), EditPoiPresenter {

    private var _binding: DialogEditPoiBinding? = null
    private val binding get() = _binding.getOrThrow()
    private var navBackStackEntry: NavBackStackEntry? = null

    private val observerOnCreate = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_CREATE) {
            navBackStackEntry?.savedStateHandle?.getLiveData<PoiSymbol>(PoiSymbolsDialog.STATE_POI_SYMBOL_SELECTED)
                ?.observe(viewLifecycleOwner) { result ->
                    binding.uiModel?.symbol = result
                    findNavController().navigateUp()
                }
        }
    }
    private val observerOnDestroy = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_DESTROY) {
            navBackStackEntry?.savedStateHandle?.remove<PoiSymbol>(PoiSymbolsDialog.STATE_POI_SYMBOL_SELECTED)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogEditPoiBinding.inflate(inflater, container, false)
        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navBackStackEntry = findNavController().getBackStackEntry(R.id.editPoiDialog)
        navBackStackEntry?.lifecycle?.addObserver(observerOnCreate)
        viewLifecycleOwner.lifecycle.addObserver(observerOnDestroy)

        binding.presenter = this
        binding.uiModel = EditPoiUiModel(
            mode = EditPoiDialogArgs.fromBundle(requireArguments()).mode,
            name = EditPoiDialogArgs.fromBundle(requireArguments()).name,
            symbol = PoiSymbol.valueOf(EditPoiDialogArgs.fromBundle(requireArguments()).symbol),
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            @Suppress("deprecation")
            val display = requireActivity().windowManager.defaultDisplay
            val size = Point()
            @Suppress("deprecation")
            display.getSize(size)
            val params = it.attributes
            val widthMax = resources.getDimensionPixelSize(R.dimen.max_width)

            @Suppress("MagicNumber")
            val width = (size.x * 0.9).toInt()
            params.width = if (width < widthMax) {
                width
            } else {
                widthMax
            }
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            it.attributes = params as android.view.WindowManager.LayoutParams
        }
    }

    override fun onTypeClick() {
        binding.uiModel?.symbol?.name?.let {
            findNavController().safeNavigate(
                EditPoiDialogDirections.actionToPoiSymbolsFragment(
                    it
                )
            )
        }
    }

    override fun onSave() {
        binding.uiModel?.let {
            safeLet(it.name, it.symbol) { name, symbol ->
                findNavController().previousBackStackEntry?.savedStateHandle?.set(
                    STATE_EDIT_POI_SELECTED,
                    EditPoiResult.Save(
                        point = EditPoiDialogArgs.fromBundle(requireArguments()).point,
                        name = name,
                        symbol = symbol
                    )
                )
            }
        }
    }

    override fun onRemove() {
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            STATE_EDIT_POI_SELECTED,
            EditPoiResult.Remove(
                point = EditPoiDialogArgs.fromBundle(requireArguments()).point,
            )
        )
    }

    companion object {
        const val STATE_EDIT_POI_SELECTED = "edit_poi_selected"
    }
}
