package com.ravenfeld.maps.ui.common.alert

interface AlertPresenter {
    fun onButtonRightClick()
    fun onButtonLeftClick()
}
