package com.ravenfeld.maps.ui.preset

data class PresetUiModel(
    val id: Int,
    val name: String,
    val selected: Boolean
)
