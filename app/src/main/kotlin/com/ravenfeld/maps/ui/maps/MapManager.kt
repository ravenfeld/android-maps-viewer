package com.ravenfeld.maps.ui.maps

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PointF
import android.location.Location
import android.os.Bundle
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.utils.Utils
import com.google.gson.JsonObject
import com.ravenfeld.maps.R
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Poi
import com.ravenfeld.maps.domain.geo.PoiSymbol
import com.ravenfeld.maps.domain.geo.TrackPoint
import com.ravenfeld.maps.domain.geo.Way
import com.ravenfeld.maps.domain.geo.distanceBetween
import com.ravenfeld.maps.domain.geo.nearestTrackPointOnLine
import com.ravenfeld.maps.domain.map.DisplayInfo
import com.ravenfeld.maps.domain.map.Map
import com.ravenfeld.maps.domain.map.MapStorage
import com.ravenfeld.maps.domain.place.model.PlaceOnMap
import com.ravenfeld.maps.extension.toTouchRectF
import com.ravenfeld.maps.maplibre.GoogleLocationEngineImpl
import com.ravenfeld.maps.ui.place.PlaceOnMapUiModel
import org.maplibre.android.camera.CameraPosition
import org.maplibre.android.camera.CameraUpdateFactory
import org.maplibre.android.geometry.LatLng
import org.maplibre.android.geometry.LatLngBounds
import org.maplibre.android.gestures.Utils.dpToPx
import org.maplibre.android.location.LocationComponentActivationOptions
import org.maplibre.android.location.LocationComponentOptions
import org.maplibre.android.location.OnCameraTrackingChangedListener
import org.maplibre.android.location.OnLocationClickListener
import org.maplibre.android.location.engine.LocationEngineProxy
import org.maplibre.android.location.engine.LocationEngineRequest
import org.maplibre.android.location.modes.CameraMode
import org.maplibre.android.location.modes.RenderMode
import org.maplibre.android.maps.MapLibreMap
import org.maplibre.android.maps.MapLibreMap.OnCameraMoveListener
import org.maplibre.android.maps.MapView
import org.maplibre.android.maps.OnMapReadyCallback
import org.maplibre.android.maps.Style
import org.maplibre.android.plugins.annotation.OnSymbolDragListener
import org.maplibre.android.plugins.annotation.Symbol
import org.maplibre.android.plugins.annotation.SymbolManager
import org.maplibre.android.plugins.annotation.SymbolOptions
import org.maplibre.android.plugins.scalebar.ScaleBarOptions
import org.maplibre.android.plugins.scalebar.ScaleBarPlugin
import org.maplibre.android.style.expressions.Expression
import org.maplibre.android.style.expressions.Expression.all
import org.maplibre.android.style.expressions.Expression.eq
import org.maplibre.android.style.expressions.Expression.get
import org.maplibre.android.style.expressions.Expression.literal
import org.maplibre.android.style.expressions.Expression.switchCase
import org.maplibre.android.style.layers.LineLayer
import org.maplibre.android.style.layers.Property
import org.maplibre.android.style.layers.PropertyFactory
import org.maplibre.android.style.layers.PropertyValue
import org.maplibre.android.style.layers.SymbolLayer
import org.maplibre.android.style.sources.GeoJsonSource
import org.maplibre.geojson.Feature
import org.maplibre.geojson.FeatureCollection

private const val OPEN_MAPS_PREFERENCES = "OPEN_MAPS_PREFERENCES"
private const val ZOOM = "ZOOM"
private const val LATITUDE = "LATITUDE"
private const val LONGITUDE = "LONGITUDE"
private const val CAMERA_MODE = "CAMERA_MODE"
private const val UPDATE_INTERVAL_LOCATION = 1_000L
private const val ZOOM_LOCATION = 13.0
private const val ZOOM_MAX_TRACK = 17.0
private const val ANIMATION_DURATION_MS = 750L
private const val PADDING_BOUNDS: Float = 75f
private const val ARROW_ID = "ARROW_ID"
private const val MARKER_ICON = "MARKER_ICON"
private const val TOUCH_ICON = "TOUCH_ICON"
private const val SOURCE_MARKER_ID = "SOURCE_MARKER_ID"
private const val LAYER_MARKER_ID = "LAYER_MARKER_ID"
private const val LAYER_MAPLIBRE_POINTS = "org.maplibre.annotations.points"
internal const val POI_ON_TRACK_IMAGE = "poi_on_track_image_"
internal const val POI_ON_MAP_IMAGE = "poi_on_map_image_"
private const val SOURCE_POI_ON_MAP_ID = "SOURCE_POI_ON_MAP_ID"
private const val LAYER_POI_ON_MAP_ID = "LAYER_POI_ON_MAP_ID"

private const val SOURCE_HIGHLIGHT_TRACK_ID = "SOURCE_HIGHLIGHT_TRACK_ID"
private const val LAYER_HIGHLIGHT_TRACK_ID = "LAYER_HIGHLIGHT_TRACK_ID"

@Suppress("LongParameterList", "ComplexMethod", "TooManyFunctions", "LargeClass")
class MapManager(
    private val context: Context,
    private val mapView: MapView,
    private val compassView: ImageView,
    private val uiModel: MapUiModel,
    private val onMapReady: () -> Unit,
    private val onMapLoaded: () -> Unit,
    private val onLoadRoute: () -> Unit,
    private val onPoint: (point: GeoPoint) -> Unit,
    private val onMovePoint: (oldGeoPoint: GeoPoint, newPoint: GeoPoint, zoom: Double) -> Unit,
    private val onAddPoint: (point: GeoPoint) -> Unit,
    private val onSelectPoi: (poi: Poi) -> Unit,
    private val onGetTrack: (pointClicked: GeoPoint, zoom: Double) -> Unit,
    private val onLocationInfo: (cameraPosition: GeoPoint, userLocation: GeoPoint?) -> Unit,
    private val onTrackSelected: (TrackUiModel, Boolean) -> Unit,
    private val onTrackPointUnselected: () -> Unit,
    private val onCameraMoveEnded: () -> Unit,
) : OnMapReadyCallback, MapLibreMap.OnMapClickListener, MapLibreMap.OnMapLongClickListener, OnSymbolDragListener {

    private var mapLibreMap: MapLibreMap? = null
    private var isMapLoad = false
    private var symbolManager: SymbolManager? = null
    private var lastDragTimestamp: Long = 0
    private var initDragTimestamp: Long = 0
    private val placeOnMapAnnotation = PlaceOnMapAnnotation(mapView)

    private val cameraMoveListener = OnCameraMoveListener {
        mapLibreMap?.let {
            if (uiModel.cameraMode != CameraMode.TRACKING_COMPASS) {
                @Suppress("MagicNumber")
                compassView.rotation = 360 - it.cameraPosition.bearing.toFloat()
            }

            if (!uiModel.trackingGPS && uiModel.displayInfo == DisplayInfo.LOCALISATION) {
                uiModel.displayInfo = DisplayInfo.NONE
                uiModel.altitude = null
            }
        }
    }

    private val cameraIdleListener = MapLibreMap.OnCameraIdleListener {
        onCameraMoveEnded()
    }

    private val locationClickListener = OnLocationClickListener {
        setTrackingMode()
        getUserPosition()
    }

    private val cameraTrackingChangedListener = object : OnCameraTrackingChangedListener {
        override fun onCameraTrackingDismissed() {
            mapLibreMap?.let {
                compassView.rotation = 360 - it.cameraPosition.bearing.toFloat()
            }
            uiModel.cameraMode = CameraMode.NONE
            if (uiModel.displayInfo == DisplayInfo.LOCALISATION) {
                uiModel.displayInfo = DisplayInfo.NONE
                uiModel.altitude = null
            }
        }

        override fun onCameraTrackingChanged(currentMode: Int) {
            uiModel.cameraMode = currentMode
        }
    }

    private val sharePreferences by lazy {
        context.getSharedPreferences(OPEN_MAPS_PREFERENCES, Context.MODE_PRIVATE)
    }

    fun onCreate(savedInstanceState: Bundle?) {
        mapView.onCreate(savedInstanceState)
        isMapLoad = false
        mapView.getMapAsync(this)
    }

    fun onStart() {
        mapView.onStart()
    }

    fun onResume() {
        mapView.onResume()
    }

    fun onPause() {
        mapView.onPause()
    }

    fun onStop() {
        mapView.onStop()
        mapLibreMap?.let {
            val edit = sharePreferences.edit()
            edit.putFloat(ZOOM, it.cameraPosition.zoom.toFloat())
            it.cameraPosition.target?.let { latLng ->
                edit.putFloat(LATITUDE, latLng.latitude.toFloat())
                edit.putFloat(LONGITUDE, latLng.longitude.toFloat())
            }

            if (it.locationComponent.isLocationComponentActivated) {
                edit.putInt(CAMERA_MODE, it.locationComponent.cameraMode)
            }
            edit.apply()
        }
    }

    fun onSaveInstanceState(outState: Bundle) {
        mapView.onSaveInstanceState(outState)
    }

    fun onLowMemory() {
        mapView.onLowMemory()
    }

    fun onDestroyView() {
        mapLibreMap?.removeOnMapClickListener(this)
        mapLibreMap?.removeOnMapLongClickListener(this)
        mapLibreMap?.locationComponent?.removeOnCameraTrackingChangedListener(
            cameraTrackingChangedListener
        )
        mapLibreMap?.locationComponent?.removeOnLocationClickListener(locationClickListener)
        mapLibreMap?.removeOnCameraMoveListener(cameraMoveListener)
        mapLibreMap?.removeOnCameraIdleListener(cameraIdleListener)
        symbolManager?.removeDragListener(this)
        symbolManager?.onDestroy()
        mapView.onDestroy()
        mapLibreMap = null
    }

    override fun onMapReady(mapLibreMap: MapLibreMap) {
        this.mapLibreMap = mapLibreMap
        this.mapLibreMap?.addOnMapClickListener(this)
        this.mapLibreMap?.addOnMapLongClickListener(this)
        onMapReady()
    }

    @Suppress("NestedBlockDepth", "LongMethod")
    override fun onMapClick(point: LatLng): Boolean = if (!uiModel.displayAreaDownload) {
        mapLibreMap?.let { maplibre ->
            val pointf = maplibre.projection.toScreenLocation(point)

            val click = maplibre.touchPoi(pointf) || maplibre.touchPoiOnMap(pointf) || maplibre.touchTrack(pointf)

            if (!click && !placeOnMapAnnotation.remove(maplibre)) {
                if (uiModel.isEditMode) {
                    if (uiModel.track == null) {
                        onPoint(
                            point.toGeoPoint()
                        )
                    } else {
                        onGetTrack(
                            point.toGeoPoint(),
                            maplibre.cameraPosition.zoom
                        )
                    }
                    return true
                } else if (uiModel.displayInfo == DisplayInfo.NONE || uiModel.displayInfo == DisplayInfo.LOCALISATION) {
                    mapLibreMap?.animateCamera(
                        CameraUpdateFactory.newCameraPosition(
                            CameraPosition.Builder().target(
                                LatLng(
                                    point.latitude,
                                    point.longitude
                                )
                            ).build()
                        ),
                        ANIMATION_DURATION_MS.toInt(),
                        object : MapLibreMap.CancelableCallback {
                            override fun onCancel() {
                                // No impl
                            }

                            override fun onFinish() {
                                if (mapLibreMap?.locationComponent?.isLocationComponentActivated == true) {
                                    onLocationInfo(
                                        point.toGeoPoint(),
                                        mapLibreMap?.locationComponent?.lastKnownLocation?.toGeoPoint()
                                    )
                                }
                            }
                        }
                    )
                    false
                } else {
                    false
                }
            } else {
                true
            }
        } ?: false
    } else {
        false
    }

    private fun MapLibreMap.touchPoi(pointf: PointF): Boolean {
        var clickPoi = false

        if (uiModel.isEditMode) {
            val rectF = pointf.toTouchRectF()

            val keys = uiModel.listTrackUiModel.map { it.layerPoiName }
            var i = keys.size - 1

            while (!clickPoi && i >= 0) {
                val key = keys[i]
                val featureList: List<Feature> = queryRenderedFeatures(rectF, key)
                val poi = featureList.firstOrNull()?.properties()?.toPoi()
                poi?.let {
                    clickPoi = true
                    placeOnMapAnnotation.remove(this)
                    onSelectPoi(it)
                }
                i--
            }
        }
        return clickPoi
    }

    private fun MapLibreMap.touchPoiOnMap(pointf: PointF): Boolean {
        val features = queryRenderedFeatures(
            pointf.toTouchRectF(),
            LAYER_POI_ON_MAP_ID,
        )
        return features.firstOrNull()?.let { feature ->
            feature.properties()?.toPlaceOnMap()?.let { placeOnMap ->
                placeOnMapAnnotation.create(this, placeOnMap)
                true
            } ?: false
        } ?: false
    }

    @Suppress("NestedBlockDepth")
    private fun MapLibreMap.touchTrack(pointf: PointF): Boolean {
        @Suppress("MagicNumber")
        val rectF = pointf.toTouchRectF()
        var clickTrack = false

        val keys = uiModel.listTrackUiModel.map { it.layerLineName } +
            uiModel.listTrackUiModel.map { it.layerCircleName }
        var i = keys.size - 1

        while (!clickTrack && i >= 0) {
            val key = keys[i]
            val featureList: List<Feature> = queryRenderedFeatures(rectF, key)
            if (featureList.isNotEmpty()) {
                clickTrack = true
                uiModel.listTrackUiModel.find { it.layerLineName == key || it.layerCircleName == key }
                    ?.let { track ->
                        if (uiModel.track?.id != track.id) {
                            uiModel.isEditMode = false
                            removeTouchMarker()
                            placeOnMapAnnotation.remove(this)
                            onTrackSelected(track, true)
                        }
                    }
            }
            i--
        }
        return clickTrack
    }

    override fun onMapLongClick(p0: LatLng): Boolean {
        // Workaround for https://github.com/mapbox/mapbox-plugins-android/issues/1162
        @Suppress("MagicNumber")
        if (System.currentTimeMillis() - lastDragTimestamp < 500L) {
            return true
        }

        return if (uiModel.editModeAndTrackWithSegment) {
            onAddPoint(p0.toGeoPoint())
            true
        } else {
            false
        }
    }

    fun setStyle(newOrderMap: Boolean, listMaps: List<Map>) {
        if (mapLibreMap?.style == null || newOrderMap) {
            newStyle(listMaps)
        } else {
            updateStyle(listMaps)
        }
    }

    private fun getLatLngSaved(): LatLng? =
        if (sharePreferences.contains(LATITUDE) && sharePreferences.contains(LONGITUDE)) {
            LatLng(
                sharePreferences.getFloat(LATITUDE, 0.0f).toDouble(),
                sharePreferences.getFloat(LONGITUDE, 0.0f).toDouble()
            )
        } else {
            null
        }

    @Suppress("MagicNumber", "LongMethod")
    private fun newStyle(maps: List<Map>) {
        removeTouchMarker()
        mapLibreMap?.let { maplibre ->
            maplibre.style?.removeLayer(LAYER_HIGHLIGHT_TRACK_ID)

            val builder = Style.Builder()
            if (maps.any { it.storage == MapStorage.MBTILES_VECTOR }) {
                builder.fromUri("asset://style.json")
            }
            builder.withImage(
                ARROW_ID,
                getBitmapFromVectorDrawable(context, R.drawable.ic_arrow_direction)
            )
            builder.withImage(
                MARKER_ICON,
                getBitmapFromVectorDrawable(context, R.drawable.ic_location)
            )
            builder.withImage(TOUCH_ICON, getBitmapFromVectorDrawable(context, R.drawable.ic_touch))

            maps.forEach {
                if (it.storage == MapStorage.MBTILES_VECTOR) {
                    builder.withSource(it.toVectorSource())
                } else {
                    builder.withSource(it.toRasterSource())
                    builder.withLayer(it.toRasterLayer())
                }
            }
            PoiSymbol.entries.forEach {
                builder.withImage(
                    POI_ON_TRACK_IMAGE + it.name,
                    getBitmapFromVectorDrawable(context, it.iconMapId)
                )
            }
            PlaceOnMapUiModel.entries.forEach {
                builder.withImage(
                    POI_ON_MAP_IMAGE + it.name,
                    getBitmapFromVectorDrawable(context, it.iconMapId)
                )
            }

            builder.withSource(initPoiOnMapSource())
            builder.withLayer(initPoiOnMapLayer())
            builder.withSource(GeoJsonSource(SOURCE_HIGHLIGHT_TRACK_ID))
            builder.withSource(GeoJsonSource(SOURCE_MARKER_ID))
            builder.withLayerAbove(
                SymbolLayer(LAYER_MARKER_ID, SOURCE_MARKER_ID)
                    .withProperties(
                        PropertyFactory.iconImage(MARKER_ICON),
                        PropertyFactory.iconAllowOverlap(true),
                        PropertyFactory.iconAnchor(Property.ICON_ANCHOR_BOTTOM),
                    ),
                LAYER_MAPLIBRE_POINTS
            )
            maplibre.setStyle(builder) {
                maplibre.uiSettings.isAttributionEnabled = false
                maplibre.uiSettings.isLogoEnabled = true
                maplibre.uiSettings.isRotateGesturesEnabled = true
                maplibre.uiSettings.isCompassEnabled = false
                maplibre.uiSettings.isTiltGesturesEnabled = false
                maplibre.addOnCameraMoveListener(cameraMoveListener)
                maplibre.addOnCameraIdleListener(cameraIdleListener)

                val latLngSaved = getLatLngSaved()
                if (!isMapLoad && sharePreferences.contains(ZOOM) && latLngSaved != null) {
                    maplibre.cameraPosition = CameraPosition.Builder()
                        .target(latLngSaved)
                        .zoom(sharePreferences.getFloat(ZOOM, 0.0f).toDouble())
                        .build()
                }
                onLoadRoute()
                activeLocation()
                val scaleBarPlugin = ScaleBarPlugin(mapView, maplibre)
                val scaleBarOptions = ScaleBarOptions(context)
                @Suppress("MagicNumber")
                scaleBarOptions.setTextColor(android.R.color.black)
                    .setTextSize(Utils.convertDpToPixel(10f))
                    .setBarHeight(5f)
                    .setBorderWidth(2f)
                    .setRefreshInterval(15)
                    .setMarginLeft(Utils.convertDpToPixel(8f))
                    .setMarginTop(Utils.convertDpToPixel(8f))
                    .setTextBarMargin(15f)
                    .setShowTextBorder(true)
                    .setMaxWidthRatio(Utils.convertDpToPixel(120f) / mapView.width)
                    .setTextBorderWidth(5f)

                scaleBarPlugin.create(scaleBarOptions)

                symbolManager = SymbolManager(mapView, maplibre, it)
                symbolManager?.setIconAllowOverlap(false)
                symbolManager?.addDragListener(this)

                isMapLoad = true
                onMapLoaded()
            }
        }
    }

    private fun updateStyle(maps: List<Map>) {
        mapLibreMap?.style?.let { style ->
            maps.forEach {
                if (it.storage != MapStorage.MBTILES_VECTOR) {
                    style.getLayer(it.name)
                        ?.setProperties(PropertyValue("raster-opacity", it.alpha))
                    style.getLayer(it.name)?.setProperties(
                        PropertyValue(
                            "visibility",
                            if (it.visible) {
                                "visible"
                            } else {
                                "none"
                            }
                        )
                    )
                }
            }
        }
    }

    fun loadTracks(listTrack: List<TrackUiModel>) {
        removeTouchMarker()
        mapLibreMap?.style?.let { style ->
            uiModel.listTrackUiModel.forEach {
                style.removeLayer(it.layerLineName)
                style.removeLayer(it.layerArrowName)
                style.removeLayer(it.layerCircleName)
                style.removeLayer(it.layerPoiName)

                style.removeSource(it.sourceLineName)
                style.removeSource(it.sourceCircleName)
                style.removeSource(it.sourcePoiName)
            }
            uiModel.listTrackUiModel = listTrack

            listTrack.filter { it.visible }.forEach { track ->
                if (track.trackPoints.size == 1) {
                    style.addSource(track.toCircleSource())
                } else {
                    style.addSource(track.toLineSource())
                }
                style.addSource(track.toPoiOnTrackSource())

                style.addLayerBelow(track.toLineLayer(track.color), LAYER_MARKER_ID)
                style.addLayerBelow(track.toCircleLayer(track.color), LAYER_MARKER_ID)
                style.addLayerBelow(track.toArrowLayer(ARROW_ID), LAYER_MARKER_ID)
                style.addLayerBelow(track.toPoiOnTrackLayer(), LAYER_MARKER_ID)
            }

            if (listTrack.isNotEmpty()) {
                if (uiModel.hasLoadTrack) {
                    trackPointUnselected()
                    listTrack.newTrack()
                } else if (uiModel.displayInfo == DisplayInfo.TRACK) {
                    listTrack.lastTrackSelected()
                }
            }
        }
    }

    private fun List<TrackUiModel>.newTrack() {
        last().let { trackLoad ->
            if (trackLoad.trackPoints.isNotEmpty()) {
                cameraMove(trackLoad.locationNorthEast, trackLoad.locationSouthWest)
            }
            uiModel.hasLoadTrack = false
            onTrackSelected(trackLoad, true)
        }
    }

    private fun List<TrackUiModel>.lastTrackSelected() {
        uiModel.track?.let { track ->
            val trackUiModel = find { it.id == track.id } ?: last()

            checkHighlightSelected(trackUiModel)

            onTrackSelected(trackUiModel, uiModel.displayInfo == DisplayInfo.TRACK)
            if (uiModel.isEditMode) {
                addTouchMarker(trackUiModel)
            }
        } ?: run {
            trackPointUnselected()

            val trackUiModel = last()
            onTrackSelected(last(), uiModel.displayInfo == DisplayInfo.TRACK)
            addTouchMarker(trackUiModel)
        }
    }

    private fun checkHighlightSelected(trackUiModel: TrackUiModel) {
        val markerGeoJsonSource = mapLibreMap?.style?.getSourceAs<GeoJsonSource>(
            SOURCE_MARKER_ID
        )
        val marker = markerGeoJsonSource?.querySourceFeatures(null)
            ?.firstOrNull()
            ?.properties()?.toGeoPoint()

        marker?.let { point ->

            val trackPointOnLine = trackUiModel.trackPoints.nearestTrackPointOnLine(point)

            val distance = distanceBetween(trackPointOnLine.geoPoint, point)
            if (
                distance > 1
            ) {
                trackPointUnselected()
            } else {
                trackPointOnLine.way?.let {
                    mapLibreMap?.style?.redrawHighlight(trackPointOnLine.way, trackUiModel)
                }
            }
        }
    }

    @Suppress("MagicNumber")
    fun cameraMove(
        locationNorthEast: LatLng,
        locationSouthWest: LatLng,
        zoomMax: Double = ZOOM_MAX_TRACK,
        paddingDp: Float = PADDING_BOUNDS
    ) {
        val latLngBounds = LatLngBounds.Builder().include(locationNorthEast)
            .include(locationSouthWest).build()
        mapLibreMap?.let {
            val cameraPosition = CameraUpdateFactory.newLatLngBounds(
                latLngBounds,
                dpToPx(paddingDp).toInt()
            ).getCameraPosition(it)
            @Suppress("UnnecessaryParentheses")
            if ((cameraPosition?.zoom ?: 0.0) > zoomMax) {
                it.easeCamera(CameraUpdateFactory.newLatLngZoom(locationNorthEast, zoomMax))
            } else {
                it.easeCamera(
                    CameraUpdateFactory.newLatLngBounds(
                        latLngBounds,
                        dpToPx(paddingDp).toInt()
                    ),
                    ANIMATION_DURATION_MS.toInt()
                )
            }
        }
    }

    @Suppress("MagicNumber")
    fun cameraMove(
        location: LatLng
    ) {
        mapLibreMap?.easeCamera(
            CameraUpdateFactory.newLatLngZoom(location, 15.0),
            ANIMATION_DURATION_MS.toInt()
        )
    }

    fun setAlpha(name: String, alpha: Float) {
        @Suppress("MagicNumber")
        mapLibreMap?.style?.getLayer(name)
            ?.setProperties(PropertyValue("raster-opacity", (alpha / 100.0).toFloat()))
    }

    @Suppress("NestedBlockDepth")
    fun activeLocation() {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mapLibreMap?.let {
                it.style?.let { style ->
                    val locationComponent = it.locationComponent
                    val locationComponentOptions = LocationComponentOptions.builder(context)
                        .pulseEnabled(true)
                        .build()

                    val locationEngine = LocationEngineProxy(GoogleLocationEngineImpl(context))
                    locationComponent.activateLocationComponent(
                        LocationComponentActivationOptions.builder(context, style)
                            .locationComponentOptions(locationComponentOptions)
                            .locationEngine(locationEngine)
                            .locationEngineRequest(
                                LocationEngineRequest.Builder(UPDATE_INTERVAL_LOCATION)
                                    .setFastestInterval(UPDATE_INTERVAL_LOCATION)
                                    .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                                    .build()
                            )
                            .build()
                    )

                    locationComponent.addOnLocationClickListener(locationClickListener)
                    locationComponent.addOnCameraTrackingChangedListener(
                        cameraTrackingChangedListener
                    )
                    locationComponent.isLocationComponentEnabled = true
                    locationComponent.renderMode = RenderMode.COMPASS
                    if (!sharePreferences.contains(ZOOM)) {
                        it.cameraPosition = CameraPosition.Builder().zoom(ZOOM_LOCATION).build()
                    }
                    locationComponent.cameraMode = sharePreferences.getInt(
                        CAMERA_MODE,
                        CameraMode.TRACKING
                    )
                }
            }
        }
    }

    fun setTrackingMode() {
        mapLibreMap?.let {
            if (it.locationComponent.isLocationComponentActivated) {
                if (it.cameraPosition.zoom < ZOOM_LOCATION) {
                    it.locationComponent.setCameraMode(
                        CameraMode.TRACKING,
                        ANIMATION_DURATION_MS,
                        ZOOM_LOCATION,
                        null,
                        null,
                        null
                    )
                } else {
                    it.locationComponent.cameraMode = CameraMode.TRACKING
                }
                getUserPosition()
            }
        }
    }

    fun setCameraMode() {
        mapLibreMap?.let { maplibre ->
            val isTracking = if (maplibre.locationComponent.isLocationComponentActivated) {
                maplibre.locationComponent.cameraMode >= CameraMode.TRACKING
            } else {
                false
            }
            if (isTracking && maplibre.cameraPosition.bearing == 0.0) {
                maplibre.locationComponent.cameraMode = CameraMode.TRACKING_COMPASS
            } else {
                maplibre.animateCamera(
                    CameraUpdateFactory.newCameraPosition(
                        CameraPosition.Builder()
                            .bearing(0.0)
                            .build()
                    ),
                    (ANIMATION_DURATION_MS / 2).toInt(),
                    object : MapLibreMap.CancelableCallback {
                        override fun onCancel() {
                            // No impl
                        }

                        override fun onFinish() {
                            if (isTracking) {
                                maplibre.locationComponent.cameraMode = CameraMode.TRACKING
                            }
                        }
                    }
                )
            }
        }
    }

    fun trackPointSelected(trackPointSelected: TrackPoint, track: TrackUiModel) {
        mapLibreMap?.let {
            it.style?.let { style ->
                trackPointSelected.way?.let { way ->
                    style.drawHighlight(way, track)
                }
                val markerGeoJsonSource = style.getSourceAs<GeoJsonSource>(SOURCE_MARKER_ID)
                markerGeoJsonSource?.let {
                    markerGeoJsonSource.setGeoJson(
                        trackPointSelected.toFeature()
                    )
                }
                mapLibreMap?.animateCamera(
                    CameraUpdateFactory.newCameraPosition(
                        CameraPosition.Builder().target(
                            LatLng(
                                trackPointSelected.geoPoint.latitude,
                                trackPointSelected.geoPoint.longitude,
                                trackPointSelected.geoPoint.altitude ?: 0.0
                            )
                        ).build()
                    ),
                    (ANIMATION_DURATION_MS / 2).toInt()
                )
            }
        }
    }

    private fun Style.drawHighlight(way: Way, track: TrackUiModel) {
        val highlightGeoJsonSource = getSourceAs<GeoJsonSource>(
            SOURCE_HIGHLIGHT_TRACK_ID
        )
        val oldWay = highlightGeoJsonSource?.querySourceFeatures(null)
            ?.firstOrNull()
            ?.properties()
            ?.toWay()

        if (oldWay?.startDistance != way.startDistance || oldWay.endDistance != way.endDistance) {
            highlightGeoJsonSource?.let {
                highlightGeoJsonSource.setGeoJson(
                    way.toFeature(trackPoints = track.trackPoints)
                )
            }
            if (getLayer(LAYER_HIGHLIGHT_TRACK_ID) == null) {
                @Suppress("MagicNumber")
                addLayerBelow(
                    LineLayer(
                        LAYER_HIGHLIGHT_TRACK_ID,
                        SOURCE_HIGHLIGHT_TRACK_ID
                    ).withProperties(
                        PropertyFactory.lineCap(Property.LINE_CAP_ROUND),
                        PropertyFactory.lineJoin(Property.LINE_JOIN_ROUND),
                        PropertyFactory.lineWidth(4f),
                        PropertyFactory.lineColor("#ffffff"),
                    ),
                    track.layerArrowName
                )
            }
        }
    }

    private fun Style.redrawHighlight(way: Way, track: TrackUiModel) {
        removeLayer(LAYER_HIGHLIGHT_TRACK_ID)
        drawHighlight(way = way, track = track)
    }

    fun trackPointUnselected() {
        mapLibreMap?.let {
            it.style?.let { style ->
                val markerGeoJsonSource = style.getSourceAs<GeoJsonSource>(SOURCE_MARKER_ID)
                markerGeoJsonSource?.let {
                    markerGeoJsonSource.setGeoJson(FeatureCollection.fromFeatures(ArrayList()))
                }

                style.removeLayer(LAYER_HIGHLIGHT_TRACK_ID)
            }
        }
        onTrackPointUnselected()
    }

    fun getProjectionPoints(points: Array<PointF>): Array<LatLng>? {
        return mapLibreMap?.let {
            @Suppress("MagicNumber")
            val area: Array<LatLng> = Array(4) { LatLng() }
            points.forEachIndexed { index, pointF ->
                area[index] = it.projection.fromScreenLocation(pointF)
            }
            area
        } ?: run {
            null
        }
    }

    private fun getBitmapFromVectorDrawable(context: Context, drawableId: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context, drawableId)
        val bitmap = Bitmap.createBitmap(
            drawable!!.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    private fun LatLng.toGeoPoint() =
        GeoPoint(
            latitude = latitude,
            longitude = longitude,
        )

    private fun Location.toGeoPoint(): GeoPoint =
        GeoPoint(
            latitude = latitude,
            longitude = longitude,
        )

    private fun getUserPosition() {
        if (uiModel.displayInfo == DisplayInfo.NONE || uiModel.displayInfo == DisplayInfo.LOCALISATION) {
            mapLibreMap?.cameraPosition?.target?.let { position ->
                onLocationInfo(
                    position.toGeoPoint(),
                    mapLibreMap?.locationComponent?.lastKnownLocation?.toGeoPoint()
                )
            }
        }
    }

    fun getCameraPosition() = mapLibreMap?.cameraPosition

    fun onEditModeChanged() {
        if (uiModel.isEditMode) {
            uiModel.track?.let {
                addTouchMarker(trackUiModel = it)
            }
        } else {
            removeTouchMarker()
        }
    }

    fun getCameraBounds() = mapLibreMap?.projection?.visibleRegion?.latLngBounds

    private fun addTouchMarker(trackUiModel: TrackUiModel) {
        trackUiModel.waypoints.forEach { waypoint ->
            symbolManager?.create(
                SymbolOptions()
                    .withLatLng(LatLng(waypoint.latitude, waypoint.longitude))
                    .withIconImage(TOUCH_ICON)
                    .withIconSize(1.0f)
                    .withDraggable(true)
                    .withData(
                        JsonObject().apply {
                            addProperty(LATITUDE, waypoint.latitude)
                            addProperty(LONGITUDE, waypoint.longitude)
                        }
                    )
            )
        }
    }

    private fun removeTouchMarker() {
        symbolManager?.deleteAll()
    }

    override fun onAnnotationDragStarted(annotation: Symbol?) {
        lastDragTimestamp = System.currentTimeMillis()
        initDragTimestamp = System.currentTimeMillis()
    }

    override fun onAnnotationDrag(annotation: Symbol?) {
        lastDragTimestamp = System.currentTimeMillis()
    }

    override fun onAnnotationDragFinished(annotation: Symbol?) {
        lastDragTimestamp = System.currentTimeMillis()
        // Move the map and the symbol moves very quickly
        @Suppress("MagicNumber")
        annotation?.data?.asJsonObject?.let { data ->
            if (System.currentTimeMillis() - initDragTimestamp >= 100L) {
                onMovePoint(
                    GeoPoint(
                        latitude = data.get(LATITUDE).asDouble,
                        longitude = data.get(LONGITUDE).asDouble
                    ),
                    GeoPoint(
                        latitude = annotation.latLng.latitude,
                        longitude = annotation.latLng.longitude
                    ),
                    mapLibreMap?.cameraPosition?.zoom ?: 0.0
                )
            } else {
                annotation.latLng = LatLng(
                    latitude = data.get(LATITUDE).asDouble,
                    longitude = data.get(LONGITUDE).asDouble
                )
                symbolManager?.update(annotation)
            }
        }
    }

    fun updatePlaceOnMap(places: List<PlaceOnMap>) {
        mapLibreMap?.let { mapLibreMap ->
            if (places.none { placeOnMapAnnotation.isDisplay(it.center) }) {
                placeOnMapAnnotation.remove(mapLibreMap)
            }
            mapLibreMap.style?.let { style ->
                val geoJsonSource = style.getSourceAs<GeoJsonSource>(SOURCE_POI_ON_MAP_ID)
                geoJsonSource?.let {
                    geoJsonSource.setGeoJson(FeatureCollection.fromFeatures(places.map { it.toFeature() }))
                }
            }
        }
    }

    private fun initPoiOnMapSource() =
        GeoJsonSource(
            SOURCE_POI_ON_MAP_ID,
            FeatureCollection.fromFeatures(
                emptyList()
            )
        )

    @Suppress("MagicNumber")
    private fun initPoiOnMapLayer() =
        SymbolLayer(LAYER_POI_ON_MAP_ID, SOURCE_POI_ON_MAP_ID)
            .withProperties(
                PropertyFactory.iconImage(getPoiOnMapImageExpression()),
                PropertyFactory.iconAllowOverlap(true),
                PropertyFactory.iconAnchor(Property.ICON_ANCHOR_CENTER),
            ).apply {
                minZoom = 10.0f
            }

    private fun getPoiOnMapImageExpression(): Expression {
        val expressions = mutableListOf<Expression>()
        PlaceOnMapUiModel.entries.forEach {
            expressions.add(
                all(
                    eq(
                        get(literal(POI_ON_MAP_TYPE)),
                        literal(it.name)
                    )
                )
            )
            expressions.add(literal(POI_ON_MAP_IMAGE + it.name))
        }
        expressions.add(literal(POI_ON_MAP_IMAGE + PoiSymbol.GENERIC.name))
        @Suppress("SpreadOperator")
        return switchCase(*expressions.toTypedArray())
    }
}
