package com.ravenfeld.maps.ui.track.details

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ravenfeld.maps.domain.geo.Highway
import com.ravenfeld.maps.domain.geo.StatsHighway
import com.ravenfeld.maps.domain.geo.StatsSurface
import com.ravenfeld.maps.domain.geo.Surface
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf

@Composable
fun StatsTrack(
    statsHighway: ImmutableList<StatsHighway>,
    statsSurface: ImmutableList<StatsSurface>,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier.padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        StatsHighway(statsHighway = statsHighway)
        StatsSurface(statsSurface = statsSurface)
    }
}

@Preview
@Composable
private fun StatsTrackPreview() {
    StatsTrack(
        statsHighway = persistentListOf(
            StatsHighway(Highway.Road, 1.0, 50),
            StatsHighway(Highway.HikingPath, 10.0, 50)
        ),
        statsSurface = persistentListOf(
            StatsSurface(Surface.Natural, 1.0, 50)
        )
    )
}
