package com.ravenfeld.maps.ui.maps

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.ravenfeld.maps.R
import com.ravenfeld.maps.domain.place.model.PlaceOnMap
import com.ravenfeld.maps.extension.toBitmap
import com.ravenfeld.maps.ui.place.PlaceOnMapUiModel
import org.maplibre.android.geometry.LatLng
import org.maplibre.android.maps.MapLibreMap
import org.maplibre.android.maps.MapView
import org.maplibre.android.style.layers.Property
import org.maplibre.android.style.layers.PropertyFactory
import org.maplibre.android.style.layers.SymbolLayer
import org.maplibre.android.style.sources.GeoJsonSource
import org.maplibre.geojson.Point

private const val ANNOTATION_IMAGE_ID = "annotation_image_id"
private const val ANNOTATION_SOURCE_ID = "annotation_source_id"
private const val ANNOTATION_LAYER_ID = "annotation_layer_id"

class PlaceOnMapAnnotation(private val mapView: MapView) {

    private val viewLayout: FrameLayout = FrameLayout(mapView.context)
    private var center: LatLng? = null

    init {
        viewLayout.layoutParams = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT,
        )
    }
    fun isDisplay(latLng: LatLng) =
        center?.let {
            it.latitude == latLng.latitude &&
                it.longitude == latLng.longitude
        } ?: false

    fun remove(map: MapLibreMap): Boolean =
        map.style?.let {
            center = null
            it.removeLayer(ANNOTATION_LAYER_ID)
            it.removeImage(ANNOTATION_IMAGE_ID)
            it.removeSource(ANNOTATION_SOURCE_ID)
        } ?: false

    fun create(map: MapLibreMap, placeOnMap: PlaceOnMap) {
        remove(map)
        this.center = placeOnMap.center
        val view = LayoutInflater.from(mapView.context)
            .inflate(R.layout.annotation, viewLayout, false)
        view.findViewById<TextView>(R.id.text).text =
            placeOnMap.text ?: mapView.context.getString(PlaceOnMapUiModel.valueOf(placeOnMap.type.name).textId)

        map.style?.apply {
            addImage(ANNOTATION_IMAGE_ID, view.toBitmap())
            addSource(
                GeoJsonSource(ANNOTATION_SOURCE_ID, placeOnMap.center.toPoint())
            )
            addLayer(
                SymbolLayer(
                    ANNOTATION_LAYER_ID,
                    ANNOTATION_SOURCE_ID
                ).withProperties(
                    PropertyFactory.iconImage(ANNOTATION_IMAGE_ID),
                    PropertyFactory.iconAnchor(Property.ICON_ANCHOR_BOTTOM),
                    @Suppress("MagicNumber")
                    PropertyFactory.iconOffset(arrayOf(0.0f, -12.0f))
                )
            )
        }
    }
}

private fun LatLng.toPoint() =
    Point.fromLngLat(longitude, latitude)
