package com.ravenfeld.maps.ui.options

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
enum class Option : Parcelable {
    REVERSE,
    BACK_HOME,
    REMOVE_LAST_POINT
}
