package com.ravenfeld.maps.ui.add

import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.R
import com.ravenfeld.maps.data.utils.safeLet
import com.ravenfeld.maps.databinding.DialogAddEditMapBinding
import com.ravenfeld.maps.ui.common.RequestEvent
import com.ravenfeld.maps.ui.common.TextError
import com.ravenfeld.maps.ui.common.getOrThrow
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class AddEditMapDialog : AppCompatDialogFragment(), AddEditMapPresenter {

    private var _binding: DialogAddEditMapBinding? = null
    private val binding get() = _binding.getOrThrow()

    private val viewModel: AddEditMapViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogAddEditMapBinding.inflate(inflater, container, false)
        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                launch {
                    viewModel.events.collect {
                        it.getContentIfNotHandled()?.let { event ->
                            when (event) {
                                RequestEvent.Error -> binding.uiModel?.nameError = TextError.NAME_EXIST
                                RequestEvent.Success -> findNavController().navigateUp()
                            }
                        }
                    }
                }
                launch {
                    viewModel.uiModel(
                        AddEditMapDialogArgs.fromBundle(requireArguments()).mode,
                        AddEditMapDialogArgs.fromBundle(requireArguments()).name
                    ).collect {
                        binding.uiModel = it
                    }
                }
            }
        }
    }

    override fun onValidateClick(mode: DialogMode) {
        binding.uiModel?.let {
            when (mode) {
                DialogMode.ADD -> safeLet(it.name, it.url) { name, url ->
                    viewModel.add(name, url, it.zoom[0], it.zoom[1])
                }

                DialogMode.EDIT -> safeLet(it.name, it.url) { name, url ->
                    viewModel.save(name, url, it.zoom[0], it.zoom[1])
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            @Suppress("deprecation")
            val display = requireActivity().windowManager.defaultDisplay
            val size = Point()
            @Suppress("deprecation")
            display.getSize(size)
            val params = it.attributes
            val widthMax = resources.getDimensionPixelSize(R.dimen.max_width)

            @Suppress("MagicNumber")
            val width = (size.x * 0.9).toInt()
            params.width = if (width < widthMax) {
                width
            } else {
                widthMax
            }
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            it.attributes = params as android.view.WindowManager.LayoutParams
        }
    }
}
