package com.ravenfeld.maps.ui.name

interface NamePresenter {
    fun onButtonOkClick()
    fun onButtonCancelClick()
}
