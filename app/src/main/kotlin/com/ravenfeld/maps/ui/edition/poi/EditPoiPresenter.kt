package com.ravenfeld.maps.ui.edition.poi

interface EditPoiPresenter {
    fun onTypeClick()
    fun onSave()
    fun onRemove()
}
