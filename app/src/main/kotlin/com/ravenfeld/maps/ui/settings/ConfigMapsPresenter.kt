package com.ravenfeld.maps.ui.settings

interface ConfigMapsPresenter {
    fun onNavigationBack()
    fun onAddClick()
    fun onEnableChanged(name: String, enable: Boolean)
    fun onEdit(name: String)
    fun onDelete(name: String)
}
