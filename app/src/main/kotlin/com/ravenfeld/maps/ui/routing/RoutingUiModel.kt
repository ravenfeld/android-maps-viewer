package com.ravenfeld.maps.ui.routing

import com.ravenfeld.maps.domain.routing.RoutingMode

data class RoutingUiModel(val routingModeSelected: RoutingMode) {
    val listUiModel: List<ItemRoutingUiModel> by lazy {
        listOf(
            ItemRoutingUiModel(RoutingMode.FREE, routingModeSelected == RoutingMode.FREE),
            ItemRoutingUiModel(RoutingMode.HIKE, routingModeSelected == RoutingMode.HIKE),
            ItemRoutingUiModel(RoutingMode.MOUNTAIN_BIKE, routingModeSelected == RoutingMode.MOUNTAIN_BIKE),
            ItemRoutingUiModel(RoutingMode.ROAD_BIKE, routingModeSelected == RoutingMode.ROAD_BIKE)
        )
    }
}
