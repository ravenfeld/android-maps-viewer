package com.ravenfeld.maps.ui.edition

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AddOnTrackResult(val point: String, val type: Type) : Parcelable {
    enum class Type {
        WAYPOINT,
        POI
    }
}
