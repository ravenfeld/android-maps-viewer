package com.ravenfeld.maps.ui.maps

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.github.mikephil.charting.charts.LineChart
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.ravenfeld.maps.R

class CustomBottomSheetBehavior<V : View> @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : BottomSheetBehavior<V>(context, attrs) {
    private val viewId: Int
    private var viewTouch: View? = null

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.CustomBottomSheetBehavior)
        viewId = a.getResourceId(R.styleable.CustomBottomSheetBehavior_touchAnchorId, -1)
        a.recycle()
    }

    override fun onLayoutChild(parent: CoordinatorLayout, child: V, layoutDirection: Int): Boolean {
        viewTouch = child.findViewById<LineChart>(viewId)
        return super.onLayoutChild(parent, child, layoutDirection)
    }

    override fun onInterceptTouchEvent(
        parent: CoordinatorLayout,
        child: V,
        event: MotionEvent
    ): Boolean {
        return if (isViewTouchTarget(event)) {
            false
        } else {
            super.onInterceptTouchEvent(parent, child, event)
        }
    }

    private fun isViewTouchTarget(event: MotionEvent): Boolean {
        var touch = false
        viewTouch?.let { v ->
            val queenLocation = IntArray(2)
            v.getLocationOnScreen(queenLocation)
            val upperLimit: Int = queenLocation[1] + v.measuredHeight
            val lowerLimit = queenLocation[1]
            val y = event.rawY.toInt()
            touch = touch || y in lowerLimit + 1 until upperLimit
        }
        return touch
    }

    override fun onAttachedToLayoutParams(layoutParams: CoordinatorLayout.LayoutParams) {
        super.onAttachedToLayoutParams(layoutParams)
        viewTouch = null
    }

    override fun onDetachedFromLayoutParams() {
        super.onDetachedFromLayoutParams()
        viewTouch = null
    }
}
