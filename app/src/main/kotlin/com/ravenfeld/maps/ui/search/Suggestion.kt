package com.ravenfeld.maps.ui.search

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewFontScale
import androidx.compose.ui.tooling.preview.PreviewScreenSizes
import androidx.compose.ui.unit.dp
import com.ravenfeld.maps.R
import com.ravenfeld.maps.ui.search.model.SuggestionItemUiModel
import java.text.DecimalFormat

@Composable
fun Suggestion(
    suggestion: SuggestionItemUiModel,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier
            .clickable {
                onClick()
            }
            .padding(vertical = 14.dp, horizontal = 8.dp),
    ) {
        Image(
            modifier = Modifier
                .clip(RoundedCornerShape(8.dp))
                .background(MaterialTheme.colorScheme.tertiaryContainer)
                .padding(8.dp),
            contentScale = ContentScale.None,
            painter = painterResource(suggestion.iconResId),
            contentDescription = null
        )
        Column(
            modifier = Modifier.padding(start = 12.dp),
        ) {
            suggestion.title?.let {
                Text(
                    text = suggestion.title,
                    color = MaterialTheme.colorScheme.onSecondary,
                    style = MaterialTheme.typography.bodyLarge
                )
            }
            Text(
                modifier = Modifier.padding(top = 6.dp),
                text = suggestion.typeResId?.let {
                    "${stringResource(suggestion.typeResId)}, ${suggestion.subTitle}"
                } ?: suggestion.subTitle,
                color = MaterialTheme.colorScheme.onPrimaryContainer,
                style = MaterialTheme.typography.bodyMedium
            )
            suggestion.distance?.let {
                Text(
                    modifier = Modifier.padding(top = 4.dp),
                    text = stringResource(R.string.distance_to, it.formatDistance()),
                    color = MaterialTheme.colorScheme.onPrimaryContainer,
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }
    }
}

@PreviewScreenSizes
@PreviewFontScale
@Preview
@Composable
private fun SuggestionPreview() {
    Suggestion(
        suggestion = SuggestionItemUiModel(
            id = 1,
            iconResId = R.drawable.ic_peak,
            typeResId = R.string.peak,
            title = "title",
            subTitle = "subTitle",
            distance = 10
        ),
        onClick = {}
    )
}

fun Context.formatDistance(distance: Int): String =
    distance.let {
        if (it / 100 > 0) {
            val decimalFormat = DecimalFormat("0.0")
            getString(
                R.string.distance_short_format_km,
                decimalFormat.format(it / 1_000.0)
            )
        } else {
            getString(
                R.string.distance_short_format_m,
                it
            )
        }
    }

@Composable
@ReadOnlyComposable
fun Int.formatDistance(): String = LocalContext.current.formatDistance(
    distance = this
)
