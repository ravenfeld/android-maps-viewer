package com.ravenfeld.maps.ui.maps

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.ravenfeld.maps.R
import com.ravenfeld.maps.data.utils.safeLet
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.distanceBetween
import com.ravenfeld.maps.domain.routing.RoutingMode
import com.ravenfeld.maps.ui.common.ItemDecoration
import com.ravenfeld.maps.ui.common.formatDistance
import com.ravenfeld.maps.ui.common.formatElevation

@BindingAdapter("routingIcon")
fun FloatingActionButton.setRoutingIcon(routingMode: RoutingMode) {
    when (routingMode) {
        RoutingMode.FREE -> setImageResource(R.drawable.icon_routes_free)
        RoutingMode.HIKE -> setImageResource(R.drawable.icon_routes_hike)
        RoutingMode.MOUNTAIN_BIKE -> setImageResource(R.drawable.icon_routes_mountainbike)
        RoutingMode.ROAD_BIKE -> setImageResource(R.drawable.icon_routes_roadbike)
    }
}

@SuppressLint("SetTextI18n")
@BindingAdapter("elevation")
fun TextView.setTextElevation(ele: Double) {
    text = formatElevation(ele)
}

@SuppressLint("SetTextI18n")
@BindingAdapter("distance")
fun TextView.setTextDistance(distance: Double) {
    text = formatDistance(distance)
}

@BindingAdapter("items", "presenter")
fun RecyclerView.bindItemMapUiModel(
    items: List<ItemMapUiModel>?,
    presenter: MapPresenter
) {
    items?.let {
        val adapter = getOrCreateListMapAdapter(this, presenter)
        adapter.submitList(items)
    }
}

private fun getOrCreateListMapAdapter(
    recyclerView: RecyclerView,
    presenter: MapPresenter
): ListMapAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ListMapAdapter) {
        recyclerView.adapter as ListMapAdapter
    } else {
        val bindableRecyclerAdapter = ListMapAdapter(presenter)
        recyclerView.adapter = bindableRecyclerAdapter
        ContextCompat.getDrawable(recyclerView.context, R.drawable.divider)?.let {
            recyclerView.addItemDecoration(
                ItemDecoration(it, false)
            )
        }
        enableDrag(presenter, recyclerView)
        bindableRecyclerAdapter
    }
}

@BindingAdapter("items", "presenter")
fun RecyclerView.bindItemTrackUiModel(
    items: List<ItemTrackUiModel>?,
    presenter: MapPresenter
) {
    items?.sortedByDescending { it.id }?.let { list ->
        val adapter = getOrCreateListTrackAdapter(this, presenter)
        adapter.submitList(list)
    }
}

private fun getOrCreateListTrackAdapter(
    recyclerView: RecyclerView,
    presenter: MapPresenter
): ListTrackAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ListTrackAdapter) {
        recyclerView.adapter as ListTrackAdapter
    } else {
        val bindableRecyclerAdapter = ListTrackAdapter(presenter)
        recyclerView.adapter = bindableRecyclerAdapter
        ContextCompat.getDrawable(recyclerView.context, R.drawable.divider)?.let {
            recyclerView.addItemDecoration(
                ItemDecoration(it, false)
            )
        }
        bindableRecyclerAdapter
    }
}

private fun enableDrag(
    presenter: MapPresenter,
    recyclerView: RecyclerView
) {
    ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
        ItemTouchHelper.UP or ItemTouchHelper.DOWN,
        0
    ) {
        private var oldBackgroundColor: Int? = null
        private var from: Int? = null
        private var to: Int? = null

        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            val adapter = recyclerView.adapter as ListMapAdapter
            val from = viewHolder.adapterPosition
            val to = target.adapterPosition
            adapter.moveItem(from, to)
            this.from?.let {
                this.to = to
            } ?: run {
                this.from = from
                this.to = to
            }

            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            // no op
        }

        override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
            super.onSelectedChanged(viewHolder, actionState)
            if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
                viewHolder?.let {
                    oldBackgroundColor = (it.itemView.background as ColorDrawable).color
                    it.itemView.setBackgroundColor(
                        ContextCompat.getColor(
                            it.itemView.context,
                            R.color.grey_drag
                        )
                    )
                    it.itemView.z = 1.0f
                }
            }
        }

        override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
            oldBackgroundColor?.let {
                viewHolder.itemView.setBackgroundColor(it)
            }
            viewHolder.itemView.z = 0.0f

            if (from != to) {
                val adapter = recyclerView.adapter as ListMapAdapter
                safeLet(from, to, adapter.listOriginal) { from, to, maps ->
                    presenter.onMapMoveOrder(maps[from].order, maps[to].order)
                }
            }
            from = null
            to = null
        }
    }).apply { attachToRecyclerView(recyclerView) }
}

@BindingAdapter("color")
fun ImageView.setColor(color: String?) {
    color?.let {
        setColorFilter(Color.parseColor(it))
    } ?: run {
        setColorFilter(Color.TRANSPARENT)
    }
}

@BindingAdapter("trackingGPS", "cameraPosition", "userLocation", "userAltitudeMsl", "altitude")
fun TextView.setMapInfo(
    trackingGPS: Boolean,
    cameraPosition: GeoPoint?,
    userLocation: GeoPoint?,
    userAltitudeMsl: Double?,
    altitude: Double?
) {
    if (cameraPosition != null) {
        text = if (trackingGPS) {
            if (altitude != null || userAltitudeMsl != null || userLocation != null) {
                context.resources.getString(
                    R.string.map_info_center_user,
                    cameraPosition.latitude,
                    cameraPosition.longitude,
                    formatElevation(altitude ?: userAltitudeMsl ?: userLocation?.altitude ?: 0.0)
                )
            } else {
                context.resources.getString(
                    R.string.map_info,
                    cameraPosition.latitude,
                    cameraPosition.longitude
                )
            }
        } else if (userLocation != null) {
            altitude?.let {
                context.resources.getString(
                    R.string.map_info_no_center_user_with_alti,
                    cameraPosition.latitude,
                    cameraPosition.longitude,
                    formatElevation(it),
                    formatDistance(
                        distanceBetween(
                            cameraPosition,
                            userLocation
                        )
                    )
                )
            } ?: run {
                context.resources.getString(
                    R.string.map_info_no_center_user,
                    cameraPosition.latitude,
                    cameraPosition.longitude,
                    formatDistance(
                        distanceBetween(
                            cameraPosition,
                            userLocation
                        )
                    )
                )
            }
        } else {
            context.resources.getString(
                R.string.map_info,
                cameraPosition.latitude,
                cameraPosition.longitude
            )
        }
    }
}
