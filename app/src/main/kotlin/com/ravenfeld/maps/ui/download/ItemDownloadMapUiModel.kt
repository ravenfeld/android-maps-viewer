package com.ravenfeld.maps.ui.download

import com.ravenfeld.maps.domain.map.Map

@Suppress("DataClassShouldBeImmutable")
data class ItemDownloadMapUiModel(
    val name: String,
    val visible: Boolean,
    val enable: Boolean
) {

    var checked = visible && enable
}

fun Map.toItemDownloadMapUiModel() = ItemDownloadMapUiModel(name, visible, enable)
