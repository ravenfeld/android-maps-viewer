package com.ravenfeld.maps.ui.maps

data class TouchInfoUiModel(
    val distance: Double,
    val upHill: Double,
    val downHill: Double,
)
