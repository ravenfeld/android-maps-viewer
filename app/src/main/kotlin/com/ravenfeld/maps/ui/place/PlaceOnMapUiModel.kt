package com.ravenfeld.maps.ui.place

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.ravenfeld.maps.R

enum class PlaceOnMapUiModel(
    @DrawableRes val iconMapId: Int,
    @StringRes val textId: Int,
) {
    PEAK(
        iconMapId = R.drawable.poi_peak,
        textId = R.string.peak
    ),
    ALPINE_HUT(
        iconMapId = R.drawable.poi_alpine_hut,
        textId = R.string.alpine_hut
    ),
    WATER(
        iconMapId = R.drawable.poi_water,
        textId = R.string.waterpoint
    ),
    PARKING(
        iconMapId = R.drawable.poi_parking,
        textId = R.string.parking
    ),
    PARKING_BICYCLE(
        iconMapId = R.drawable.poi_parking_bicycle,
        textId = R.string.parking_bicycle
    ),
    TOILET(
        iconMapId = R.drawable.poi_toilet,
        textId = R.string.toilet
    ),
    SADDLE(
        iconMapId = R.drawable.poi_saddle,
        textId = R.string.saddle
    ),
    WILDERNESS_HUT(
        iconMapId = R.drawable.poi_wilderness_hut,
        textId = R.string.wilderness_hut
    ),
    SHOP(
        iconMapId = R.drawable.poi_shop,
        textId = R.string.shop
    ),
    CAMP_SITE(
        iconMapId = R.drawable.poi_camp_site,
        textId = R.string.camp_site
    )
}
