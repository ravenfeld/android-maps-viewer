package com.ravenfeld.maps.ui.name

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.databinding.DialogNameBinding
import com.ravenfeld.maps.ui.common.getOrThrow
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class NameDialog : AppCompatDialogFragment(), NamePresenter {

    private var _binding: DialogNameBinding? = null
    private val binding by lazy {
        _binding.getOrThrow()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DialogNameBinding.inflate(inflater, container, false)

        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        @SuppressWarnings("MagicNumber")
        val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
        dialog?.window?.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT)
        binding.uiModel = NameUiModel(NameDialogArgs.fromBundle(requireArguments()).nameDefault)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @Suppress("MagicNumber")
    override fun onButtonOkClick() {
        binding.uiModel?.name?.let { name ->
            findNavController().previousBackStackEntry?.savedStateHandle?.set(
                STATE_NAME_SELECTED,
                NameResult(
                    NameDialogArgs.fromBundle(requireArguments()).trackId,
                    name
                )

            )
        }
    }

    override fun onButtonCancelClick() {
        dismiss()
    }

    companion object {
        const val STATE_NAME_SELECTED = "name_selected"
    }
}
