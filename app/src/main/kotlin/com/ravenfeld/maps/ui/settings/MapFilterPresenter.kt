package com.ravenfeld.maps.ui.settings

interface MapFilterPresenter {
    fun onEnableChanged(name: String, enable: Boolean)
    fun onEdit(name: String)
    fun onDelete(name: String)
    fun onExpandedCollapsed(name: String)
}
