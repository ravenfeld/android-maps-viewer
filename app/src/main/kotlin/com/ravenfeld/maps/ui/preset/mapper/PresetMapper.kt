package com.ravenfeld.maps.ui.preset.mapper

import com.ravenfeld.maps.domain.preset.model.Preset
import com.ravenfeld.maps.ui.preset.PresetUiModel
import kotlinx.collections.immutable.toImmutableList

fun List<Preset>.toUiModel() = this.map(Preset::toUiModel).toImmutableList()

fun Preset.toUiModel() = PresetUiModel(id = id, name = name, selected = selected)
