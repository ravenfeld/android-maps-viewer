package com.ravenfeld.maps.ui.color

import android.graphics.Color
import androidx.databinding.BaseObservable

class ColorPickerUiModel : BaseObservable() {
    var indicatorColor: Int = Color.RED
        set(value) {
            field = value
            notifyChange()
        }
}
