package com.ravenfeld.maps.ui.edition.poi

import android.os.Parcelable
import com.ravenfeld.maps.domain.geo.PoiSymbol
import kotlinx.parcelize.Parcelize

@Parcelize
sealed class EditPoiResult(open val point: String) : Parcelable {
    @Parcelize
    data class Save(
        override val point: String,
        val name: String,
        val symbol: PoiSymbol
    ) : EditPoiResult(point = point)

    @Parcelize
    data class Remove(override val point: String) : EditPoiResult(point = point)
}
