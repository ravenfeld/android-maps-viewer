package com.ravenfeld.maps.ui.settings

import com.ravenfeld.maps.domain.map.MapBase

@Suppress("DataClassShouldBeImmutable")
data class MapsFilterUiModel(val stream: Boolean, var listUiModel: List<ItemMapFilterBaseUiModel>)

@Suppress("NestedBlockDepth")
fun List<MapBase>.toListMapsFilterUiModel(
    stream: Boolean,
    oldListUiModel: List<ItemMapFilterBaseUiModel>?
): MapsFilterUiModel {
    val listMapsFilterUiModel = ArrayList<ItemMapFilterBaseUiModel>()
    forEach { mapBase ->
        when (mapBase) {
            is com.ravenfeld.maps.domain.map.Map -> listMapsFilterUiModel.add(mapBase.toItemMapFilterUiModel())
            is com.ravenfeld.maps.domain.map.MapGroup -> {
                val group = mapBase.toItemMapGroupFilterUiModel(
                    oldListUiModel?.find { it.name == mapBase.name }?.let {
                        (it as ItemMapGroupFilterUiModel).expand
                    } ?: run {
                        false
                    }
                )
                listMapsFilterUiModel.add(group)
                if (group.expand) {
                    mapBase.listMap.forEach { map ->
                        listMapsFilterUiModel.add(map.toItemSubMapFilterUiModel(group.name))
                    }
                }
            }
            else -> throw ClassCastException("Unable to convert the object ")
        }
    }
    return MapsFilterUiModel(
        stream,
        listMapsFilterUiModel
    )
}
