package com.ravenfeld.maps.ui.maps

import android.app.Activity
import android.location.LocationManager
import android.provider.Settings
import androidx.activity.result.IntentSenderRequest
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.Priority
import com.google.android.gms.location.SettingsClient
import com.google.android.gms.tasks.Task

// Default value in LocationRequest
private const val INTERVAL_MILLIS = 3_600_000L

class AskLocation internal constructor(
    activity: Activity,
    val onSuccess: () -> Unit,
    val onLaunchSettingsRequest: (intentSendRequest: IntentSenderRequest) -> Unit,
    val onAirPlaneMode: () -> Unit
) {
    private val context = activity.applicationContext
    private val locationRequest = LocationRequest.Builder(
        Priority.PRIORITY_HIGH_ACCURACY,
        INTERVAL_MILLIS
    ).build()

    private val locationSettingsRequest = LocationSettingsRequest.Builder()
        .addLocationRequest(locationRequest)
        .build()

    private val client: SettingsClient = activity.let(LocationServices::getSettingsClient)

    private fun Task<LocationSettingsResponse>.addListeners() {
        addOnSuccessListener(successListener())
        addOnFailureListener(failureListener())
    }

    private fun failureListener(): (Exception) -> Unit = { exception ->
        if (exception is ResolvableApiException) {
            onLaunchSettingsRequest(IntentSenderRequest.Builder(exception.resolution).build())
        }
    }

    private fun successListener(): (LocationSettingsResponse) -> Unit = {
        onSuccess()
    }

    fun launch() {
        if (hasGpsOn()) {
            onSuccess()
            return
        }

        if (hasAirPlaneOn()) {
            onAirPlaneMode()
            return
        }

        client.checkLocationSettings(locationSettingsRequest).addListeners()
    }

    private fun hasGpsOn(): Boolean = context
        ?.let {
            ContextCompat.getSystemService(
                it,
                LocationManager::class.java
            )
        }
        ?.isProviderEnabled(LocationManager.GPS_PROVIDER) == true

    private fun hasAirPlaneOn(): Boolean =
        Settings.Global.getInt(
            context?.contentResolver,
            Settings.Global.AIRPLANE_MODE_ON,
            0
        ) != 0
}
