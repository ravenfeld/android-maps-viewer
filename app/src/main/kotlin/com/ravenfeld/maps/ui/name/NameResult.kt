package com.ravenfeld.maps.ui.name

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NameResult(val trackId: Int, val name: String) : Parcelable
