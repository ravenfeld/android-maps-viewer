package com.ravenfeld.maps.ui.track.export

import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.R
import com.ravenfeld.maps.databinding.DialogExportTrackBinding
import com.ravenfeld.maps.domain.track.ExportTrack
import com.ravenfeld.maps.ui.common.getOrThrow

class ExportTrackDialog : AppCompatDialogFragment(), ExportTrackPresenter {

    private var _binding: DialogExportTrackBinding? = null
    private val binding get() = _binding.getOrThrow()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogExportTrackBinding.inflate(inflater, container, false)
        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.presenter = this
        binding.uiModel = ExportTrackUiModel(
            ExportTrackDialogArgs.fromBundle(requireArguments()).name,
            ExportTrackDialogArgs.fromBundle(requireArguments()).type
        )
        binding.nameTextInput.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                binding.uiModel?.name = ""
            }
        }
    }

    override fun onValidateClick() {
        binding.uiModel?.let { uiModel ->
            uiModel.name?.let {
                findNavController().previousBackStackEntry?.savedStateHandle?.set(
                    STATE_CLICK_SEND_TRACK,
                    ExportTrack(
                        it,
                        uiModel.exportType
                    )
                )
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            @Suppress("deprecation")
            val display = requireActivity().windowManager.defaultDisplay
            val size = Point()
            @Suppress("deprecation")
            display.getSize(size)
            val params = it.attributes
            val widthMax = resources.getDimensionPixelSize(R.dimen.max_width)

            @Suppress("MagicNumber")
            val width = (size.x * 0.9).toInt()
            params.width = if (width < widthMax) {
                width
            } else {
                widthMax
            }
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            it.attributes = params as android.view.WindowManager.LayoutParams
        }
    }

    companion object {
        const val STATE_CLICK_SEND_TRACK = "click_send"
    }
}
