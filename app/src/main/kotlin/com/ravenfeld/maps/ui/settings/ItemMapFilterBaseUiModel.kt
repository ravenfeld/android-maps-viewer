package com.ravenfeld.maps.ui.settings

import com.ravenfeld.maps.domain.map.MapStorage

open class ItemMapFilterBaseUiModel(
    val name: String,
    val storage: MapStorage,
    val enable: Boolean
)
