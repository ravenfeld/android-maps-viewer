package com.ravenfeld.maps.ui.maps

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.compose.runtime.NoLiveLiterals
import androidx.core.content.ContextCompat
import com.ravenfeld.maps.R
import kotlin.math.sqrt

@NoLiveLiterals
class AreaView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var pointSelected = 0

    private var paintStroke: Paint = Paint().apply {
        style = Paint.Style.STROKE
        color = ContextCompat.getColor(context, R.color.area_line)
        strokeWidth = resources.getDimension(R.dimen.area_stroke)
    }

    private var paintCircle: Paint = Paint().apply {
        color = ContextCompat.getColor(context, R.color.area_line)
    }

    private var paintOuter: Paint = Paint().apply {
        color = ContextCompat.getColor(context, R.color.area_outer)
        xfermode = PorterDuffXfermode(PorterDuff.Mode.ADD)
    }

    private var paintInner: Paint = Paint().apply {
        color = Color.TRANSPARENT
        xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    }

    @Suppress("MagicNumber")
    val points: Array<PointF> = Array(4) { PointF() }

    init {
        isFocusable = true
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        initArea()
    }

    @Suppress("MagicNumber")
    private fun initArea() {
        if (points.any { it.x == 0f && it.y == 0f }) {
            val centerX = width / 2f
            val centerY = height / 2f
            val size = resources.getDimensionPixelSize(R.dimen.area_size)
            points[0].x = centerX - size
            points[0].y = centerY - size
            points[1].x = centerX - size
            points[1].y = centerY + size
            points[2].x = centerX + size
            points[2].y = centerY + size
            points[3].x = centerX + size
            points[3].y = centerY - size
        }
    }

    @Suppress("MagicNumber")
    override fun onDraw(canvas: Canvas) {
        val left = points[0].x
        val top = points[0].y
        val right = points[2].x
        val bottom = points[2].y

        canvas.drawRect(0.0f, 0.0f, width.toFloat(), height.toFloat(), paintOuter)
        canvas.drawRect(left, top, right, bottom, paintInner)

        canvas.drawRect(left, top, right, bottom, paintStroke)

        points.forEach {
            canvas.drawCircle(
                it.x,
                it.y,
                resources.getDimension(R.dimen.area_pointer_radius),
                paintCircle
            )
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Suppress("MagicNumber")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        val eventX = event.x
        val eventY = event.y
        var eventHandled = false
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                pointSelected = -1
                var i = 0
                while (i < points.size && pointSelected < 0) {
                    val ball = points[i]
                    val pointerX = ball.x
                    val pointerY = ball.y
                    val radCircle =
                        sqrt(
                            (
                                (pointerX - eventX) * (pointerX - eventX) +
                                    (pointerY - eventY) * (pointerY - eventY)
                                ).toDouble()
                        )
                    if (radCircle < resources.getDimension(R.dimen.area_pointer_radius_detekt)) {
                        pointSelected = i
                    } else {
                        i++
                    }
                }
                invalidate()
                eventHandled = pointSelected >= 0
            }

            MotionEvent.ACTION_MOVE -> if (pointSelected > -1) {
                points[pointSelected].x = eventX
                points[pointSelected].y = eventY
                if (pointSelected == 0 || pointSelected == 2) {
                    points[1].x = points[0].x
                    points[1].y = points[2].y
                    points[3].x = points[2].x
                    points[3].y = points[0].y
                } else {
                    points[0].x = points[1].x
                    points[0].y = points[3].y
                    points[2].x = points[3].x
                    points[2].y = points[1].y
                }
                invalidate()
                eventHandled = true
            }
        }
        return eventHandled
    }
}
