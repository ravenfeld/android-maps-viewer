package com.ravenfeld.maps.ui.options

interface OptionsPresenter {
    fun onOptionSelected(option: Option)
}
