package com.ravenfeld.maps.ui.options

import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.R
import com.ravenfeld.maps.ui.common.ItemDecoration

@BindingAdapter("items", "presenter")
fun RecyclerView.bindItemOptionUiModel(
    items: List<Option>?,
    presenter: OptionsPresenter
) {
    items?.let {
        val adapter = getOrCreateAdapter(this, presenter)
        adapter.submitList(items)
    }
}

private fun getOrCreateAdapter(
    recyclerView: RecyclerView,
    presenter: OptionsPresenter
): ListOptionAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ListOptionAdapter) {
        recyclerView.adapter as ListOptionAdapter
    } else {
        val bindableRecyclerAdapter = ListOptionAdapter(presenter)
        recyclerView.adapter = bindableRecyclerAdapter
        ContextCompat.getDrawable(recyclerView.context, R.drawable.divider)?.let {
            recyclerView.addItemDecoration(
                ItemDecoration(it, false)
            )
        }
        bindableRecyclerAdapter
    }
}

@BindingAdapter("optionIcon")
fun ImageView.setOptionIcon(routingMode: Option) {
    when (routingMode) {
        Option.BACK_HOME -> setImageResource(R.drawable.ic_back_home)
        Option.REVERSE -> setImageResource(R.drawable.ic_reverse_track)
        Option.REMOVE_LAST_POINT -> setImageResource(R.drawable.ic_remove_last_waypoint)
    }
}

@BindingAdapter("optionTitle")
fun TextView.setOptionTitle(routingMode: Option) {
    when (routingMode) {
        Option.BACK_HOME -> setText(R.string.option_back_home_title)
        Option.REVERSE -> setText(R.string.option_reverse_title)
        Option.REMOVE_LAST_POINT -> setText(R.string.option_remove_last_point_title)
    }
}

@BindingAdapter("optionSubtitle")
fun TextView.setOptionSubtitle(routingMode: Option) {
    when (routingMode) {
        Option.BACK_HOME -> setText(R.string.option_back_home_sbtitle)
        Option.REVERSE -> setText(R.string.option_reverse_subtitle)
        Option.REMOVE_LAST_POINT -> setText(R.string.option_remove_last_point_subtitle)
    }
}
