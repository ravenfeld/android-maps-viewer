package com.ravenfeld.maps.ui.settings

import com.ravenfeld.maps.domain.map.Map
import com.ravenfeld.maps.domain.map.MapStorage

class ItemSubMapFilterUiModel(
    name: String,
    val groupName: String,
    val url: String,
    storage: MapStorage,
    enable: Boolean
) : ItemMapFilterBaseUiModel(name, storage, enable)

fun Map.toItemSubMapFilterUiModel(groupName: String): ItemSubMapFilterUiModel = ItemSubMapFilterUiModel(
    name,
    groupName,
    url,
    storage,
    enable
)
