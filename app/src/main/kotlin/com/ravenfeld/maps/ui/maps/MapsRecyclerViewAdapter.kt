package com.ravenfeld.maps.ui.maps

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.databinding.ItemMapBinding

private val DIFF_UTILS = object : DiffUtil.ItemCallback<ItemMapUiModel>() {

    override fun areItemsTheSame(
        oldItem: ItemMapUiModel,
        newItem: ItemMapUiModel
    ): Boolean = oldItem.name == newItem.name

    override fun areContentsTheSame(
        oldItem: ItemMapUiModel,
        newItem: ItemMapUiModel
    ) = oldItem.name == newItem.name &&
        oldItem.alpha == newItem.alpha &&
        oldItem.visible == newItem.visible
}

class ListMapAdapter(private val presenter: MapPresenter) :
    ListAdapter<ItemMapUiModel, MapViewHolder>(DIFF_UTILS) {

    var listOriginal: List<ItemMapUiModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MapViewHolder {
        val binding = ItemMapBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return MapViewHolder(binding, presenter)
    }

    override fun onBindViewHolder(holder: MapViewHolder, position: Int) =
        holder.bind(getItem(position))

    override fun submitList(list: List<ItemMapUiModel>?) {
        super.submitList(list)
        listOriginal = list
    }

    fun moveItem(from: Int, to: Int) {
        val list = currentList.toMutableList()
        val fromLocation = list[from]
        list.removeAt(from)
        list.add(to, fromLocation)
        super.submitList(list)
    }
}

class MapViewHolder(
    private val binding: ItemMapBinding,
    private val presenter: MapPresenter
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(uiModel: ItemMapUiModel) {
        binding.uiModel = uiModel
        binding.presenter = presenter
        binding.executePendingBindings()
    }
}
