package com.ravenfeld.maps.ui.maps

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ravenfeld.maps.domain.altitude.AltitudeRepository
import com.ravenfeld.maps.domain.event.GetTrackEvent
import com.ravenfeld.maps.domain.geo.GeoPoint
import com.ravenfeld.maps.domain.geo.Poi
import com.ravenfeld.maps.domain.map.DisplayInfo
import com.ravenfeld.maps.domain.map.Map
import com.ravenfeld.maps.domain.map.MapRepository
import com.ravenfeld.maps.domain.map.MapStorage
import com.ravenfeld.maps.domain.place.model.SearchPlaceOnMap
import com.ravenfeld.maps.domain.place.model.Type
import com.ravenfeld.maps.domain.place.usecase.ObservePoiOnMapUseCase
import com.ravenfeld.maps.domain.place.usecase.PlaceOnMapResult
import com.ravenfeld.maps.domain.preset.repository.PresetRepository
import com.ravenfeld.maps.domain.routing.RoutingMode
import com.ravenfeld.maps.domain.settings.SettingsRepository
import com.ravenfeld.maps.domain.track.TrackRepository
import com.ravenfeld.maps.ui.search.WhileSubscribedOrRetained
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.maplibre.android.geometry.LatLngBounds
import javax.inject.Inject

private const val DISTANCE_MAX_IN_METER = 50
private const val SEARCH_DEBOUNCE_IN_MILLIS = 500L

@Suppress("TooManyFunctions")
@HiltViewModel
class MapViewModel @Inject constructor(
    private val mapRepository: MapRepository,
    private val trackRepository: TrackRepository,
    private val settingsRepository: SettingsRepository,
    private val altitudeRepository: AltitudeRepository,
    private val presetRepository: PresetRepository,
    private val observePoiOnMap: ObservePoiOnMapUseCase
) :
    ViewModel() {

    val map: Flow<Pair<Boolean, List<Map>>> = combine(
        settingsRepository.getFlowMapsOfflineEnable(),
        mapRepository.getListMapEnable()
    ) { offlineEnable, listMap ->
        uiModel.offlineEnable = offlineEnable
        if (offlineEnable) {
            listMap.filter { it.storage != MapStorage.STREAM }
        } else {
            listMap
        }
    }.map { listMap ->
        val listMaps = listMap.filter {
            it.storage != MapStorage.MBTILES_VECTOR
        }.map { tiles -> tiles.toItemMapUiModel() }.reversed()
        val newOrderMap = listMaps.map { it.name } != uiModel.listItemMapUiModel.map { it.name }
        uiModel.listItemMapUiModel = listMaps
        Pair(newOrderMap, listMap)
    }

    val tracks: Flow<List<TrackUiModel>> = trackRepository.getTracks().map { list ->
        uiModel.listItemTrackUiModel = list.map { it.toItemTrackUiModel() }
        list.map { it.toTrackUiModel() }
    }

    val uiModel = MapUiModel()

    val loadFileState = trackRepository.getLoadFileState()

    private var searchPlaceOnMapOld: SearchPlaceOnMap? = null
    private val searchPlaceOnMap = MutableStateFlow<SearchPlaceOnMap?>(null)

    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    val placeOnMaps = searchPlaceOnMap
        .debounce(SEARCH_DEBOUNCE_IN_MILLIS)
        .filterNotNull()
        .flatMapLatest { searchPlaceOnMap ->
            observePoiOnMap(searchPlaceOnMapOld, searchPlaceOnMap).map { result ->
                when (result) {
                    PlaceOnMapResult.NoInternet -> null
                    is PlaceOnMapResult.Retrieved -> result.placeOnMaps
                    is PlaceOnMapResult.Loading -> {
                        searchPlaceOnMapOld = result.searchPlaceOnMap
                        null
                    }
                }
            }
        }
        .stateIn(
            scope = viewModelScope,
            started = WhileSubscribedOrRetained,
            initialValue = null
        )
        .filterNotNull()

    fun init() {
        viewModelScope.run {
            launch {
                mapRepository.init()
            }
            launch {
                trackRepository.getStateRouting().collect {
                    uiModel.engineRoutingReady = it
                }
            }
            launch {
                settingsRepository.getFlowRoutingMode().collect {
                    uiModel.routingMode = it
                }
            }
        }
    }

    fun getAltitudeMsl() {
        viewModelScope.launch {
            altitudeRepository.getAltitudeMsl().collect {
                uiModel.userAltitudeMsl = it
            }
        }
    }

    fun getLocationInfo(cameraPosition: GeoPoint, userLocation: GeoPoint?) {
        viewModelScope.launch {
            uiModel.altitude = altitudeRepository.getAltitude(
                point = cameraPosition,
            )
            uiModel.cameraPosition = cameraPosition
            uiModel.userLocation = userLocation
            uiModel.displayInfo = DisplayInfo.LOCALISATION
        }
    }

    fun mapMoveOrder(from: Int, to: Int) {
        viewModelScope.launch {
            mapRepository.moverOrder(from, to)
        }
    }

    fun mapAlpha(name: String, alpha: Float) {
        viewModelScope.launch {
            mapRepository.alpha(name, alpha)
        }
    }

    fun mapVisible(name: String, visible: Boolean) {
        viewModelScope.launch {
            mapRepository.visible(name, visible)
        }
    }

    fun loadFile(uri: Uri) {
        viewModelScope.launch {
            uiModel.isEditMode = false
            uiModel.hasLoadTrack = true
            val success = trackRepository.loadFile(uri)
            if (!success) {
                uiModel.hasLoadTrack = false
            }
        }
    }

    fun trackRemove(routeId: Int) {
        viewModelScope.launch {
            trackRepository.remove(routeId)
        }
    }

    fun trackColor(trackId: Int, color: String) {
        viewModelScope.launch {
            trackRepository.color(trackId, color)
        }
    }

    fun trackVisible(trackId: Int, visible: Boolean) {
        viewModelScope.launch {
            trackRepository.visible(trackId, visible)
        }
    }

    fun trackName(trackId: Int, name: String) {
        viewModelScope.launch {
            trackRepository.name(trackId, name)
        }
    }

    fun trackStart(start: GeoPoint) {
        viewModelScope.launch {
            trackRepository.startTrack(start)
        }
    }

    fun getSegment(
        routeId: Int,
        arrival: GeoPoint,
        zoom: Double,
        routingModel: RoutingMode
    ): Flow<GetTrackEvent> = trackRepository.getSegment(
        trackId = routeId,
        arrival = arrival,
        backHome = false,
        zoom = zoom,
        routingMode = routingModel
    )

    fun movePoint(
        routeId: Int,
        oldWayPoint: GeoPoint,
        newWayPoint: GeoPoint,
        zoom: Double,
        routingMode: RoutingMode
    ): Flow<GetTrackEvent> = trackRepository.moveWaypoint(
        trackId = routeId,
        oldWayPoint = oldWayPoint,
        newWayPoint = newWayPoint,
        zoom = zoom,
        routingMode = routingMode
    )

    fun addWaypoint(trackId: Int, point: GeoPoint) {
        viewModelScope.launch {
            trackRepository.addWaypoint(trackId = trackId, waypoint = point)
        }
    }

    fun savePoi(trackId: Int, poi: Poi) {
        viewModelScope.launch {
            trackRepository.savePoi(trackId = trackId, poi = poi)
        }
    }

    fun removePoi(trackId: Int, point: GeoPoint) {
        viewModelScope.launch {
            trackRepository.removePoi(trackId = trackId, point = point)
        }
    }

    fun getTrackBack(
        zoom: Double
    ): Flow<GetTrackEvent> =
        uiModel.track?.let {
            trackRepository.getSegment(
                trackId = it.id,
                arrival = it.trackPoints.first().geoPoint,
                backHome = true,
                zoom = zoom,
                routingMode = uiModel.routingMode
            )
        } ?: flowOf(GetTrackEvent.ERROR)

    fun removeLastPoint(routeId: Int) {
        viewModelScope.launch {
            trackRepository.removeLastPoint(routeId)
        }
    }

    fun cancelLastUserAction(routeId: Int): Flow<GetTrackEvent> = trackRepository.cancelLastUserAction(
        routeId
    )

    fun reverse(routeId: Int) {
        viewModelScope.launch {
            trackRepository.reverse(routeId)
        }
    }

    fun sendTrack(name: String, routeId: Int) = trackRepository.sendTrack(
        name,
        routeId
    )

    fun exportTrack(name: String, routeId: Int) = trackRepository.saveGpxFile(
        name,
        routeId
    )

    fun getOfflineEnable() = settingsRepository.getMapsOfflineEnable()

    fun setMapsOfflineEnable(enable: Boolean) = settingsRepository.setMapsOfflineEnable(enable)

    suspend fun getPresetSelected() = presetRepository.getPresetSelected()

    fun onPoiOnMapChanged(typePoiOnMaps: List<Type>, latLngBounds: LatLngBounds) {
        searchPlaceOnMap.update {
            SearchPlaceOnMap(
                searchArea = latLngBounds,
                typePlaceOnMaps = typePoiOnMaps
            )
        }
    }

    fun onCameraMoveEnded(latLngBounds: LatLngBounds) {
        searchPlaceOnMapOld.let { oldSearch ->
            if (oldSearch == null ||
                oldSearch.searchArea.northWest.distanceTo(latLngBounds.northWest) >= DISTANCE_MAX_IN_METER
            ) {
                searchPlaceOnMap.update {
                    SearchPlaceOnMap(
                        searchArea = latLngBounds,
                        typePlaceOnMaps = it?.typePlaceOnMaps
                            ?: settingsRepository.getDisplayPlace()
                    )
                }
            }
        }
    }

    fun resetLoadFile() {
        trackRepository.resetLoadFile()
    }
}
