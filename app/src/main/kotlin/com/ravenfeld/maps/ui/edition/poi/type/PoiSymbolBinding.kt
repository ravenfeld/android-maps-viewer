package com.ravenfeld.maps.ui.edition.poi.type

import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ravenfeld.maps.R
import com.ravenfeld.maps.ui.common.ItemDecoration

@BindingAdapter("items", "presenter")
fun RecyclerView.bindItemPoiSymbolUiModel(
    items: List<ItemPoiSymbolUiModel>?,
    presenter: PoiSymbolPresenter
) {
    items?.let {
        val adapter = getOrCreateAdapter(this, presenter)
        adapter.submitList(items)
    }
}

private fun getOrCreateAdapter(
    recyclerView: RecyclerView,
    presenter: PoiSymbolPresenter
): ListPoiSymbolAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ListPoiSymbolAdapter) {
        recyclerView.adapter as ListPoiSymbolAdapter
    } else {
        val bindableRecyclerAdapter = ListPoiSymbolAdapter(presenter)
        recyclerView.adapter = bindableRecyclerAdapter
        ContextCompat.getDrawable(recyclerView.context, R.drawable.divider)?.let {
            recyclerView.addItemDecoration(
                ItemDecoration(it, false)
            )
        }
        bindableRecyclerAdapter
    }
}

@BindingAdapter("poiSymbolSelected")
fun View.setPoiSymbolSelected(selected: Boolean) {
    if (selected) {
        setBackgroundColor(ContextCompat.getColor(context, R.color.grey))
    } else {
        background = null
    }
}
