package com.ravenfeld.maps.ui.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ravenfeld.maps.data.utils.safeLet
import com.ravenfeld.maps.domain.map.MapBase
import com.ravenfeld.maps.domain.map.MapRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MapFilterViewModel @Inject constructor(private val mapRepository: MapRepository) :
    ViewModel() {

    private val stateFlow = MutableStateFlow<MapsFilterUiModel?>(null)
    private var oldUiModel: MapsFilterUiModel? = null
    private var oldMaps: List<MapBase>? = null

    fun maps(stream: Boolean): Flow<MapsFilterUiModel> {
        viewModelScope.launch {
            mapRepository.getListMapGroupName(stream).collect { maps ->
                oldMaps = maps
                oldUiModel = maps.toListMapsFilterUiModel(stream, oldUiModel?.listUiModel)
                stateFlow.update { oldUiModel }
            }
        }
        return stateFlow.filterNotNull()
    }

    fun expandedCollapsed(name: String) {
        safeLet(oldUiModel, oldMaps) { oldUiModelSafe, oldMaps ->
            val group = oldUiModelSafe.listUiModel.find { name == it.name } as ItemMapGroupFilterUiModel
            group.expand = !group.expand
            stateFlow.update { oldMaps.toListMapsFilterUiModel(oldUiModelSafe.stream, oldUiModelSafe.listUiModel) }
        }
    }
}
