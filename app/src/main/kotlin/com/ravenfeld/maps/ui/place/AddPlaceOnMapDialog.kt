package com.ravenfeld.maps.ui.place

import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ravenfeld.maps.R
import com.ravenfeld.maps.databinding.DialogAddPlaceOnMapBinding
import com.ravenfeld.maps.ui.common.getOrThrow
import com.ravenfeld.maps.ui.maps.toType
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddPlaceOnMapDialog : AppCompatDialogFragment(), AddPlaceOnMapPresenter {
    private var _binding: DialogAddPlaceOnMapBinding? = null
    private val binding by lazy {
        _binding.getOrThrow()
    }

    private val viewModel: AddPlaceOnMapViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogAddPlaceOnMapBinding.inflate(inflater, container, false)
        dialog?.let {
            it.requestWindowFeature(Window.FEATURE_NO_TITLE)
            it.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.presenter = this
        viewModel.uiModel.let {
            binding.uiModel = it
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            @Suppress("deprecation")
            val display = requireActivity().windowManager.defaultDisplay
            val size = Point()
            @Suppress("deprecation")
            display.getSize(size)
            val params = it.attributes
            val widthMax = resources.getDimensionPixelSize(R.dimen.max_width)

            @Suppress("MagicNumber")
            val width = (size.x * 0.9).toInt()
            params.width = if (width < widthMax) {
                width
            } else {
                widthMax
            }
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            it.attributes = params as android.view.WindowManager.LayoutParams
        }
    }

    override fun onAddPoi() {
        val places = binding.uiModel?.listUiModel?.filter { it.checked }
            ?.map { it.placeOnMapUiModel }?.toType() ?: emptyList()
        viewModel.setDisplayPlace(places)
        findNavController().previousBackStackEntry?.savedStateHandle?.set(
            STATE_POI_ON_MAP_SELECTED,
            places
        )
    }

    companion object {
        const val STATE_POI_ON_MAP_SELECTED = "poi_on_map_selected"
    }
}
