package com.ravenfeld.maps.domain.geo

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.ravenfeld.maps.R

enum class PoiSymbol(
    @DrawableRes val iconId: Int,
    @DrawableRes val iconMapId: Int,
    @StringRes val textId: Int,
) {
    GENERIC(
        iconId = R.drawable.waypoint_generic_fill,
        iconMapId = R.drawable.waypoint_generic,
        textId = R.string.waypoint_generic
    ),
    SUMMIT(
        iconId = R.drawable.waypoint_peak_fill,
        R.drawable.waypoint_peak,
        textId = R.string.waypoint_summit
    ),
    VALLEY(
        iconId = R.drawable.waypoint_valley_fill,
        iconMapId = R.drawable.waypoint_valley,
        textId = R.string.waypoint_valley
    ),
    WATER(
        iconId = R.drawable.waypoint_waterpoint_fill,
        iconMapId = R.drawable.waypoint_waterpoint,
        textId = R.string.waypoint_water
    ),
    FOOD(
        iconId = R.drawable.waypoint_restaurant_fill,
        iconMapId = R.drawable.waypoint_restaurant,
        textId = R.string.waypoint_food
    ),
    HOUSE(
        iconId = R.drawable.waypoint_house_fill,
        iconMapId = R.drawable.waypoint_house,
        textId = R.string.waypoint_house
    ),
    DANGER(
        iconId = R.drawable.waypoint_danger_fill,
        iconMapId = R.drawable.waypoint_danger,
        textId = R.string.waypoint_danger
    ),
    FIRST_AID(
        iconId = R.drawable.waypoint_emergency_fill,
        iconMapId = R.drawable.waypoint_emergency,
        textId = R.string.waypoint_emergency
    ),
    SEGMENT_START(
        iconId = R.drawable.waypoint_begin_fill,
        iconMapId = R.drawable.waypoint_begin,
        textId = R.string.waypoint_begin
    ),
    SEGMENT_END(
        iconId = R.drawable.waypoint_end_fill,
        iconMapId = R.drawable.waypoint_end,
        textId = R.string.waypoint_end
    ),
    TURN_RIGHT(
        iconId = R.drawable.waypoint_turn_right_fill,
        iconMapId = R.drawable.waypoint_turn_right,
        textId = R.string.waypoint_turn_right
    ),
    TURN_LEFT(
        iconId = R.drawable.waypoint_turn_left_fill,
        iconMapId = R.drawable.waypoint_turn_left,
        textId = R.string.waypoint_turn_left
    ),
    STRAIGHT(
        iconId = R.drawable.waypoint_straight_fill,
        iconMapId = R.drawable.waypoint_straight,
        textId = R.string.waypoint_straight
    ),
}
