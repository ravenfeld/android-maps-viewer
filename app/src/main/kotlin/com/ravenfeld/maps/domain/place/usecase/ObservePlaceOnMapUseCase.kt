package com.ravenfeld.maps.domain.place.usecase

import com.ravenfeld.maps.data.remote.NetworkHandler
import com.ravenfeld.maps.domain.place.model.PlaceOnMap
import com.ravenfeld.maps.domain.place.model.SearchPlaceOnMap
import com.ravenfeld.maps.domain.place.repository.PlaceOnMapRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val MAX_WIDTH_IN_METER = 10_000

class ObservePoiOnMapUseCase @Inject constructor(
    private val placeOnMapRepository: PlaceOnMapRepository,
    private val networkHandler: NetworkHandler
) {

    @OptIn(ExperimentalCoroutinesApi::class)
    operator fun invoke(
        oldSearch: SearchPlaceOnMap?,
        newSearch: SearchPlaceOnMap
    ): Flow<PlaceOnMapResult> =
        networkHandler.flowNetworkAvailableFlow()
            .flatMapLatest { networkAvailable ->
                flow {
                    if (networkAvailable) {
                        if (newSearch.typePlaceOnMaps.isEmpty()) {
                            emit(
                                PlaceOnMapResult.Retrieved(
                                    emptyList(),
                                    newSearch
                                )
                            )
                        } else if (
                            newSearch.isNotSame(oldSearch) &&
                            newSearch.tooLarge()
                        ) {
                            emit(PlaceOnMapResult.Loading(searchPlaceOnMap = newSearch))
                            val processedList = coroutineScope {
                                newSearch.typePlaceOnMaps.map {
                                    async(
                                        Dispatchers.IO
                                    ) {
                                        placeOnMapRepository.getPlaceOnMap(
                                            type = it,
                                            area = newSearch.searchArea
                                        )
                                    }
                                }.awaitAll()
                            }
                            val joined = ArrayList<PlaceOnMap>()
                            processedList.forEach {
                                joined.addAll(it)
                            }
                            emit(
                                PlaceOnMapResult.Retrieved(
                                    joined,
                                    newSearch
                                )
                            )
                        }
                    } else {
                        emit(PlaceOnMapResult.NoInternet)
                    }
                }
            }

    private fun SearchPlaceOnMap.tooLarge() =
        searchArea.northEast.distanceTo(searchArea.northWest) <= MAX_WIDTH_IN_METER

    private fun SearchPlaceOnMap.isNotSame(oldSearch: SearchPlaceOnMap?): Boolean =
        oldSearch == null ||
            !oldSearch.searchArea.contains(this.searchArea) ||
            oldSearch.typePlaceOnMaps != this.typePlaceOnMaps
}

sealed interface PlaceOnMapResult {
    data class Loading(val searchPlaceOnMap: SearchPlaceOnMap) : PlaceOnMapResult
    data object NoInternet : PlaceOnMapResult
    data class Retrieved(
        val placeOnMaps: List<PlaceOnMap>,
        val searchPlaceOnMap: SearchPlaceOnMap
    ) : PlaceOnMapResult
}
