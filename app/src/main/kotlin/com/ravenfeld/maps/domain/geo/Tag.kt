package com.ravenfeld.maps.domain.geo

import kotlinx.serialization.Serializable

@Serializable
data class Tag(
    val highway: Highway = Highway.Unknown,
    val surface: Surface = Surface.Unknown,
)

enum class Highway {
    AlpineMountainPath,
    HikingPath,
    Path,
    Steps,
    Cycleway,
    Footway,
    Track,
    Street,
    Road,
    Highway,
    Unknown
}

enum class Surface {
    Natural,
    Compacted,
    Cobblestone,
    Tarmac,
    Asphalt,
    Unknown
}
