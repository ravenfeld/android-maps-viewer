package com.ravenfeld.maps.domain.map

enum class DisplayInfo {
    NONE, TRACK, LOCALISATION
}
