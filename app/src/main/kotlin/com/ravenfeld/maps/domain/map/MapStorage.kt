package com.ravenfeld.maps.domain.map

enum class MapStorage {
    MBTILES_RASTER, MBTILES_VECTOR, FILE, STREAM, GROUP
}
