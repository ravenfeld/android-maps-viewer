package com.ravenfeld.maps.domain.place.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
enum class Type : Parcelable {
    PEAK,
    SADDLE,
    ALPINE_HUT,
    WILDERNESS_HUT,
    WATER,
    PARKING,
    PARKING_BICYCLE,
    TOILET,
    SHOP,
    CAMP_SITE
}
