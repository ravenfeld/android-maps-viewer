package com.ravenfeld.maps.domain.map

data class MapInput(
    val name: String,
    val path: String,
    val minZoom: Float,
    val maxZoom: Float
)
