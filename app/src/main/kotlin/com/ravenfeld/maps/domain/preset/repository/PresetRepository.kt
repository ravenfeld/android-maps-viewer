package com.ravenfeld.maps.domain.preset.repository

import com.ravenfeld.maps.domain.preset.model.Preset
import kotlinx.coroutines.flow.Flow

interface PresetRepository {
    fun getPresets(): Flow<List<Preset>>
    suspend fun select(presetId: Int)
    suspend fun remove(presetId: Int)
    suspend fun rename(
        presetId: Int,
        name: String
    )
    suspend fun insert(
        name: String
    )
    suspend fun getPresetSelected(): Preset
}
