package com.ravenfeld.maps.domain.map

open class MapBase(
    val name: String,
    val storage: MapStorage
)
