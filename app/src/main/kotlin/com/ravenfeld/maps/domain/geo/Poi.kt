package com.ravenfeld.maps.domain.geo

import kotlinx.serialization.Serializable

@Serializable
data class Poi(
    val name: String,
    val point: GeoPoint,
    val symbol: PoiSymbol
)
