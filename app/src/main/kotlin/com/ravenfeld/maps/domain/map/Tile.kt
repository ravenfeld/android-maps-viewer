package com.ravenfeld.maps.domain.map

import org.maplibre.android.geometry.LatLng
import kotlin.math.PI
import kotlin.math.asinh
import kotlin.math.atan
import kotlin.math.floor
import kotlin.math.pow
import kotlin.math.sinh
import kotlin.math.tan

const val MAX_TILES = 25_000_000
const val SIZE_TILE = 45_000L

fun getNbTiles(area: Array<LatLng>, zooms: IntArray): Int {
    val northWest = doubleArrayOf(area[0].latitude, area[0].longitude)
    val southEst = doubleArrayOf(area[2].latitude, area[2].longitude)
    var nbTiles = 0

    var zoom = zooms[0]
    while (zoom in zooms[0]..zooms[1] && nbTiles < MAX_TILES) {
        val minXtile = getXYTile(northWest[0], northWest[1], zoom).x
        val maxXtile = getXYTile(southEst[0], southEst[1], zoom).x

        val minYtile = getXYTile(northWest[0], northWest[1], zoom).y
        val maxYtile = getXYTile(southEst[0], southEst[1], zoom).y

        var x = minXtile
        while (x in minXtile..maxXtile && nbTiles < MAX_TILES) {
            var y = minYtile
            while (y in minYtile..maxYtile && nbTiles < MAX_TILES) {
                nbTiles++
                y++
            }
            x++
        }
        zoom++
    }
    return nbTiles
}

fun getTiles(
    northWest: DoubleArray,
    southEst: DoubleArray,
    zooms: IntArray
): Set<TileCoordinate> {
    val listTileBox = HashSet<TileCoordinate>()

    for (zoom in zooms[0]..zooms[1]) {
        val minXtile = getXYTile(northWest[0], northWest[1], zoom).x
        val maxXtile = getXYTile(southEst[0], southEst[1], zoom).x

        val minYtile = getXYTile(northWest[0], northWest[1], zoom).y
        val maxYtile = getXYTile(southEst[0], southEst[1], zoom).y

        for (x in minXtile..maxXtile) {
            for (y in minYtile..maxYtile) {
                listTileBox.add(TileCoordinate(x, y, zoom))
            }
        }
    }

    return listTileBox
}

@Suppress("MagicNumber")
fun getXYTile(lat: Double, lon: Double, zoom: Int): TileCoordinate {
    val latRad = Math.toRadians(lat)
    var xtile = floor((lon + 180) / 360 * (1 shl zoom)).toInt()
    var ytile = floor((1.0 - asinh(tan(latRad)) / PI) / 2 * (1 shl zoom)).toInt()

    if (xtile < 0) {
        xtile = 0
    }
    if (xtile >= 1 shl zoom) {
        xtile = (1 shl zoom) - 1
    }
    if (ytile < 0) {
        ytile = 0
    }
    if (ytile >= 1 shl zoom) {
        ytile = (1 shl zoom) - 1
    }
    return TileCoordinate(xtile, ytile, zoom)
}

@Suppress("MagicNumber")
fun tileToLon(x: Int, z: Int): Double =
    x / 2.0.pow(z.toDouble()) * 360.0 - 180

@Suppress("MagicNumber")
fun tileToLat(y: Int, z: Int): Double {
    val n = Math.PI - 2.0 * Math.PI * y / 2.0.pow(z.toDouble())
    return Math.toDegrees(atan(sinh(n)))
}

data class TileCoordinate(val x: Int, val y: Int, val z: Int)
