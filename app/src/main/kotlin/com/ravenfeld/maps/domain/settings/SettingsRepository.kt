package com.ravenfeld.maps.domain.settings

import com.ravenfeld.maps.domain.place.model.Type
import com.ravenfeld.maps.domain.routing.RoutingMode
import kotlinx.coroutines.flow.Flow

interface SettingsRepository {
    fun getRoutingMode(): RoutingMode
    fun getFlowRoutingMode(): Flow<RoutingMode>
    fun setRoutingMode(routingMode: RoutingMode)
    fun getMapsOfflineEnable(): Boolean
    fun setMapsOfflineEnable(enable: Boolean)
    fun getFlowMapsOfflineEnable(): Flow<Boolean>
    fun getDisplayPlace(): List<Type>
    fun setDisplayPlace(displayTypes: List<Type>)
}
