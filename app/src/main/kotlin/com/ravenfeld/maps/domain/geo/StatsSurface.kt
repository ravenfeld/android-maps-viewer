package com.ravenfeld.maps.domain.geo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class StatsSurface(
    val type: Surface,
    val distance: Double,
    val percent: Int
) : Parcelable
