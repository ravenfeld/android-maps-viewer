package com.ravenfeld.maps.domain.geo

import com.ravenfeld.maps.data.mapper.toPoint
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.maplibre.geojson.Point
import org.maplibre.turf.TurfConstants
import org.maplibre.turf.TurfMeasurement
import org.maplibre.turf.TurfMisc

@Serializable
data class TrackPoint(
    @SerialName("geoPoint")
    val geoPoint: GeoPoint,
    @SerialName("way")
    val way: Way? = null
) {
    override fun toString(): String = "$geoPoint way: $way"
}

internal fun TrackPoint.toPoint() =
    geoPoint.toPoint()

internal fun List<TrackPoint>.toPoint() =
    map(TrackPoint::toPoint)

internal fun List<TrackPoint>.length() =
    TurfMeasurement.length(this.toPoint(), TurfConstants.UNIT_METERS)

internal fun List<TrackPoint>.sortWay(): List<TrackPoint> {
    return if (all { it.way != null }) {
        val ways = mutableListOf<Way>()
        var distance = 0.0
        forEachIndexed { index, trackPoint ->
            trackPoint.way?.let { way ->
                if (index == 0) {
                    ways.add(way)
                } else {
                    distance += distanceBetween(get(index - 1), trackPoint)
                    val lastWay = ways.last()
                    if (way.tag == lastWay.tag) {
                        ways[ways.lastIndex] = lastWay.copy(
                            end = way.end,
                            endDistance = distance
                        )
                    } else {
                        ways[ways.lastIndex] = lastWay.copy(
                            end = way.start,
                            endDistance = distance
                        )
                        ways.add(
                            way.copy(
                                startDistance = distance
                            )
                        )
                    }
                }
            }
        }

        map { it.geoPoint }.addWay(ways)
    } else {
        this
    }
}

internal fun List<GeoPoint>.addWay(ways: List<Way>): List<TrackPoint> {
    var indexWay = 0
    return map { geoPoint ->
        if (
            indexWay < ways.size - 1 &&
            geoPoint.isSamePosition(ways[indexWay].end)
        ) {
            indexWay++
        }
        TrackPoint(
            geoPoint = geoPoint,
            way = ways[indexWay]
        )
    }
}

fun distanceBetween(startTrackPoint: TrackPoint, endTrackPoint: TrackPoint) =
    TurfMeasurement.distance(
        startTrackPoint.toPoint(),
        endTrackPoint.toPoint(),
        TurfConstants.UNIT_METERS
    )

fun List<TrackPoint>.nearestTrackPointOnLine(point: GeoPoint): TrackPoint {
    val pointOnLine = TurfMisc.nearestPointOnLine(
        point.toPoint(),
        map { it.toPoint() },
        TurfConstants.UNIT_METERS
    ).geometry() as Point
    var trackPointResult: TrackPoint = this.first()

    var distance = Double.MAX_VALUE
    this.forEach {
        val distanceWithTrackPoint = TurfMeasurement.distance(
            pointOnLine,
            it.geoPoint.toPoint(),
            TurfConstants.UNIT_METERS
        )
        if (distanceWithTrackPoint < distance) {
            distance = distanceWithTrackPoint
            trackPointResult = it
        }
    }
    return TrackPoint(
        geoPoint = pointOnLine.toGeoPoint(),
        way = trackPointResult.way
    )
}
