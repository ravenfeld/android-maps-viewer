package com.ravenfeld.maps.domain.map

class MapGroup(
    name: String,
    storage: MapStorage,
    var enable: Boolean,
    val listMap: MutableList<Map> = ArrayList()
) : MapBase(name, storage)
