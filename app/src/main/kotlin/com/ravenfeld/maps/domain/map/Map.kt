package com.ravenfeld.maps.domain.map

import org.maplibre.android.style.layers.PropertyValue
import org.maplibre.android.style.layers.RasterLayer
import org.maplibre.android.style.sources.RasterSource
import org.maplibre.android.style.sources.TileSet
import org.maplibre.android.style.sources.VectorSource
import java.util.concurrent.TimeUnit

private const val TILE_SIZE = 128
private const val FADE_DURATION = 100
private const val MAX_ZOOM_VECTOR = 14f
const val MIN_ZOOM = 0f
const val MAX_ZOOM = 22f

@Suppress("LongParameterList")
class Map(
    name: String,
    val url: String,
    storage: MapStorage,
    val path: String,
    val enable: Boolean,
    val order: Int,
    val visible: Boolean,
    val alpha: Float,
    val minZoom: Float,
    val maxZoom: Float
) : MapBase(name, storage) {

    fun toRasterSource(): RasterSource {
        val tileSet = TileSet(name, path)
        tileSet.minZoom = minZoom
        tileSet.maxZoom = maxZoom
        val tileSize = TILE_SIZE
        return RasterSource(
            name,
            tileSet,
            tileSize
        ).apply {
            if (storage == MapStorage.STREAM) {
                @Suppress("MagicNumber")
                minimumTileUpdateInterval = TimeUnit.HOURS.toMillis(24)
            }
        }
    }

    fun toVectorSource() = VectorSource(
        "map_vector",
        TileSet(name, path).apply {
            maxZoom = MAX_ZOOM_VECTOR
        }
    )

    fun toRasterLayer() = RasterLayer(name, name)
        .withProperties(
            PropertyValue("raster-fade-duration", FADE_DURATION),
            PropertyValue("raster-opacity", alpha),
            PropertyValue(
                "visibility",
                if (visible) {
                    "visible"
                } else {
                    "none"
                }
            )
        )
}
