package com.ravenfeld.maps.domain.geo

import kotlinx.serialization.Serializable

@Serializable
data class Way(
    val start: GeoPoint,
    val end: GeoPoint,
    val startDistance: Double,
    val endDistance: Double,
    val tag: Tag
)
