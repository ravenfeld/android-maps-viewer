package com.ravenfeld.maps.domain.track

import android.os.Parcelable
import com.ravenfeld.maps.ui.track.export.ExportType
import kotlinx.parcelize.Parcelize

@Parcelize
data class ExportTrack(
    val name: String,
    val type: ExportType
) : Parcelable
