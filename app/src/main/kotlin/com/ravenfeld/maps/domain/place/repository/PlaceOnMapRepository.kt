package com.ravenfeld.maps.domain.place.repository

import com.ravenfeld.maps.domain.place.model.PlaceOnMap
import com.ravenfeld.maps.domain.place.model.Type
import org.maplibre.android.geometry.LatLngBounds

interface PlaceOnMapRepository {
    suspend fun getPlaceOnMap(type: Type, area: LatLngBounds): List<PlaceOnMap>
}
