package com.ravenfeld.maps.domain.geo

import kotlin.math.abs
import kotlin.math.max
import kotlin.math.round

private const val SMOOTH = 100

data class TrackStats(
    val measurement: TrackMeasurement,
    val chartElevation: List<ChartElevationPoint>,
    val statsHighway: List<StatsHighway>,
    val statsSurface: List<StatsSurface>
)

data class TrackMeasurement(
    val distance: Double,
    val upHill: Double,
    val downHill: Double,
)

fun List<TrackPoint>.toTrackStats(): TrackStats {
    var lastTrackPoint: TrackPoint? = null
    var distance = 0.0
    val pointsElevation = mutableListOf<ChartElevationPoint>()
    val highwayDistance = mutableMapOf<Highway, Double>()
    val surfaceDistance = mutableMapOf<Surface, Double>()
    val measurement = toTrackMeasurement { trackPoint, elevation ->
        val delta = lastTrackPoint?.let {
            distanceBetween(it, trackPoint)
        } ?: 0.0

        distance += delta

        lastTrackPoint?.way?.tag?.let {
            highwayDistance.merge(it.highway, delta, Double::plus)
            surfaceDistance.merge(it.surface, delta, Double::plus)
        }
        lastTrackPoint = trackPoint

        pointsElevation.add(ChartElevationPoint(distance, elevation))
    }

    val highwayPercent = highwayDistance.convertToPercent()
    val surfacePercent = surfaceDistance.convertToPercent()

    return TrackStats(
        measurement = measurement,
        chartElevation = pointsElevation,
        statsHighway = highwayDistance.map {
            StatsHighway(
                type = it.key,
                distance = it.value,
                percent = highwayPercent.getOrDefault(it.key, 0)
            )
        },
        statsSurface = surfaceDistance.map {
            StatsSurface(
                type = it.key,
                distance = it.value,
                percent = surfacePercent.getOrDefault(it.key, 0)
            )
        }
    )
}

fun <K> Map<K, Double>.convertToPercent(): Map<K, Int> {
    val map = mutableMapOf<K, Int>()
    val sum = values.sum()
    if (sum == 0.0) {
        keys.forEach {
            map[it] = 0
        }
        return map
    }
    val percentages = values.map { max(1, round(it / sum * 100).toInt()) }.toMutableList()

    val totalPercentage = percentages.sum()
    var difference = 100 - totalPercentage
    if (difference > 2 || difference < 0) {
        val sortedIndices = if (difference > 0) {
            percentages.indices.sortedBy { percentages[it] }
        } else {
            percentages.indices.sortedByDescending { percentages[it] }
        }

        for (index in sortedIndices) {
            val adjustment = if (difference > 0) 1 else -1
            percentages[index] += adjustment
            difference -= adjustment
            if (difference == 0) break
        }
    }

    keys.forEachIndexed { index, k ->
        map[k] = percentages[index]
    }
    return map
}

@Suppress("MagicNumber", "CyclomaticComplexMethod")
fun List<TrackPoint>.toTrackMeasurement(
    onAddElevation: ((trackPoint: TrackPoint, elevation: Double) -> Unit)? = null
): TrackMeasurement {
    var upHill = 0.0
    var downHill = 0.0
    var previousEle: Double? = null
    var start = 0
    var end = -1
    var cumul = 0.0

    this.forEachIndexed { index, coordinate ->
        while (start < index && distanceBetween(
                this[start].geoPoint,
                coordinate.geoPoint
            ) > SMOOTH
        ) {
            cumul -= this[start].geoPoint.altitude ?: 0.0
            start++
        }

        while (end + 1 < this.size && distanceBetween(
                coordinate.geoPoint,
                this[end + 1].geoPoint
            ) <= SMOOTH
        ) {
            cumul += this[end + 1].geoPoint.altitude ?: 0.0
            end++
        }

        val elevation = if (index == this.size - 1 || index == 0) {
            coordinate.geoPoint.altitude ?: 0.0
        } else {
            cumul / (end - start + 1)
        }

        previousEle?.let {
            val ele = elevation - it
            if (ele > 0) {
                upHill += ele
            } else {
                downHill += ele
            }
            previousEle = elevation
        } ?: run {
            previousEle = elevation
        }
        onAddElevation?.invoke(coordinate, elevation)
    }
    if (size >= 2 && distanceBetween(
            first().geoPoint,
            last().geoPoint
        ) <= 10
    ) {
        upHill = downHill
    }
    return TrackMeasurement(
        distance = length(),
        upHill = abs(upHill),
        downHill = abs(downHill)
    )
}
