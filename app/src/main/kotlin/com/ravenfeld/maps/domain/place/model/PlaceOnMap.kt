package com.ravenfeld.maps.domain.place.model

import org.maplibre.android.geometry.LatLng

data class PlaceOnMap(
    val center: LatLng,
    val type: Type,
    val text: String?
)
