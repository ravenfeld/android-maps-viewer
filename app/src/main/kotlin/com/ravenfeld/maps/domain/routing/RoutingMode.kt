package com.ravenfeld.maps.domain.routing

enum class RoutingMode {
    FREE, HIKE, MOUNTAIN_BIKE, ROAD_BIKE
}
