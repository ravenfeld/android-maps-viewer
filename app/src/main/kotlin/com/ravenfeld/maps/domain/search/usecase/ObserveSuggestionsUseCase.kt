package com.ravenfeld.maps.domain.search.usecase

import com.ravenfeld.maps.data.remote.NetworkHandler
import com.ravenfeld.maps.domain.search.model.Suggestion
import com.ravenfeld.maps.domain.search.repository.SearchRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import org.maplibre.android.geometry.LatLng
import javax.inject.Inject

class ObserveSuggestionsUseCase @Inject constructor(
    private val searchRepository: SearchRepository,
    private val networkHandler: NetworkHandler
) {
    @OptIn(ExperimentalCoroutinesApi::class)
    suspend operator fun invoke(
        query: String,
        locationUser: LatLng?
    ): Flow<SuggestionsResult> =
        networkHandler.flowNetworkAvailableFlow()
            .flatMapLatest { networkAvailable ->
                flow {
                    if (networkAvailable) {
                        if (query.isEmpty()) {
                            emit(SuggestionsResult.SearchIsEmpty)
                        } else if (query.length <= 2) {
                            emit(SuggestionsResult.SearchIsTooSmall)
                        } else {
                            emit(SuggestionsResult.Loading)
                            emit(
                                SuggestionsResult.Retrieved(
                                    searchRepository.getSuggestions(
                                        query = query,
                                        locationUser = locationUser
                                    )
                                )
                            )
                        }
                    } else {
                        emit(SuggestionsResult.NoInternet)
                    }
                }
            }
}

sealed interface SuggestionsResult {
    data object NoInternet : SuggestionsResult
    data object Loading : SuggestionsResult
    data class Retrieved(val suggestions: List<Suggestion>) : SuggestionsResult
    data object SearchIsTooSmall : SuggestionsResult
    data object SearchIsEmpty : SuggestionsResult
}
