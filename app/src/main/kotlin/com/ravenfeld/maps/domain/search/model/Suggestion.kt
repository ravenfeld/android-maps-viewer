package com.ravenfeld.maps.domain.search.model

import org.maplibre.android.geometry.LatLng
import org.maplibre.android.geometry.LatLngBounds

data class Suggestion(
    val id: Long,
    val name: String?,
    val center: LatLng,
    val bounds: LatLngBounds?,
    val houseNumber: String?,
    val street: String?,
    val city: String?,
    val state: String?,
    val country: String,
    val type: Type
)
