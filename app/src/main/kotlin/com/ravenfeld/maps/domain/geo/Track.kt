package com.ravenfeld.maps.domain.geo

import android.util.Xml
import com.garmin.fit.CourseMesg
import com.garmin.fit.CoursePoint
import com.garmin.fit.CoursePointMesg
import com.garmin.fit.DateTime
import com.garmin.fit.Event
import com.garmin.fit.EventMesg
import com.garmin.fit.EventType
import com.garmin.fit.Field
import com.garmin.fit.FileCreatorMesg
import com.garmin.fit.FileEncoder
import com.garmin.fit.FileIdMesg
import com.garmin.fit.Fit
import com.garmin.fit.LapMesg
import com.garmin.fit.Manufacturer
import com.garmin.fit.Profile
import com.garmin.fit.RecordMesg
import com.garmin.fit.Sport
import org.maplibre.geojson.LineString
import org.maplibre.geojson.Point
import org.maplibre.turf.TurfConstants
import org.maplibre.turf.TurfMeasurement
import org.maplibre.turf.TurfMisc
import java.io.File
import java.io.OutputStream
import java.lang.reflect.Constructor
import java.lang.reflect.InvocationTargetException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs

class Track(
    val id: Int,
    val name: String,
    val visible: Boolean,
    val color: String,
    val coordinate: Coordinate,
    val updated: Date
) {
    val locationNorthEast: GeoPoint
    val locationSouthWest: GeoPoint
    val stats: TrackStats
    val trackPoints: List<TrackPoint> = coordinate.segments.flatten().sortWay()

    init {
        stats = this.trackPoints.toTrackStats()

        if (trackPoints.isNotEmpty()) {
            this.locationNorthEast = GeoPoint(
                trackPoints.maxOf { it.geoPoint.latitude },
                trackPoints.maxOf { it.geoPoint.longitude }
            )
            this.locationSouthWest = GeoPoint(
                trackPoints.minOf { it.geoPoint.latitude },
                trackPoints.minOf { it.geoPoint.longitude }
            )
        } else {
            this.locationNorthEast = GeoPoint(0.0, 0.0)
            this.locationSouthWest = GeoPoint(0.0, 0.0)
        }
    }

    @Suppress("MagicNumber", "SwallowedException", "EmptyCatchBlock", "ComplexMethod", "LongMethod")
    fun writeFit(name: String, outfile: File) {
        val timeNow = Calendar.getInstance().timeInMillis
        val minEle = trackPoints.minOf { it.geoPoint.altitude ?: 0.0 }
        val maxEle = trackPoints.maxOf { it.geoPoint.altitude ?: 0.0 }
        val maxLat = trackPoints.maxOf { it.geoPoint.latitude }
        val minLat = trackPoints.minOf { it.geoPoint.latitude }
        val maxLon = trackPoints.maxOf { it.geoPoint.longitude }
        val minLon = trackPoints.minOf { it.geoPoint.longitude }

        val encode = FileEncoder(outfile, Fit.ProtocolVersion.V2_0)

        // Generate FileIdMessage
        // Every FIT file MUST contain a 'File ID' message as the first message
        val fileIdMesg = FileIdMesg().apply {
            serialNumber = 93_688_566L
            timeCreated = DateTime(Date())
            manufacturer = Manufacturer.GARMIN
            product = 65_534
            number = 1
            type = com.garmin.fit.File.COURSE
        }

        encode.write(fileIdMesg) // Encode the FileIDMesg

        val fileCreatorMesg = FileCreatorMesg().apply {
            hardwareVersion = 0
        }

        encode.write(fileCreatorMesg)

        val courseMesg = CourseMesg().apply {
            this.name = name
            localNum = 0
            sport = Sport.GENERIC
        }

        encode.write(courseMesg)

        val firstWayPoint = trackPoints.first().geoPoint
        val startDate = Date(timeNow)
        val lastWayPoint = trackPoints.last().geoPoint
        val distance = stats.measurement.distance

        val endDate = Date(timeNow + distance.toLong() * 1000)

        val lapMesg = LapMesg()
        lapMesg.localNum = 0
        lapMesg.timestamp = DateTime(startDate)
        lapMesg.startTime = DateTime(startDate)
        lapMesg.startPositionLat = firstWayPoint.latitude.toSemiCircles()
        lapMesg.startPositionLong = firstWayPoint.longitude.toSemiCircles()
        lapMesg.endPositionLat = lastWayPoint.latitude.toSemiCircles()
        lapMesg.endPositionLong = lastWayPoint.longitude.toSemiCircles()

        val duration = endDate.time - startDate.time
        lapMesg.totalTimerTime = TimeUnit.MILLISECONDS.toSeconds(duration).toFloat()
        lapMesg.totalDistance = distance.toFloat()
        lapMesg.avgSpeed = (distance * 1000.0 / duration.toDouble()).toFloat()
        lapMesg.totalElapsedTime = TimeUnit.MILLISECONDS.toSeconds(duration).toFloat()
        lapMesg.totalAscent = stats.measurement.upHill.toInt()
        lapMesg.totalDescent = abs(stats.measurement.downHill).toInt()

        if (maxEle > 0.0) {
            lapMesg.maxAltitude = maxEle.toFloat()
        }
        if (minEle > 0.0) {
            lapMesg.minAltitude = minEle.toFloat()
        }

        lapMesg.messageIndex = 0

        // Add the bounding box of the course in the undocumented fields
        try {
            val c: Constructor<Field> = Field::class.java.getDeclaredConstructor(
                String::class.java,
                Int::class.javaPrimitiveType,
                Int::class
                    .javaPrimitiveType,
                Double::class.javaPrimitiveType,
                Double::class.javaPrimitiveType,
                String::class.java,
                Boolean::class.javaPrimitiveType,
                Profile.Type::class.java
            )
            c.isAccessible = true
            @Suppress("StringLiteralDuplication")
            lapMesg.addField(
                c.newInstance(
                    "bound_max_position_lat",
                    27,
                    133,
                    1.0,
                    0.0,
                    "semicircles",
                    false,
                    Profile.Type.SINT32
                ) as Field
            )
            lapMesg.addField(
                c.newInstance(
                    "bound_max_position_long",
                    28,
                    133,
                    1.0,
                    0.0,
                    "semicircles",
                    false,
                    Profile.Type.SINT32
                ) as Field
            )
            lapMesg.addField(
                c.newInstance(
                    "bound_min_position_lat",
                    29,
                    133,
                    1.0,
                    0.0,
                    "semicircles",
                    false,
                    Profile.Type.SINT32
                ) as Field
            )
            lapMesg.addField(
                c.newInstance(
                    "bound_min_position_long",
                    30,
                    133,
                    1.0,
                    0.0,
                    "semicircles",
                    false,
                    Profile.Type.SINT32
                ) as Field
            )
            lapMesg.setFieldValue(27, 0, maxLat.toSemiCircles(), '\uffff'.code)
            lapMesg.setFieldValue(28, 0, maxLon.toSemiCircles(), '\uffff'.code)
            lapMesg.setFieldValue(29, 0, minLat.toSemiCircles(), '\uffff'.code)
            lapMesg.setFieldValue(30, 0, minLon.toSemiCircles(), '\uffff'.code)
        } catch (e: NoSuchMethodException) {
        } catch (e: IllegalAccessException) {
        } catch (e: InstantiationException) {
        } catch (e: InvocationTargetException) {
        }
        encode.write(lapMesg)

        val eventStartMesg = EventMesg()
        eventStartMesg.localNum = 0
        eventStartMesg.event = Event.TIMER
        eventStartMesg.eventType = EventType.START
        eventStartMesg.eventGroup = 0.toShort()
        eventStartMesg.timestamp = DateTime(startDate)
        encode.write(eventStartMesg)

        var lastPoint: GeoPoint? = null
        var distanceTotal = 0.0
        trackPoints.forEach { trackPoint ->
            val distance = lastPoint?.let {
                distanceBetween(it, trackPoint.geoPoint)
            } ?: run { 0.0 }
            distanceTotal += distance
            val r = RecordMesg()
            r.localNum = 0
            r.positionLat = trackPoint.geoPoint.latitude.toSemiCircles()
            r.positionLong = trackPoint.geoPoint.longitude.toSemiCircles()
            r.distance = distanceTotal.toFloat()
            r.timestamp = DateTime(Date(timeNow + distance.toLong() * 1000))
            if (trackPoint.geoPoint.altitude != null && trackPoint.geoPoint.altitude >= 0) {
                r.altitude = trackPoint.geoPoint.altitude.toFloat()
            } else {
                r.altitude = 0f
            }
            r.speed = 0.0.toFloat()
            encode.write(r)
            lastPoint = trackPoint.geoPoint
        }

        val eventStopMesg = EventMesg()
        eventStopMesg.localNum = 0
        eventStopMesg.event = Event.TIMER
        eventStopMesg.eventType = EventType.STOP_DISABLE_ALL
        eventStopMesg.eventGroup = 0.toShort()
        eventStopMesg.timestamp = DateTime(endDate)
        encode.write(eventStopMesg)

        coordinate.pois.map {
            val coursePoint = CoursePointMesg()
            coursePoint.localNum = 0
            coursePoint.positionLat = it.point.latitude.toSemiCircles()
            coursePoint.positionLong = it.point.longitude.toSemiCircles()
            coursePoint.name = it.name
            coursePoint.type = it.symbol.toCoursePoint()

            coursePoint.distance = TurfMeasurement.length(
                TurfMisc.lineSlice(
                    trackPoints.first()
                        .toPoint(),
                    it.point.toPoint(),
                    LineString.fromLngLats(
                        trackPoints.map {
                            it.toPoint()
                        }
                    )
                ),
                TurfConstants.UNIT_METERS
            ).toFloat()
            coursePoint
        }
            .sortedBy { it.distance }
            .forEach { coursePoint ->
                encode.write(coursePoint)
            }

        encode.close()
    }

    private fun GeoPoint.toPoint() = Point.fromLngLat(
        this.longitude,
        this.latitude,
        this.altitude
            ?: 0.0
    )

    private fun PoiSymbol.toCoursePoint() =
        when (this) {
            PoiSymbol.GENERIC -> CoursePoint.GENERIC
            PoiSymbol.HOUSE -> CoursePoint.GENERIC
            PoiSymbol.SUMMIT -> CoursePoint.SUMMIT
            PoiSymbol.VALLEY -> CoursePoint.VALLEY
            PoiSymbol.WATER -> CoursePoint.WATER
            PoiSymbol.FOOD -> CoursePoint.FOOD
            PoiSymbol.DANGER -> CoursePoint.DANGER
            PoiSymbol.FIRST_AID -> CoursePoint.FIRST_AID
            PoiSymbol.SEGMENT_START -> CoursePoint.SEGMENT_START
            PoiSymbol.SEGMENT_END -> CoursePoint.SEGMENT_END
            PoiSymbol.TURN_RIGHT -> CoursePoint.RIGHT
            PoiSymbol.TURN_LEFT -> CoursePoint.LEFT
            PoiSymbol.STRAIGHT -> CoursePoint.STRAIGHT
        }

    @Suppress("MagicNumber")
    fun writeGpx(name: String, outputStream: OutputStream) {
        val xmlSerializer = Xml.newSerializer()
        xmlSerializer.setOutput(outputStream, "UTF-8")
        xmlSerializer.startDocument("UTF-8", false)
        xmlSerializer.startTag(null, "gpx")
        xmlSerializer.attribute(null, "creator", "MapViewer")
        xmlSerializer.attribute(null, "version", "1.1")
        xmlSerializer.attribute(null, "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        xmlSerializer.attribute(null, "xmlns", "http://www.topografix.com/GPX/1/1")
        xmlSerializer.attribute(
            null,
            "xsi:schemaLocation",
            "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd"
        )
        coordinate.pois.forEach {
            xmlSerializer.startTag(null, "wpt")
            xmlSerializer.attribute(null, "lat", it.point.latitude.toString())
            xmlSerializer.attribute(null, "lon", it.point.longitude.toString())
            xmlSerializer.startTag(null, "name")
            xmlSerializer.text(it.name)
            xmlSerializer.endTag(null, "name")
            xmlSerializer.startTag(null, "type")
            xmlSerializer.text(it.symbol.name)
            xmlSerializer.endTag(null, "type")
            xmlSerializer.endTag(null, "wpt")
        }
        xmlSerializer.startTag(null, "trk")
        xmlSerializer.startTag(null, "name")
        xmlSerializer.text(name)
        xmlSerializer.endTag(null, "name")
        xmlSerializer.startTag(null, "trkseg")
        trackPoints.forEach { trackPoint ->
            xmlSerializer.startTag(null, "trkpt")
            xmlSerializer.attribute(null, "lat", trackPoint.geoPoint.latitude.toString())
            xmlSerializer.attribute(null, "lon", trackPoint.geoPoint.longitude.toString())
            if (trackPoint.geoPoint.altitude != null && trackPoint.geoPoint.altitude > 0) {
                xmlSerializer.startTag(null, "ele")
                xmlSerializer.text(trackPoint.geoPoint.altitude.toString())
                xmlSerializer.endTag(null, "ele")
            }
            xmlSerializer.endTag(null, "trkpt")
        }
        xmlSerializer.endTag(null, "trkseg")
        xmlSerializer.endTag(null, "trk")
        xmlSerializer.endTag(null, "gpx")
        xmlSerializer.endDocument()
        xmlSerializer.flush()
        outputStream.close()
    }

    /**
     * 2_147_483_648
     * it is the largest integer that can be represented on 32 signed bits
     */
    @Suppress("MagicNumber")
    private fun Double.toSemiCircles(): Int {
        val d = this * 2_147_483_648.0 / 180.0
        return d.toInt()
    }
}

data class ChartElevationPoint(val dist: Double, val elevation: Double)
