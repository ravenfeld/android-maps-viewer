package com.ravenfeld.maps.domain.map

import kotlinx.coroutines.flow.Flow
import org.maplibre.android.geometry.LatLng

@Suppress("TooManyFunctions", "ComplexInterface")
interface MapRepository {
    suspend fun init()
    suspend fun enable(name: String, enable: Boolean)
    suspend fun moverOrder(from: Int, to: Int)
    suspend fun alpha(name: String, alpha: Float)
    suspend fun visible(name: String, visible: Boolean)
    suspend fun save(name: String, url: String, minZoom: Float, maxZoom: Float)
    suspend fun delete(name: String)
    suspend fun add(name: String, url: String, minZoom: Float, maxZoom: Float): Boolean
    suspend fun addOrUpdate(maps: List<MapInput>)
    fun getListMapStream(): Flow<List<Map>>
    fun getListMapGroupName(stream: Boolean): Flow<List<MapBase>>
    fun getListMapEnable(): Flow<List<Map>>
    fun getListMapStreamEnable(): Flow<List<Map>>
    fun getMap(name: String): Flow<Map>
    suspend fun downloadTiles(
        name: String,
        mapsName: List<String>,
        area: Array<LatLng>,
        minZoom: Int,
        maxZoom: Int
    ): Boolean
}
