package com.ravenfeld.maps.domain.geo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CoordinateLegacy(
    @SerialName("waypoints")
    val waypoints: MutableList<GeoPoint> = mutableListOf(),
    @SerialName("segments")
    val segments: MutableList<List<GeoPoint>>,
    @SerialName("pois")
    val pois: MutableList<Poi> = mutableListOf(),
)
