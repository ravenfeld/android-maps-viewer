package com.ravenfeld.maps.domain.altitude

import com.ravenfeld.maps.domain.geo.GeoPoint
import kotlinx.coroutines.flow.Flow

interface AltitudeRepository {

    fun getAltitudeMsl(): Flow<Double>
    suspend fun getAltitude(point: GeoPoint): Double?
    suspend fun getAltitudes(points: List<GeoPoint>): List<GeoPoint>
}
