package com.ravenfeld.maps.domain.routing

enum class RoutingState {
    NotReady, Standby, Routing, Reconstructing, Canceling
}
