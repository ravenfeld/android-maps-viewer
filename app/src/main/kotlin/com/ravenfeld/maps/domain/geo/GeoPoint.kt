package com.ravenfeld.maps.domain.geo

import com.ravenfeld.maps.data.mapper.toPoint
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.maplibre.geojson.Point
import org.maplibre.turf.TurfConstants
import org.maplibre.turf.TurfMeasurement

@Serializable
data class GeoPoint(
    @SerialName("latitude")
    val latitude: Double,
    @SerialName("longitude")
    val longitude: Double,
    @SerialName("altitude")
    val altitude: Double? = null,
) {

    override fun toString(): String = "$latitude / $longitude  elevation: $altitude"

    fun isSamePosition(other: GeoPoint) =
        latitude == other.latitude && longitude == other.longitude
}

@Suppress("ComplexMethod", "LongMethod")
fun List<GeoPoint>.symplify(startUserPoint: GeoPoint, targetUserPoint: GeoPoint): List<GeoPoint> {
    if (this.size >= 2) {
        val newHighway = ArrayList<GeoPoint>()
        var indexTarget = -1
        var indexStart = -1
        var smallestDistTarget = Double.MAX_VALUE
        var smallestDistSource = Double.MAX_VALUE

        this.forEachIndexed { indexEdge, routingPoint ->
            val distStart = distanceBetween(
                startUserPoint,
                routingPoint
            )
            if (distStart < smallestDistSource) {
                smallestDistSource = distStart
                indexStart = indexEdge
            }

            val distTarget = distanceBetween(
                targetUserPoint,
                routingPoint
            )
            if (distTarget < smallestDistTarget) {
                smallestDistTarget = distTarget
                indexTarget = indexEdge
            }
        }

        var projectStart = getProjectionGeoPoint(
            startUserPoint,
            this[
                if (indexStart == 0) {
                    indexStart
                } else {
                    indexStart - 1
                }
            ],
            this[
                if (indexStart == 0) {
                    indexStart + 1
                } else {
                    indexStart
                }
            ]
        )

        var start = if (indexStart == 0) {
            indexStart + 1
        } else {
            indexStart
        }

        if (projectStart?.first == ProjectionResult.AFTER) {
            if (start < this.size - 1) {
                start += 1
            }
            projectStart = getProjectionGeoPoint(
                startUserPoint,
                this[
                    if (indexStart == 0) {
                        indexStart + 1
                    } else {
                        indexStart
                    }
                ],
                this[
                    if (indexStart < this.size - 1) {
                        indexStart + 1
                    } else {
                        indexStart
                    }
                ]
            )
        }
        projectStart?.let {
            if (projectStart.second != this[start]) {
                newHighway.add(projectStart.second)
            }
        }

        for (i in start until indexTarget) {
            newHighway.add(this[i])
        }

        var projectTarget = getProjectionGeoPoint(
            targetUserPoint,
            this[
                if (indexTarget == 0) {
                    indexTarget
                } else {
                    indexTarget - 1
                }
            ],
            this[
                if (indexTarget == 0) {
                    indexTarget + 1
                } else {
                    indexTarget
                }
            ]
        )

        if (projectTarget?.first == ProjectionResult.AFTER) {
            newHighway.add(this[indexTarget])
            projectTarget = getProjectionGeoPoint(
                targetUserPoint,
                this[
                    if (indexTarget == 0) {
                        indexTarget
                    } else {
                        indexTarget
                    }
                ],
                this[
                    if (indexTarget < this.size - 1) {
                        indexTarget + 1
                    } else {
                        indexTarget
                    }
                ]
            )
        }

        if (projectStart?.second != projectTarget?.second || newHighway.isEmpty()) {
            projectTarget?.let {
                newHighway.add(projectTarget.second)
            }
        }

        return newHighway
    } else {
        return this
    }
}

fun getProjectionGeoPoint(
    point: GeoPoint,
    startGeoPoint: GeoPoint,
    endGeoPoint: GeoPoint
): Pair<ProjectionResult, GeoPoint>? {
    if (point == startGeoPoint && point == endGeoPoint) {
        return null
    }
    val a = point.longitude - startGeoPoint.longitude
    val b = point.latitude - startGeoPoint.latitude
    val c = endGeoPoint.longitude - startGeoPoint.longitude
    val d = endGeoPoint.latitude - startGeoPoint.latitude

    val dot = a * c + b * d
    val lenSq = c * c + d * d
    var param = -1.0
    if (lenSq != 0.0) { // in case of 0 length line
        param = dot / lenSq
    }

    return when {
        param < 0 ->
            Pair(ProjectionResult.BEFORE, startGeoPoint)

        param > 1 ->
            Pair(ProjectionResult.AFTER, endGeoPoint)

        else -> Pair(
            ProjectionResult.CENTER,
            GeoPoint(startGeoPoint.latitude + param * d, startGeoPoint.longitude + param * c)
        )
    }
}

enum class ProjectionResult { BEFORE, CENTER, AFTER }

fun distanceBetween(startGeoPoint: GeoPoint, endGeoPoint: GeoPoint): Double {
    return TurfMeasurement.distance(
        startGeoPoint.toPoint(),
        endGeoPoint.toPoint(),
        TurfConstants.UNIT_METERS
    )
}

fun distanceBetweenPointLine(
    point: GeoPoint,
    startGeoPoint: GeoPoint,
    endGeoPoint: GeoPoint
): Double {
    val project = getProjectionGeoPoint(point, startGeoPoint, endGeoPoint)
    return project?.let {
        distanceBetween(point, project.second)
    } ?: run {
        0.0
    }
}

fun List<GeoPoint>.split(distanceSplit: Double): List<GeoPoint> {
    return if (this.size < 2) {
        this
    } else {
        val split = mutableListOf<GeoPoint>()
        for (i in 0..this.size - 2) {
            split.addAll(splitLine(this[i], this[i + 1], distanceSplit, false))
        }
        split.add(this.last())
        split
    }
}

private fun splitLine(
    point1: GeoPoint,
    point2: GeoPoint,
    distanceSplit: Double,
    includeLast: Boolean = true
): List<GeoPoint> {
    val distance = distanceBetween(point1, point2)
    val list = mutableListOf<GeoPoint>()
    return if (distance < distanceSplit * 2) {
        list.add(point1)
        if (distance > distanceSplit) {
            list.add(getPointDistance(point1, point2, distance / 2.0))
        }
        if (includeLast) {
            list.add(point2)
        }
        list
    } else {
        list.add(point1)
        for (i in 1..(distance / distanceSplit).toInt()) {
            list.add(getPointDistance(point1, point2, distanceSplit * i))
        }
        if (includeLast) {
            list.add(point2)
        }
        list
    }
}

private fun getPointDistance(
    point1: GeoPoint,
    point2: GeoPoint,
    distance: Double
): GeoPoint =
    TurfMeasurement.along(
        listOf(point1, point2).map { it.toPoint() },
        distance,
        TurfConstants.UNIT_METERS
    ).toGeoPoint()

internal fun Point.toGeoPoint() =
    GeoPoint(
        latitude = this.latitude(),
        longitude = this.longitude(),
        altitude = if (this.hasAltitude()) {
            this.altitude()
        } else {
            0.0
        }
    )
