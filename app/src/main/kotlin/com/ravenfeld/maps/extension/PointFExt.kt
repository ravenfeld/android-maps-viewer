package com.ravenfeld.maps.extension

import android.graphics.PointF
import android.graphics.RectF

private const val PADDING_TOUCH = 10f

internal fun PointF.toTouchRectF(padding: Float = PADDING_TOUCH) = RectF(
    x - padding,
    y + padding,
    x + padding,
    y - padding,
)
