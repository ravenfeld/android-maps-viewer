package com.ravenfeld.maps.extension

import android.content.Context

fun Context.getExternalFile() =
    this.getExternalFilesDirs(null).let { external ->
        if (external.size == 1) {
            external[0]
        } else {
            external.find { !it.path.contains("emulated") } ?: external[0]
        }
    }
