package com.ravenfeld.maps.domain.geo

import org.junit.Assert
import org.junit.Test

class ListGeoPointSymplifyTest {

    @Test
    fun test_0_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_1_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(1.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(1.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_2_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(2.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_3_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(3.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(3.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_4_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(4.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(4.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_5_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(5.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_6_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(6.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(6.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_7_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(7.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(7.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_8_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(8.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(8.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_9_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(9.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(9.0, 0.0),
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_10_10_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(10.0, 1.0)
        val pointTarget = GeoPoint(10.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(10.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_9_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(9.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(9.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_8_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(8.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0),
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_7_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(7.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(7.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_6_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(6.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(6.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_5_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(5.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_4_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(4.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(4.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_3_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(3.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(3.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_2_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(2.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(2.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_1_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(1.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0),
            GeoPoint(1.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_0_0_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(0.0, 1.0)
        val pointTarget = GeoPoint(0.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(0.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_1_9_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(1.0, 1.0)
        val pointTarget = GeoPoint(9.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(1.0, 0.0),
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0),
            GeoPoint(9.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_2_8_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(2.0, 1.0)
        val pointTarget = GeoPoint(8.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(2.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(8.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_3_7_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(3.0, 1.0)
        val pointTarget = GeoPoint(7.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(3.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(7.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_4_6_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(4.0, 1.0)
        val pointTarget = GeoPoint(6.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(4.0, 0.0),
            GeoPoint(5.0, 0.0),
            GeoPoint(6.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_5_5_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))
        highway.add(GeoPoint(5.0, 0.0))
        highway.add(GeoPoint(8.0, 0.0))
        highway.add(GeoPoint(10.0, 0.0))

        val pointStart = GeoPoint(5.0, 1.0)
        val pointTarget = GeoPoint(5.0, 1.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(5.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }

    @Test
    fun test_small_way() {
        val highway = ArrayList<GeoPoint>()
        highway.add(GeoPoint(0.0, 0.0))
        highway.add(GeoPoint(2.0, 0.0))

        val pointStart = GeoPoint(3.0, 1.0)
        val pointTarget = GeoPoint(2.0, 0.0)

        val drawHighway = highway.symplify(pointStart, pointTarget)

        val testHighway = listOf(
            GeoPoint(2.0, 0.0)
        )
        Assert.assertEquals(testHighway, drawHighway)
    }
}
