# OsmConversionPipeline

Java application to convert OpenStreetMap data (osm.pbf) to AndroMapViews format. Uses sevaral steps to simplify the routing graph and finally separate it into chunks.

```
mvn clean compile assembly:single
```

```
java -jar target/osm-conversion-pipeline-0.0.1-SNAPSHOT-jar-with-dependencies.jar INPUT_PBF OUTPUT
```