package jgrunert.osm_routing_app;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtils {

    public static void zip(String folder, String zipFile) {
        List <String> fileList = generateFileList(new File(folder));
        byte[] buffer = new byte[1024];
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            System.out.println("Output to Zip : " + zipFile);
            FileInputStream in = null;

            for (String file: fileList) {
                ZipEntry ze = new ZipEntry( file.replace(new File(folder).getAbsolutePath(), ""));
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(file);
                    int len;
                    while ((len = in .read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }

            zos.closeEntry();
            System.out.println("Folder successfully compressed");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static List <String> generateFileList(File folder) {
        List <String> fileList = new ArrayList < String > ();
        // add file only
        if (folder.isFile()) {
            fileList.add(folder.getAbsolutePath());
        }

        if (folder.isDirectory()) {
            File[] subNote = folder.listFiles();
            for (File filename: subNote) {
                fileList.addAll(generateFileList(filename));
            }
        }
        return fileList;
    }
}
