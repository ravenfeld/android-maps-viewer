I use https://github.com/systemed/tilemaker to convert a pbf to mbtiles.

I provide the configuration files because by default the private paths are not taken into account.


tilemaker --input /path/to/your/input.osm.pbf  --output /path/to/your/output.mbtiles